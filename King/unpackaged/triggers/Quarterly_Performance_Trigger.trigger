/*
    Trigger Name : Quarterly_Performance_Trigger
    Description: Trigger on Quarterly_Performance__c object
*/
trigger Quarterly_Performance_Trigger on Quarterly_Performance__c (before insert, before update) 
{
    Quarterly_Performance_TriggerHandler handler = new Quarterly_Performance_TriggerHandler();
    if(Trigger.isBefore && Trigger.isInsert) {
        handler.onBeforeInsert(Trigger.new);
    } else if (Trigger.isBefore && Trigger.isUpdate) {
        handler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
}