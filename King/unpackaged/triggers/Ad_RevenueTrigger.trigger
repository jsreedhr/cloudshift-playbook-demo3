/*
    Trigger Name : Ad_RevenueTriggerHandler
    Description: Trigger on Ad_Revenue__c object
*/
trigger Ad_RevenueTrigger on Ad_Revenue__c (before insert) 
{
    Ad_RevenueTriggerHandler handler = new Ad_RevenueTriggerHandler();
    if(Trigger.isBefore && Trigger.isInsert)
    {
        handler.onBeforeInsert(Trigger.New);
        handler.onBeforeInsertCopyDealInfo(Trigger.New);
    }
}