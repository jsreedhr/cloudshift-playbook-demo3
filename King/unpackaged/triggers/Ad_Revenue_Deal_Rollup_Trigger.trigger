trigger Ad_Revenue_Deal_Rollup_Trigger on Ad_Revenue__c (after update, after insert, after delete, after undelete) {
	Ad_Revenue_Deal_RollupTriggerHandler handler = new Ad_Revenue_Deal_RollupTriggerHandler();
    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isUndelete))
    {
		handler.onAfterChange(Trigger.new);
    } else if(Trigger.isAfter && Trigger.isDelete) {
        handler.onAfterChange(Trigger.old);
    }
}