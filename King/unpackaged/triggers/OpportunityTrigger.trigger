/*
    Trigger Name : OpportunityTrigger
    Description: Trigger on Opportunity object
*/
trigger OpportunityTrigger on Opportunity (after update) 
{
    OpportunityTriggerHandler handler = new OpportunityTriggerHandler();
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        System.debug('Trigger.isUpdate----' + Trigger.isUpdate);
        handler.onAfterUpdate(Trigger.New, Trigger.OldMap);
    }
}