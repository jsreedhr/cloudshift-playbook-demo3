<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ABS</label>
    <protected>false</protected>
    <values>
        <field>Inventory_Targeting__c</field>
        <value xsi:type="xsd:string">ABS</value>
    </values>
    <values>
        <field>Targeted_Placement_Id__c</field>
        <value xsi:type="xsd:string">3640685</value>
    </values>
</CustomMetadata>
