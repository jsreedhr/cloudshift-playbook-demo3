<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Revenue_Local</fullName>
        <field>Revenue__c</field>
        <formula>IF(

AND( Opportunity__r.RecordType.Name = &apos;Direct IO/Programmatic Guaranteed/Preferred Deal - Closed Won&apos;,
ISPICKVAL(Cost_Structure__c,&apos;CPM&apos;)), Rate__c* (Impressions__c/1000),

IF(

AND(Opportunity__r.RecordType.Name  = &apos;Direct IO/Programmatic Guaranteed/Preferred Deal - Closed Won&apos;,
ISPICKVAL(Cost_Structure__c,&apos;CPCV&apos;)), Rate__c * Completions__c,

IF(Opportunity__r.RecordType.Name = &apos;PMP - Closed Won&apos;,  eCPM__c  * (Impressions__c/1000),

NULL

)))</formula>
        <name>Update Revenue (Local)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Calculate Ad Revenue %28local%29</fullName>
        <actions>
            <name>Update_Revenue_Local</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Ad_Revenue__c.Currency_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Ad_Revenue__c.Cost_Structure__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Ad_Revenue__c.Source__c</field>
            <operation>notEqual</operation>
            <value>Adjustment</value>
        </criteriaItems>
        <description>Calculate Ad Revenue (local)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
