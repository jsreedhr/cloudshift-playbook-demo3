<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Credit_Hold_Approved</fullName>
        <description>Credit Hold - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Credit_Hold_Approved</template>
    </alerts>
    <alerts>
        <fullName>Credit_Hold_Rejected</fullName>
        <description>Credit Hold - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Credit_Hold_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Credit_Hold_Submitted_for_Finance_Approval</fullName>
        <description>Credit Hold - Submitted for Finance Approval</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Credit_Hold_Submitted_for_Approval</template>
    </alerts>
    <alerts>
        <fullName>Final_Approval_Notification</fullName>
        <description>Final Approval Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Final_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Final_Rejection_Notification</fullName>
        <description>Final Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Final_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inventory_Approval_Approved</fullName>
        <description>Inventory Approval - Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Inventory_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Inventory_Approval_Rejected</fullName>
        <description>Inventory Approval - Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Inventory_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Notify_Inventory_of_Submission</fullName>
        <description>Notify Inventory of Submission</description>
        <protected>false</protected>
        <recipients>
            <recipient>abi@csg.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Inventory_Final_Approval</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Ready_for_DFP_Push</fullName>
        <description>Opportunity Ready for DFP Push</description>
        <protected>false</protected>
        <recipients>
            <recipient>Ad_Operations</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Approvals/Opportunity_Ready_for_DFP_Push</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_AV_Status_Approved</fullName>
        <description>Update AV Status - Approved</description>
        <field>AVApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update AV Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AV_Status_Rejected</fullName>
        <description>Update AV Status - Rejected</description>
        <field>AVApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update AV Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_AV_Status_Submitted</fullName>
        <description>Update AV Status - Submitted</description>
        <field>AVApprovalStatus__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update AV Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Status_Approved</fullName>
        <description>Update Credit Status - Approved</description>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Credit Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Status_Rejected</fullName>
        <description>Update Credit Status - Rejected</description>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Credit Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Status_Submitted</fullName>
        <description>Update Credit Approval Status - Submitted</description>
        <field>Credit_Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Credit Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Internal_Campaign_Name</fullName>
        <field>Internal_Campaign_Name__c</field>
        <formula>IF(NOT(ISBLANK(Agency__c)), Agency__r.Name + &quot;_&quot;,NULL) + Account.Name + &quot;_&quot; + Name + &quot;_&quot; + 

IF(INCLUDES(Region__c, &quot;US&quot;), &quot;US_&quot;, NULL) &amp; 
IF(INCLUDES(Region__c, &quot;UK&quot;), &quot;UK_&quot;, NULL) &amp; 
IF(INCLUDES(Region__c, &quot;EMEA&quot;), &quot;EMEA_&quot;, NULL) &amp;
IF(INCLUDES(Region__c, &quot;APAC&quot;), &quot;APAC_&quot;, NULL) &amp;  
IF(INCLUDES(Region__c, &quot;MENA&quot;), &quot;MENA_&quot;, NULL) &amp;
IF(INCLUDES(Region__c, &quot;LATAM&quot;), &quot;LATAM_&quot;, NULL) &amp;
IF(INCLUDES(Region__c, &quot;INTL&quot;), &quot;INTL_&quot;, NULL) +

IF(INCLUDES(CostStructure__c,&quot;CPM&quot;), &quot;CPM_&quot;,
IF(INCLUDES(CostStructure__c,&quot;CPCV&quot;), &quot;CPCV_&quot;,
NULL))

+ TEXT(Product_Family__c)</formula>
        <name>Update Internal Campaign Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Status_Approved</fullName>
        <description>Update Inventory Status to &apos;Approved&apos;.</description>
        <field>InventoryApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Inventory Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Status_Open</fullName>
        <description>Update Inventory Status to &apos;Open&apos;</description>
        <field>InventoryApprovalStatus__c</field>
        <literalValue>Open</literalValue>
        <name>Update Inventory Status - Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Status_Rejected</fullName>
        <description>Update Inventory Status to &apos;Rejected&apos;.</description>
        <field>InventoryApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Inventory Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Status_Submitted</fullName>
        <description>Update Inventory Status to &apos;Submitted&apos;.</description>
        <field>InventoryApprovalStatus__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Inventory Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Status_10</fullName>
        <description>Update Opportunity Status - 10%</description>
        <field>StageName</field>
        <literalValue>Sales Lead</literalValue>
        <name>Update Opportunity Status - 10%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Status_Won</fullName>
        <description>Update Opportunity Status - Won</description>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Opportunity Status - Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PD_Status_Approved</fullName>
        <description>Update PD Status - Approved</description>
        <field>PreferredDealApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update PD Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PD_Status_Rejected</fullName>
        <description>Update PD Status - Rejected</description>
        <field>PreferredDealApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update PD Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PD_Status_Submitted</fullName>
        <description>Update PD Status - Submitted</description>
        <field>PreferredDealApprovalStatus__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update PD Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Preferred_Deal_Approval_Not_Req</fullName>
        <description>Update Preferred Deal Approval - Not Req</description>
        <field>PreferredDealApprovalStatus__c</field>
        <literalValue>Approval not Required</literalValue>
        <name>Update Preferred Deal Approval - Not Req</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Preferred_Deal_Approval_Required</fullName>
        <description>Update Preferred Deal Approval - Required</description>
        <field>PreferredDealApprovalStatus__c</field>
        <literalValue>Approval Required</literalValue>
        <name>Update PD Approval - Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pricing_StatusApproved</fullName>
        <description>Update Pricing Status - Approved</description>
        <field>PricingApprovalStatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Pricing Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pricing_Status_Rejected</fullName>
        <description>Update Pricing Status - Rejected</description>
        <field>PricingApprovalStatus__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Pricing Status - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pricing_Status_Submitted</fullName>
        <description>Update Pricing Status - Submitted</description>
        <field>PricingApprovalStatus__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Pricing Status - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Live</fullName>
        <field>StageName</field>
        <literalValue>Live</literalValue>
        <name>Update Stage to Live</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Ad Ops for DFP Push</fullName>
        <actions>
            <name>Opportunity_Ready_for_DFP_Push</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Alert Ad Ops for DFP Push</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Internal Campaign Name</fullName>
        <actions>
            <name>Update_Internal_Campaign_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Preferred Deal Approval - Not Required</fullName>
        <actions>
            <name>Update_Preferred_Deal_Approval_Not_Req</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>notEqual</operation>
            <value>Preferred Deal</value>
        </criteriaItems>
        <description>Update Preferred Deal Approval - Not Required</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Preferred Deal Approval - Required</fullName>
        <actions>
            <name>Update_Preferred_Deal_Approval_Required</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Preferred Deal</value>
        </criteriaItems>
        <description>Update Preferred Deal Approval - Required</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Live when Revenue starts</fullName>
        <actions>
            <name>Update_Stage_to_Live</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(
NOT(ISBLANK(NoofImpressions__c)),
NoofImpressions__c &gt; 0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
