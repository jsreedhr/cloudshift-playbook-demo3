<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Deselect_3rd_Party_Targeting</fullName>
        <field>X3rd_Party_Targeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect 3rd Party Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Device_Targeting</fullName>
        <field>Device_Targeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Device_Targeting</fullName>
        <field>Device_Targeting__c</field>
        <literalValue>1</literalValue>
        <name>Select Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>X3rd_Party_Targeting_to_TRUE</fullName>
        <field>X3rd_Party_Targeting__c</field>
        <literalValue>1</literalValue>
        <name>3rd Party Targeting? to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deselect 3rd Party Targeting%3F</fullName>
        <actions>
            <name>Deselect_3rd_Party_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( X3rd_Party__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deselect Device Targeting%3F</fullName>
        <actions>
            <name>Deselect_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK( Device__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Select 3rd Party Targeting%3F</fullName>
        <actions>
            <name>X3rd_Party_Targeting_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( 

NOT(ISBLANK(  X3rd_Party__c  )) 

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Select Device Targeting%3F</fullName>
        <actions>
            <name>Select_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK( Device__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
