<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Deselect_3rd_Party_Targeting</fullName>
        <description>Deselect 3rd Party Targeting?</description>
        <field>X3rdPartyTargeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect 3rd Party Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Device_Targeting</fullName>
        <description>Deselect Device Targeting?</description>
        <field>DeviceTargeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_3rd_Party_Targeting</fullName>
        <description>Select 3rd Party Targeting?</description>
        <field>X3rdPartyTargeting__c</field>
        <literalValue>1</literalValue>
        <name>Select 3rd Party Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Device_Targeting</fullName>
        <description>Select Device Targeting?</description>
        <field>DeviceTargeting__c</field>
        <literalValue>1</literalValue>
        <name>Select Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Impressions_Completions_nonAV</fullName>
        <description>Update Impressions/Completions for non AV Products.</description>
        <field>ImpressionsCompletions__c</field>
        <formula>IF( 

AND(CostStructure__c = &apos;CPM&apos;, AddedValuePlacement__c = FALSE), Quantity*1000, Quantity)</formula>
        <name>Update Impressions/Completions (non AV)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Impressions_Completions_non_AV</fullName>
        <description>Update Impressions/Completions for non-AV products.</description>
        <field>ImpressionsCompletions__c</field>
        <formula>IF(

CostStructure__c = &quot;CPM&quot;,  (Budget__c / UnitPrice) * 1000,  (Budget__c / UnitPrice) *1.07 )</formula>
        <name>Update Impressions/Completions (non AV)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Override_Rate_AV</fullName>
        <description>Update Override Rate</description>
        <field>UnitPrice</field>
        <formula>0</formula>
        <name>Update Override Rate (AV)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quantity_AV</fullName>
        <description>Update Quantity (AV)</description>
        <field>Quantity</field>
        <formula>Budget__c /ListPrice</formula>
        <name>Update Quantity (AV)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quantity_AV_Creation</fullName>
        <description>Update Impressions (AV) - Creation</description>
        <field>ImpressionsCompletions__c</field>
        <formula>IF( 

CostStructure__c = &quot;CPM&quot;, (Budget__c / ListPrice) * 1000, (Budget__c / ListPrice) *1.07 )</formula>
        <name>Update Impressions (AV) - Creation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quantity_nonAV</fullName>
        <field>Quantity</field>
        <formula>Budget__c/ UnitPrice</formula>
        <name>Update Quantity (non AV)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto-populate Quantity %28non AV%29 based on Budget</fullName>
        <actions>
            <name>Update_Impressions_Completions_non_AV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Budget__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>OpportunityLineItem.AddedValuePlacement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Auto-populate Quantity (non AV products) based on Budget.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Deselect 3rd Party Targeting%3F</fullName>
        <actions>
            <name>Deselect_3rd_Party_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Deselect 3rd Party Targeting?.</description>
        <formula>AND(

ISBLANK( X3rdParty__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deselect Device Targeting%3F</fullName>
        <actions>
            <name>Deselect_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Deselect Device Targeting?.</description>
        <formula>AND(

ISBLANK( Device__c  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Select 3rd Party Targeting%3F</fullName>
        <actions>
            <name>Select_3rd_Party_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Select 3rd Party Targeting?.</description>
        <formula>AND(

NOT(ISBLANK( X3rdParty__c ))

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Select Device Targeting%3F</fullName>
        <actions>
            <name>Select_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Select Device Targeting?.</description>
        <formula>AND(

NOT(ISBLANK( Device__c  ))

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Impressions%2FCompletions %28AV%29 - Creation</fullName>
        <actions>
            <name>Update_Quantity_AV_Creation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.AddedValuePlacement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Impressions/Completions based on Product Type for AV products.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Impressions%2FCompletions %28non AV%29</fullName>
        <actions>
            <name>Update_Impressions_Completions_nonAV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.AddedValuePlacement__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Update Impressions/Completions based on Product Type for non AV Products.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Override Rate %28AV%29</fullName>
        <actions>
            <name>Update_Override_Rate_AV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.AddedValuePlacement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Override Rate for AV Products</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Quantity %28AV%29</fullName>
        <actions>
            <name>Update_Quantity_AV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.AddedValuePlacement__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Quantity based on Budget and Rate.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
