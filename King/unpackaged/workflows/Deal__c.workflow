<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Deselect_3rd_Party_Targeting</fullName>
        <description>Deselect 3rd Party Targeting?</description>
        <field>X3rd_Party_Targeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect 3rd Party Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deselect_Device_Targeting</fullName>
        <description>Deselect Device Targeting?</description>
        <field>DeviceTargeting__c</field>
        <literalValue>0</literalValue>
        <name>Deselect Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_3rd_Party_Targeting</fullName>
        <description>Select 3rd Party Targeting?</description>
        <field>X3rd_Party_Targeting__c</field>
        <literalValue>1</literalValue>
        <name>Select 3rd Party Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Select_Device_Targeting</fullName>
        <description>Select Device Targeting?</description>
        <field>DeviceTargeting__c</field>
        <literalValue>1</literalValue>
        <name>Select Device Targeting?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Override_Rate</fullName>
        <description>Update Override Rate with Standard Rate</description>
        <field>OverrideRate__c</field>
        <formula>Rate__c</formula>
        <name>Update Override Rate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Deselect 3rd Party Targeting%3F</fullName>
        <actions>
            <name>Deselect_3rd_Party_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Deselect 3rd Party Targeting?</description>
        <formula>AND( 

ISBLANK(  X3rd_Party__c  ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deselect Device Targeting%3F</fullName>
        <actions>
            <name>Deselect_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Deselect Device Targeting?</description>
        <formula>ISBLANK( Device__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>If Override Rate is blank then show Rate</fullName>
        <actions>
            <name>Update_Override_Rate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Override Rate is blank then show Standard Rate</description>
        <formula>OR(
ISBLANK( OverrideRate__c ),
OverrideRate__c = 0
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Select 3rd Party Targeting%3F</fullName>
        <actions>
            <name>Select_3rd_Party_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Select 3rd Party Targeting?.</description>
        <formula>AND( 

NOT(ISBLANK(  X3rd_Party__c  )) 

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Select Device Targeting%3F</fullName>
        <actions>
            <name>Select_Device_Targeting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Select Device Targeting?</description>
        <formula>NOT(ISBLANK( Device__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
