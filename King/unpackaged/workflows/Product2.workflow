<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Product_Name</fullName>
        <description>Update Product Name upon creation.</description>
        <field>Name</field>
        <formula>IF(AND(RecordType.Name = &quot;Direct/Programmatic Guaranteed&quot;,  Added_Value__c = TRUE),
TEXT(Country__c) + &quot; - &quot; + RecordType.Name + &quot; - &quot; + TEXT(Family) + &quot; - &quot; + TEXT(Cost_Structure__c) + &quot; - &quot; + &quot;Added Value&quot;,

IF(AND(RecordType.Name = &quot;Direct/Programmatic Guaranteed&quot;,  Added_Value__c = FALSE),
TEXT(Country__c) + &quot; - &quot; + RecordType.Name + &quot; - &quot; + TEXT(Family) + &quot; - &quot; + TEXT(Cost_Structure__c)
 + &quot; - &quot; + &quot; $&quot; + TEXT(LowerBudgetLimit__c) + &quot; &quot; + &quot;to&quot; +
 &quot; &quot; + &quot; $&quot; + TEXT(UpperBudgetLimit__c), 

IF(RecordType.Name = &quot;Private Marketplace/Preferred Deal&quot;,
TEXT(Country__c) + &quot; - &quot; + RecordType.Name + &quot; - &quot; + TEXT(Family) + &quot; - &quot; + TEXT(Cost_Structure__c), NULL)))</formula>
        <name>Update Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Autopopulate Product Name</fullName>
        <actions>
            <name>Update_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Autopopulate Product Name upon creation with Country: Opportunity Type: Cost Structure - Budget Level</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
