public class OpportunityDFPController {

    private final Opportunity opp;
    private Account acc;
    private DFP_Inventory_Targetting_Setting__mdt[] targetCustMetadataList;
    private Map<String, String> metaDataMap;
    private DFPCompanyService.CompanyServiceInterfacePort companySer;
    private DFPOrderService.OrderServiceInterfacePort orderSer;
    private DFPLineItemService.LineItemServiceInterfacePort lineItemSer;
    private DFPPropsalService.ProposalServiceInterfacePort proposalSer;
    private DFPPropsalLineItemService.ProposalLineItemServiceInterfacePort proposalLineItemSer;

    //private final String networkCode = '21706971204';
    //network code for master network
    private final String networkCode = '32730525';
    private final String applicationName = 'kingdfp';
    private String authToken;

    private PageReference pageRefParent;
    public Boolean isInProgress {get; set;}
    public Boolean isOrderCreated {get; set;}

    public OpportunityDFPController() {
        isInProgress = true;
        //generate auth header
        //authToken = DfpConnector.getClientAuth2Token();
        //generate auth header for master network
        authToken = DfpConnectorMaster.getClientAuth2Token();
        
        companySer = new DFPCompanyService.CompanyServiceInterfacePort(authToken);
        companySer.RequestHeader =  new DFPCompanyService.SoapRequestHeader();
        companySer.RequestHeader.networkCode = networkCode;
        companySer.RequestHeader.applicationName = applicationName;

        orderSer = new DFPOrderService.OrderServiceInterfacePort(authToken);
        orderSer.RequestHeader =  new DFPOrderService.SoapRequestHeader();
        orderSer.RequestHeader.networkCode = networkCode;
        orderSer.RequestHeader.applicationName = applicationName;

        lineItemSer = new DFPLineItemService.LineItemServiceInterfacePort(authToken);
        lineItemSer.RequestHeader =  new DFPLineItemService.SoapRequestHeader();
        lineItemSer.RequestHeader.networkCode = networkCode;
        lineItemSer.RequestHeader.applicationName = applicationName;

        proposalSer = new DFPPropsalService.ProposalServiceInterfacePort(authToken);
        proposalSer.RequestHeader =  new DFPPropsalService.SoapRequestHeader();
        proposalSer.RequestHeader.networkCode = networkCode;
        proposalSer.RequestHeader.applicationName = applicationName;

        proposalLineItemSer = new DFPPropsalLineItemService.ProposalLineItemServiceInterfacePort(authToken);
        proposalLineItemSer.RequestHeader =  new DFPPropsalLineItemService.SoapRequestHeader();
        proposalLineItemSer.RequestHeader.networkCode = networkCode;
        proposalLineItemSer.RequestHeader.applicationName = applicationName;

        String oppId = ApexPages.currentPage().getParameters().get('id');
        System.debug('controller----->>>'+oppId);
        //pageRefParent = new PageReference('https://cs83.salesforce.com/'+oppId);
        pageRefParent = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppId);
        System.debug('parent ref : '+ pageRefParent);
        opp = [Select Id, Name, Type,StageName, Sent_to_DFP__c,IsWon, DFP_External_Id__c, Account.Advertiser_Id__c,Agency__r.Advertiser_Id__c,TradingDesk__r.Advertiser_Id__c,Buyer_ID__c, AccountId, StartDate__c , EndDate__c,Owner.DFP_Salesperson_Id__c, PONumber__c,ImpressionCap1__c,NumberofDuration1__c,Duration1__c,DFP_Order_Id__c,DFP_ProposalId__c,Internal_Campaign_Name__c,Revenue_Split_1__c,
            (Select Id, PlacementName__c,Inventory_Size__c, StartDate__c, EndDate__c, UnitPrice, CurrencyISOCode, CostStructure__c, ImpressionsCompletions__c, Impressions__c, Inventory_Targeting__c from OpportunityLineItems) from Opportunity 
        where Id = :oppId];
        // 0064E000006Z1rvQAC
        // , Inventory_Targeting__c
        acc = [Select Id, Name, Advertiser_Id__c from Account where id = :opp.AccountId];

        targetCustMetadataList = [SELECT Inventory_Targeting__c, Targeted_Placement_Id__c FROM DFP_Inventory_Targetting_Setting__mdt];
        metaDataMap = new Map<String, String>();
        for(DFP_Inventory_Targetting_Setting__mdt item : targetCustMetadataList) {
            metaDataMap.put(item.Inventory_Targeting__c, item.Targeted_Placement_Id__c);
        }
        System.debug('opp li list'+opp);
    }
    public PageReference redirectToParent() {
        pageRefParent.setRedirect(true);
        System.debug('redirecting to pageref : '+ pageRefParent);
        return pageRefParent;
    }

    public Long createCompanyIfNotExist(String advertiserId) {
        DFPCompanyService.Statement filterStatement = new DFPCompanyService.Statement();
        filterStatement.query = 'WHERE id = \'' + advertiserId + '\'';
        System.debug('query.....advertiserId>'+filterStatement.query);
        DFPCompanyService.CompanyPage cPage = companySer.getCompaniesByStatement(filterStatement);
        System.debug('cPage>>>>>'+cPage);
        if(cPage.totalResultSetSize == 0) {
            //create company
            List<DFPCompanyService.Company> createCompanyList = new List<DFPCompanyService.Company>();
            DFPCompanyService.Company newCompany = new DFPCompanyService.Company();
            newCompany.name = acc.Name;
            newCompany.type_x = 'ADVERTISER';
            createCompanyList.add(newCompany);
            DFPCompanyService.Company createdCompany = companySer.createCompanies(createCompanyList)[0];
            System.debug('createdCompany'+createdCompany);
            //update account advertizer id
            acc.Advertiser_Id__c = String.valueOf(createdCompany.id);
            //update acc;
            return createdCompany.id;
        }
        return Long.valueOf(advertiserId);
    }

    public SyncMessage msg {get; set;}
    public void validateAndSyncOppWithDFP() {
        Boolean isError = true;
        List<String> typeList = new List<String>();
        typeList.add('Direct IO');
        typeList.add('Programmatic Guaranteed');
        typeList.add('Preferred Deal');
        if(!typeList.contains(opp.type)) {
            msg = new SyncMessage(false, 'DFP Sync is allowed only for Opportunity Types \'Direct IO\' and \'Programmatic Guaranteed\'');
        }else if(opp.Sent_to_DFP__c == true) {
            msg = new SyncMessage(false, 'Opportunity is already added to DFP Order');
        }else if(opp.isWon == false){
            msg = new SyncMessage(false, 'DFP Sync is allowed only for Opportunity won \'true\'');
        }else if (opp.Owner.DFP_Salesperson_Id__c == null) {
            msg = new SyncMessage(false, 'Opportunity.Owner.DFP_Salesperson_Id__c canot be null as DFP Order.traffickerId is a required field');
        }else {
            isError = false;
        }
        System.debug('validation in process : '+msg);
        if(isError) {
            isInProgress = false;
            return;
        } else if(opp.type == 'Direct IO'){
            createOrderAndLineitemInDFP();
        } else if(opp.type == 'Programmatic Guaranteed' || opp.type == 'Preferred Deal') {
            createProposalAndLineItemInDFP();
        }
        
    }
    
    public void createOrderAndLineitemInDFP() {
        System.debug('opp item'+opp);
        //create Order in DFP
        //capture order id
        Long dfpOrderId;
        if(opp.DFP_Order_Id__c != null) {
            dfpOrderId = Long.valueOf(opp.DFP_Order_Id__c);
        }else {
            List<DFPOrderService.Order> orderList = new List<DFPOrderService.Order>();
            DFPOrderService.Order ord = new DFPOrderService.Order();
            
            ord.name = opp.Internal_Campaign_Name__c;
            ord.advertiserId = createCompanyIfNotExist(opp.Account.Advertiser_Id__c);

            ord.traffickerId = (opp.Owner.DFP_Salesperson_Id__c != null) ? Long.valueOf(opp.Owner.DFP_Salesperson_Id__c) : null;
            
            ord.poNumber = opp.PONumber__c;
            //check with tom
            ord.externalOrderId = Integer.valueOf(opp.DFP_External_Id__c);
            orderList.add(ord);
            List<DFPOrderService.Order> createdList;
            try {
                createdList = orderSer.createOrders(orderList);
                dfpOrderId = createdList[0].id;
                //update order id in opportunity
                opp.DFP_Order_Id__c = String.valueOf(dfpOrderId);
                isOrderCreated = true;
            } catch (Exception e) {
                msg = new SyncMessage(false, String.valueOf(e));
                isInProgress = false;
                return;
            }
        }
        
        System.debug('order id : '+dfpOrderId);
        
         //set frequencyCap
        DFPLineItemService.FrequencyCap fCap = new DFPLineItemService.FrequencyCap();
        fcap.maxImpressions = Integer.valueOf(opp.ImpressionCap1__c);
        fcap.numTimeUnits = Integer.valueOf(opp.NumberofDuration1__c);
        fcap.timeUnit = opp.Duration1__c;

        //Create LineItems
        List<DFPLineItemService.LineItem> lineItemList = new List<DFPLineItemService.LineItem>();
        try{
            for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                DFPLineItemService.LineItem lItem = new DFPLineItemService.LineItem();
                lItem.name = oppLineItem.PlacementName__c;
                lItem.lineItemType = 'STANDARD';
                //lItem.orderId = '2298685585';
                lItem.orderId = dfpOrderId;
                //set environmentType
                lItem.environmentType = 'VIDEO_PLAYER';
                //set creativePlaceholder
				System.debug('oppLineItem.Inventory_Size__c________'+oppLineItem.Inventory_Size__c);
                DFPLineItemService.Size sizeObject = new DFPLineItemService.Size();
                sizeObject.width = Integer.valueOf(oppLineItem.Inventory_Size__c.split('x')[0]);
                sizeObject.height = Integer.valueOf(oppLineItem.Inventory_Size__c.split('x')[1].removeEnd('v'));
                DFPLineItemService.CreativePlaceholder placeHolder = new DFPLineItemService.CreativePlaceholder();
                placeHolder.size = sizeObject;
                lItem.creativePlaceholders = new List<DFPLineItemService.CreativePlaceholder>{placeHolder};
                    
                lItem.startDateTime = (oppLineItem.StartDate__c !=null) ? getDFPLineItemDateTimeFormat(oppLineItem.StartDate__c) : null;
                lItem.endDateTime = (oppLineItem.EndDate__c!=null) ? getDFPLineItemDateTimeFormat(oppLineItem.EndDate__c) : null;
                //lItem.contractedUnitsBought = oppLineItem.ImpressionsCompletions__c.longValue();
                //set costPerUnit currency code and microAmount
                DFPLineItemService.Money mon = new DFPLineItemService.Money();
                mon.microAmount = (oppLineItem.UnitPrice * 1000000).longValue();
                mon.currencyCode = oppLineItem.CurrencyISOCode;
                lItem.costPerUnit = mon;

                lItem.costType = 'CPM';
                lItem.frequencyCaps = new List<DFPLineItemService.FrequencyCap>{fCap};
                lItem.creativeRotationType = 'EVEN';

                //set targeting - 
                DFPLineItemService.Targeting targ =new DFPLineItemService.Targeting();
                DFPLineItemService.InventoryTargeting inTarg = new DFPLineItemService.InventoryTargeting();
                //inTarg.targetedPlacementIds = new List<Long> {Long.valueOf('3640685'),Long.valueOf('3659045'),Long.valueOf('3747605'),Long.valueOf('3643685'),Long.valueOf('3748085')};
                inTarg.targetedPlacementIds = getTargetPlacementIdList(oppLineItem.Inventory_Targeting__c);
                targ.inventoryTargeting = inTarg;
                lItem.targeting = targ;
                lItem.externalId = oppLineItem.id;

                //columns added becaus of soap notnull error
                DFPLineItemService.Goal goal = new DFPLineItemService.Goal();
                goal.goalType = 'LIFETIME';
                goal.unitType = 'IMPRESSIONS';
                goal.units = oppLineItem.ImpressionsCompletions__c.longValue();
                lItem.primaryGoal = goal;
                //add line item to list
                lineItemList.add(lItem);
            }
        } catch (Exception e) {
            System.debug('exception lineitem creation : '+e);
            msg = new SyncMessage(false, String.valueOf(e));
            isInProgress = false;
            return;
        }

        //create DFP LineItems and get createdList
        System.debug('line item list to add : '+ lineItemList);
        if(lineItemList.size() > 0){
            List<DFPLineItemService.LineItem> createdLineItemList;
            try {
                createdLineItemList = lineItemSer.createLineItems(lineItemList);
            } catch (Exception e) {
                msg = new SyncMessage(false, String.valueOf(e));
                isInProgress = false;
                return;
            }
            System.debug('created line item list : '+createdLineItemList);
            Map<String, String> lineIdOppLineIdMap = new Map<String,String>();
            for(DFPLineItemService.LineItem lItem: createdLineItemList) {
                lineIdOppLineIdMap.put(lItem.externalId, String.valueOf(lItem.id));
            }
            System.debug('lineIdOppLineIdMap>>>>>>>>>>>'+lineIdOppLineIdMap);
            //set createdLineItem.id to DFP_Line_Item_Id__c in opportunity lineItem
            for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                if(lineIdOppLineIdMap.containsKey(oppLineItem.id)) {
                    System.debug('lineIdOppLineIdMap.get(oppLineItem.id);>>>>>>>>>>>'+lineIdOppLineIdMap.get(oppLineItem.id));
                    oppLineItem.DFP_Line_Item_Id__c = lineIdOppLineIdMap.get(oppLineItem.id);
                }
            }
        }
        try {
            
            //update order id in opportunity
            // opp.DFP_Order_Id__c = String.valueOf(dfpOrderId);
            //update Sent_to_DFP__c to true
            opp.Sent_to_DFP__c = true;
            update opp.OpportunityLineItems;
            update opp;
            update acc;

            msg = new SyncMessage(true, 'Opportunity successfully added to DFP');
            isInProgress = false;
        } catch (Exception e) {
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDMLMessage(0)));
            msg = new SyncMessage(false, e.getDMLMessage(0));
            System.debug('Exception|DML|OpportunityDFPController > '+e.getDMLMessage(0));
            isInProgress = false;
        }
    }

    //create proposal and lineitem in dfp for opportnities of type 'Programmatic Guaranteed
    public void createProposalAndLineItemInDFP() {
        //create proposal for the opportunity if not created already.
        Long dfpPropsalId;
        if(opp.DFP_ProposalId__c != null) {
            dfpPropsalId = Long.valueOf(opp.DFP_ProposalId__c);
            System.debug('already created-----');
        }else {
            List<DFPPropsalService.Proposal> proposalList = new List<DFPPropsalService.Proposal>();
            
            DFPPropsalService.Proposal proposal = new DFPPropsalService.Proposal();
            
            proposal.name = opp.Internal_Campaign_Name__c;
            System.debug('opp.Internal_Campaign_Name__c---'+ opp.Internal_Campaign_Name__c);
            
            proposal.isProgrammatic = true;
            
            DFPPropsalService.ProposalCompanyAssociation propComAssoc = new DFPPropsalService.ProposalCompanyAssociation();
            propComAssoc.companyId = createCompanyIfNotExist(opp.Account.Advertiser_Id__c);
            propComAssoc.type_x = 'ADVERTISER';
            proposal.advertiser = propComAssoc;
            System.debug('propComAssoc---'+ propComAssoc);

            List<DFPPropsalService.ProposalCompanyAssociation> propComAssocList = new List<DFPPropsalService.ProposalCompanyAssociation>();
            DFPPropsalService.ProposalCompanyAssociation propComAssocAgency = new DFPPropsalService.ProposalCompanyAssociation();
            propComAssocAgency.companyId = (opp.Agency__r.Advertiser_Id__c != null) ? Long.valueOf(opp.Agency__r.Advertiser_Id__c) : null;
            propComAssocAgency.type_x = 'PRIMARY_AGENCY';
            propComAssocList.add(propComAssocAgency);
            proposal.agencies = propComAssocList;
            System.debug('propComAssocAgency---'+ propComAssocAgency);
            
            //to-do : clarity needed salespersonId field
            DFPPropsalService.SalespersonSplit salesSplit = new DFPPropsalService.SalespersonSplit();
            salesSplit.userId = (opp.Owner.DFP_Salesperson_Id__c != null) ? Long.valueOf(opp.Owner.DFP_Salesperson_Id__c) : null;
            salesSplit.split = (opp.Revenue_Split_1__c != null) ? Integer.valueOf(opp.Revenue_Split_1__c) : null;
            proposal.primarySalesperson = salesSplit;
            System.debug('salesSplit---'+ salesSplit);
            
            proposal.poNumber = opp.PONumber__c;
            System.debug('poNumber---'+ opp.PONumber__c);
            
            DFPPropsalService.ProposalMarketplaceInfo propMarketPlaceInfo = new DFPPropsalService.ProposalMarketplaceInfo();
            //propMarketPlaceInfo.buyerAccountId = (opp.TradingDesk__r.Advertiser_Id__c != null) ? Long.valueOf(opp.TradingDesk__r.Advertiser_Id__c) : null;
            propMarketPlaceInfo.buyerAccountId = (opp.Buyer_ID__c != null) ? Long.valueOf(opp.Buyer_ID__c) : null;
            proposal.marketplaceInfo = propMarketPlaceInfo;
            System.debug('marketplaceInfo------'+propMarketPlaceInfo);
            System.debug('proposal------'+proposal);
            proposalList.add(proposal);

            List<DFPPropsalService.Proposal> createdList;
            try {
                createdList = proposalSer.createProposals(proposalList);
                dfpPropsalId = createdList[0].id;
                //update proposal id in opportunity
                opp.DFP_ProposalId__c = String.valueOf(dfpPropsalId);
                isOrderCreated = true;
            } catch (Exception e) {
                msg = new SyncMessage(false, String.valueOf(e));
                isInProgress = false;
                return;
            }
        }
        System.debug('propsal created id : ' + dfpPropsalId);

        //create proposallineitems objects from opportunitylineitems
        List<DFPPropsalLineItemService.ProposalLineItem> lineItemList = new List<DFPPropsalLineItemService.ProposalLineItem>();
        try{
            //set frequencyCap
            //DFPPropsalLineItemService.FrequencyCap fCap = new DFPPropsalLineItemService.FrequencyCap();
            //fcap.maxImpressions = Integer.valueOf(opp.ImpressionCap1__c);
            //fcap.numTimeUnits = Integer.valueOf(opp.NumberofDuration1__c);
            //fcap.timeUnit = opp.Duration1__c;
            
            for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                
                DFPPropsalLineItemService.ProposalLineItem lItem = new DFPPropsalLineItemService.ProposalLineItem();
                
                lItem.name = oppLineItem.PlacementName__c;
                
                if(opp.type == 'Preferred Deal') {
                    lItem.lineItemType = 'PREFERRED_DEAL';
                    lItem.estimatedMinimumImpressions = oppLineItem.ImpressionsCompletions__c.longValue();
                } else {
                	lItem.lineItemType = 'STANDARD';   
                }

                lItem.proposalId = dfpPropsalId;

                lItem.environmentType = 'VIDEO_PLAYER';

                DFPPropsalLineItemService.Size sizeObject = new DFPPropsalLineItemService.Size();
                sizeObject.width = Integer.valueOf(oppLineItem.Inventory_Size__c.split('x')[0]);
                sizeObject.height = Integer.valueOf(oppLineItem.Inventory_Size__c.split('x')[1].removeEnd('v'));
                DFPPropsalLineItemService.CreativePlaceholder placeHolder = new DFPPropsalLineItemService.CreativePlaceholder();
                placeHolder.size = sizeObject;
                lItem.creativePlaceholders = new List<DFPPropsalLineItemService.CreativePlaceholder>{placeHolder};
                    
                lItem.startDateTime = (oppLineItem.StartDate__c !=null) ? getDFPProposalLineItemDateTimeFormat(oppLineItem.StartDate__c) : null;
                lItem.endDateTime = (oppLineItem.EndDate__c!=null) ? getDFPProposalLineItemDateTimeFormat(oppLineItem.EndDate__c) : null;
                
                //lItem.contractedUnitsBought = oppLineItem.ImpressionsCompletions__c.longValue();
                
                //to-do - check with tom - if mapping is correct
                DFPPropsalLineItemService.Money mon = new DFPPropsalLineItemService.Money();
                mon.microAmount = (oppLineItem.UnitPrice * 1000000).longValue();
                mon.currencyCode = oppLineItem.CurrencyISOCode;
                //lItem.baseRate = mon;
                lItem.netRate = mon;

                lItem.rateType = 'CPM';

                //added due to notnull error - todo
                DFPPropsalLineItemService.ProposalLineItemMarketplaceInfo marketplaceInfo = new DFPPropsalLineItemService.ProposalLineItemMarketplaceInfo();
                marketplaceInfo.adExchangeEnvironment = 'DISPLAY';
                lItem.marketplaceInfo = marketplaceInfo;

                lItem.deliveryRateType = 'FRONTLOADED';
				
                //fcap removed
                //lItem.frequencyCaps = new List<DFPPropsalLineItemService.FrequencyCap>{fCap};
                
                //lItem.creativeRotationType = 'EVEN';

                //set targeting - data hardcoded as per the requirement
                DFPPropsalLineItemService.Targeting targ =new DFPPropsalLineItemService.Targeting();
                DFPPropsalLineItemService.InventoryTargeting inTarg = new DFPPropsalLineItemService.InventoryTargeting();
                //inTarg.targetedPlacementIds = new List<Long> {Long.valueOf('3640685'),Long.valueOf('3659045'),Long.valueOf('3747605'),Long.valueOf('3643685'),Long.valueOf('3748085')};
                inTarg.targetedPlacementIds = getTargetPlacementIdList(oppLineItem.Inventory_Targeting__c);
                targ.inventoryTargeting = inTarg;
                lItem.targeting = targ;

				//set opportunitylineitem.id to proposal.externalOd - check with tom - to-do
                // lItem.externalId = oppLineItem.id;
                lItem.internalNotes = oppLineItem.id;

                //columns added becaus of soap notnull error
                DFPPropsalLineItemService.Goal goal = new DFPPropsalLineItemService.Goal();
                goal.goalType = 'LIFETIME';
                goal.unitType = 'IMPRESSIONS';
                goal.units = oppLineItem.ImpressionsCompletions__c.longValue();
                lItem.goal = goal;
                
                //add line item to list
                lineItemList.add(lItem);
            }
        } catch (Exception e) {
            System.debug('exception proposal lineitem creation : '+e);
            msg = new SyncMessage(false, String.valueOf(e));
            isInProgress = false;
            return;
        }

        //create DFP ProposalLineItems and get created List back
        System.debug('proposalline item list to add : '+ lineItemList);
        if(lineItemList.size() > 0){
            List<DFPPropsalLineItemService.ProposalLineItem> createdLineItemList;
            try {
                createdLineItemList = proposalLineItemSer.createProposalLineItems(lineItemList);
            } catch (Exception e) {
                msg = new SyncMessage(false, String.valueOf(e));
                isInProgress = false;
                return;
            }
            System.debug('created line item list : '+createdLineItemList);
            Map<String, String> lineIdOppLineIdMap = new Map<String,String>();
            for(DFPPropsalLineItemService.ProposalLineItem lItem: createdLineItemList) {
                //lineIdOppLineIdMap.put(lItem.externalId, String.valueOf(lItem.id));
                System.debug('internal notees ------- ' + lItem.internalNotes);
                lineIdOppLineIdMap.put(lItem.internalNotes, String.valueOf(lItem.id));
            }
            System.debug('lineIdOppLineIdMap>>>>>>>>>>>'+lineIdOppLineIdMap);
            //set createdLineItem.id to DFP_Line_Item_Id__c in opportunity lineItem
            for(OpportunityLineItem oppLineItem : opp.OpportunityLineItems) {
                if(lineIdOppLineIdMap.containsKey(oppLineItem.id)) {
                    System.debug('lineIdOppLineIdMap.get(oppLineItem.id);>>>>>>>>>>>'+lineIdOppLineIdMap.get(oppLineItem.id));
                    oppLineItem.DFP_ProposalLineItemId__c = lineIdOppLineIdMap.get(oppLineItem.id);
                }
            }
        }
        try {
            
            //update order id in opportunity
            //update Sent_to_DFP__c to true
            opp.Sent_to_DFP__c = true;
            update opp.OpportunityLineItems;
            update opp;
            update acc;

            msg = new SyncMessage(true, 'Opportunity successfully added to DFP');
            isInProgress = false;
        } catch (Exception e) {
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e.getDMLMessage(0)));
            msg = new SyncMessage(false, e.getDMLMessage(0));
            System.debug('Exception|DML|OpportunityDFPController > '+e.getDMLMessage(0));
            isInProgress = false;
        }
    }

    public List<Long> getTargetPlacementIdList(String targetPlacements) {
        List<Long> targetPlacementIdList = new List<Long>();
        for(String item : targetPlacements.split(';')) {
            targetPlacementIdList.add(Long.valueOf(metaDataMap.get(item)));
        }
        System.debug('target placement id list : ' + targetPlacementIdList);
        return targetPlacementIdList;
    }
    
    public DFPLineItemService.DateTime_x getDFPLineItemDateTimeFormat(DateTime dt) {

        DFPLineItemService.DateTime_x dfpDateTime = new DFPLineItemService.DateTime_x();
        DFPLineItemService.Date_x dfpDate = new DFPLineItemService.Date_x();
        //set date_x
        dfpDate.year = dt.year();
        dfpDate.month = dt.month();
        dfpDate.day = dt.day();
        //set datetime_x
        dfpDateTime.date_x = dfpDate;
        dfpDateTime.hour = dt.hour();
        dfpDateTime.minute = dt.minute();
        dfpDateTime.second = dt.second();
        //dfpDateTime.timeZoneId = 'America/Los_Angeles';
        dfpDateTime.timeZoneId = 'Etc/GMT';

        return dfpDateTime;
   }
    
    public DFPPropsalLineItemService.DateTime_x getDFPProposalLineItemDateTimeFormat(DateTime dt) {

        DFPPropsalLineItemService.DateTime_x dfpDateTime = new DFPPropsalLineItemService.DateTime_x();
        DFPPropsalLineItemService.Date_x dfpDate = new DFPPropsalLineItemService.Date_x();
        //set date_x
        dfpDate.year = dt.year();
        dfpDate.month = dt.month();
        dfpDate.day = dt.day();
        //set datetime_x
        dfpDateTime.date_x = dfpDate;
        dfpDateTime.hour = dt.hour();
        dfpDateTime.minute = dt.minute();
        dfpDateTime.second = dt.second();
        //dfpDateTime.timeZoneId = 'America/Los_Angeles';
        dfpDateTime.timeZoneId = 'Etc/GMT';

        return dfpDateTime;
   }
   
    public void setProgressStatus(String msgText) {
        msg = new SyncMessage(false, msgText);
        isInProgress = false;
    }

    public class SyncMessage {
        public boolean success {get; set;}
        public String text {get; set;}
        public SyncMessage(Boolean success, String text) {
            this.success = success;
            this.text = text;
        }
    }
    public void updateOpportunityOrderId() {
        System.debug('created opp'+opp);
        update opp;
        update acc;
    }
}