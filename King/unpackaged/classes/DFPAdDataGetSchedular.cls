/**********************************************
 * CLASS NAME - DFPAdDataGetSchedular
 * AUTHOR - AMAN SAWHNEY
 * PURPOSE - A schedular class to enqueue
 *  process of starting a report in DFP server
 * ********************************************/
public class DFPAdDataGetSchedular implements Schedulable{

    public void execute(system.SchedulableContext sc){
     	System.enqueueJob(new DFPEnqueDataJob('DFP'));
        System.enqueueJob(new DFPEnqueDataJob('ADX'));
        
        /*
         * Schedule DFPReportProgressCheckerScheduler after 15 min of scheduling this class.
         * 
         * */
        DateTime setDateTime = System.now().addMinutes(5);

        String setday = string.valueOf(setDateTime.day());
        String setmonth = string.valueOf(setDateTime.month());
        String sethour = string.valueOf(setDateTime.hour());
        String setminute = string.valueOf(setDateTime.minute());
        String setsecond = string.valueOf(setDateTime.second());
        String setyear = string.valueOf(setDateTime.year());
        
        String strJobDFPSet = 'JobSet-' + setsecond + '_' + setminute + '_' + sethour + '_' + setday + '_' + setmonth + '_' + setyear;
        String strScheduleSet = '0 ' + setminute + ' ' + sethour + ' ' + setday + ' ' + setmonth + ' ?' + ' ' + setyear;
        System.schedule('Dfp-init_'+strJobDFPSet, strScheduleSet,new DFPReportProgressCheckerScheduler('DFP'));
        System.schedule('Adx-init_'+strJobDFPSet, strScheduleSet,new DFPReportProgressCheckerScheduler('ADX'));

    }  
}