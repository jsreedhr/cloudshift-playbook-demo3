/**********************************************
* CLASS NAME - DFPEnqueProcessJob
* AUTHOR - AMAN SAWHNEY
* PURPOSE - A class to execute the batch process
* for DFP data
* ********************************************/
public class DFPEnqueProcessJob implements Queueable, Database.AllowsCallouts {
    public String fromType{get;set;}
    public DFPEnqueProcessJob(String fromType){
        this.fromType = fromType;
    }
    public void execute(QueueableContext context) {
        
        String query = null;
        Long jobId = null;
        Server_Job__c job = Server_Job__c.getInstance();
        DFPReportService.ReportDownloadOptions reportDownloadOptions = new DFPReportService.ReportDownloadOptions();
        reportDownloadOptions.useGzipCompression = false;
        reportDownloadOptions.exportFormat = 'XML';
        
        //bifyurcation case of DFP and ADX
        if(fromType=='DFP'){
            jobId = (Long)job.DFP_Job_Id__c;
        }
        
        else if(fromType=='ADX'){
            jobId = (Long)job.ADX_Job_Id__c;
        }
        System.debug('job Id--------'+String.valueOf(jobId));
        String url = DFPServerCallHandler.reportServiceP.getReportDownloadUrlWithOptions(jobId,reportDownloadOptions);
       	System.debug('callout xml url----------->>>>>>'+url);
        //make http call
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        
        Http http = new Http();
        HttpResponse response = http.send(req);
        
        String responseBody = response.getBody();
        System.debug('respones : body of xml link '+ responseBody);
        //Start batch process
        DFPAdBatch dfpBatchExecutable = new DFPAdBatch(fromType);
        try{
            
            //get the DFP data as list from XML input
            List<DFPAdBatchOperator.DFPData> dfpDataList = DFPXMLResolver.getDFPDataList(responseBody);
            System.debug('dfpdatalist size : '+dfpDataList.size());
            //if dfpdata list is not empty
            if(dfpDataList!=null && !dfpDataList.isEmpty()){
                
                Set<String> scopeIdSet = new Set<String>();
               
                //initialize query for batch apex as perv DFP and ADX calls.
                if(fromType=='DFP'){
                    
                    for(DFPAdBatchOperator.DFPData dfpData : dfpDataList){
                    	System.debug('dfp data:'+dfpData);
                        System.debug('set scopeId : '+dfpData.LINE_ITEM_ID);
                        scopeIdSet.add(dfpData.LINE_ITEM_ID);
                        System.debug('set scopeId : '+scopeIdSet);
                    }
                    
                    query = 'Select Id,StartDate__c,EndDate__c,Impressions__c,Completions__c,Clicks__c,CTR__c,'+
                        'Completion_Rate__c,Revenue_local__c,DFP_Line_Item_Id__c,Opportunity.DFP_Order_Id__c,OpportunityId from OpportunityLineItem where Opportunity.Type IN (\'Direct '+
                        'IO\',\'Programmatic Guaranteed\') AND DFP_Line_Item_Id__c IN:scopeIdSet';
                }
                else if(fromType=='ADX'){
                    for(DFPAdBatchOperator.DFPData dfpData : dfpDataList){
                        System.debug('adx dfp data:'+dfpData);
                        System.debug('set scopeId : '+dfpData.AD_EXCHANGE_DEAL_ID);
                        scopeIdSet.add(dfpData.AD_EXCHANGE_DEAL_ID);
                        System.debug('set scopeId : '+scopeIdSet);
                    }
                    query = 'Select Id,Deal_ID__c,No_of_Impressions__c,No_of_Completions__c,Clicks__c,CTR__c,'+
                        'VTR__c,OpportunityName__c from Deal__c where OpportunityName__r.Type IN '+
                        '(\'PMP\',\'Preferred Deal\') AND Deal_ID__c IN:scopeIdSet';
                }
                
                //fill batch attributes
                dfpBatchExecutable.query = query;
                dfpBatchExecutable.scopeIdSet = scopeIdSet;
                dfpBatchExecutable.dfpDataList = dfpDataList;
                
                //run the batch
                Database.executeBatch(dfpBatchExecutable);
                
            }
            
            else{
                System.debug('DFP returned null or empty list..');
            }
            
        }
        catch(Exception e){
            
            system.debug('Line Number-->'+e.getLineNumber()+'--'+e.getMessage());
            
        }
        
        
    }
}