public class DfpConnectorMaster {

    private static String CLIENT_AUTH_URL = 'https://www.googleapis.com/oauth2/v4/token';
    private static String PRIVATE_KEY = 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCSpNby4PC7lJ+Y\nOaQOoUU6+iKFOZK8/IDo/GBEvatglgvosI3HEyOO7KcvySaL85BYBN4m8pnJ+KY3\nLvU5r4ihFNtShkBdBdK3Gv9FjsplmjVDES9+0K5B1DF5W6KJdxqsJzHKqFN1qIBD\n3SSLmKYHq8lZCsunVebAxdDFlfpQwrckO8CobnAZ77pNlUxmobym2hyTiaPWjwI0\nCyW3HJ4P58GB42fTZ9cD7wVUFzlR7eIUN+MmBsGc4PL1gAIcdR++36Z+C4LOBoN0\nvCmrwUu1pOkph0juWvvICd2L6Br2/CY0A7GOQVkHjHQy6f9yAJjcWtPgWq/3UnGo\nXGuZCRYPAgMBAAECggEAAXcPYJx9whRLqyHEfAal+S59PvH5i7aUZEWaXTDhcYpC\nCbmUgc0irGHU5ZXMmspL4OL1d+X0yIbvJf+03Ze4+4nd5rSAeXOKriiwgn7TACwa\nJROUsBY77xN7W2/kjaG0C1+A34+loHs1P+YJnW7anBShGd/gf6rSnGndiDKu6yiT\nvXSPnVBu1aVRNLH/T1uXSwdi3EB2lBTCzIXAG7mi4pQpB8lcBijB/lGZTQPbid2/\nRSpMRPkmLn4nsr4jMBAQVKFTKwNOIMN/dW50wg5ASLd1ySG4um9GR20RtiVzeyLG\nJ2cpw8pwM9IuEVsAf5Q2xTaM2WTReqXbnb2JoAYCsQKBgQDO2hXEhoDMrXfdfIw7\nIZt6uvhlFVccXDETCJEc1SUygwYWliq8jA73klAK/pu0MaovyRbY28BiXRH4HSoZ\nWMWPGkS5jXudgTwl6B13/IhdlHSq9fSB2ZCImCxXsnaCt8U7KBZlr/H9WsUS/FA0\nfxigrszl2PgQJ3gkEaYAurA5NQKBgQC1fI93ghdcAsTxqfz2rKLXtFH0CCl0PMaP\nOzL26pfuUJjDQcVnSX/DK416ADSEajJ+7EVOWGQxlqk40+TsjwdAdmFv4S9Eoqem\nDrhGnbL4SgZqlEhcE8jLDTFE/hObXmSy3Le0F6YooloibaCgfuWiy6o+CNGLk/Ah\nzpAWsyV+swKBgGZjnRLCYU7vLr37wI4kfImrsviWL6BiIgiB3RKgjuEJ6iGUA2OB\nEIeLbQOENB5l1xAAQcbmP7xwxUxDY2tYfbHwFsSiO+Yi6lr5Il0aK4Xmzjkxte3w\ne5KhgJGITRE3VfGKTkvXY7k9DP/w1OKnZtFitRmw5FlasCVqdvk1b7VhAoGASZbe\nXakka6y7yvoQlr1dsnVtnbQJixZxtPZeLfzl6j5IEzSCJOWKpsrsLQIUSXT+ULTh\nTWlOB0V+DIrvEnTUjfDyhXkbv4rsy8rAMfIT9dWeUG0cAioLizooNj+IF8HotxYE\nb0P/ng5m2fr13N5XovFOcKUGwsAT6MFnb6zokT0CgYEApl9J8LEYKgXfw80v49S+\nLA3gP69vZYt5dwqWm1muDHl15kuD86tHShy+1jHC4wafyO2TKiUDoEYWct2I7ywW\nZ6MaIy2AOGudxdoQmffpVZKG0e5tFmJLxhOEoWbVrZAIsNLHvalw/MpOUZyxtUD9\nyuNtl0Mouprt9KUkT+p7f0w=';

    public static String getClientAuth2Token() {
        Map<String, String> m = new Map<String, String>();
        m.put('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer');
        m.put('assertion', createEncodedJWTToken());
    
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setEndpoint(CLIENT_AUTH_URL);
        req.setMethod('POST');
        req.setTimeout(60 * 1000);
        req.setBody(formEncode(m));
        System.debug('req >>>>'+req);
        HttpResponse res = new Http().send(req);
        System.debug('res before>>>>'+res);
        if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
            System.debug('res token>>>>>>>>>>>'+ extractJsonField(res.getBody(), 'access_token'));
            return extractJsonField(res.getBody(), 'access_token');
        }
        return null;
    }

     private static String extractJsonField(String body, String field) {
         
        JSONParser parser = JSON.createParser(body);
        System.debug('parser---'+ parser);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == field) {
                parser.nextToken();
                return parser.getText();
            }
        }
        return null;
        //throw new Exception(field + ' not found in response ' + body);
    }
    private static String formEncode(Map<String, String> m) {
         
         String s = '';
         for (String key : m.keySet()) {
            if (s.length() > 0) {
                s += '&';
            }
            s += key + '=' + EncodingUtil.urlEncode(m.get(key), 'UTF-8');
         }
         return s;
    }

    private static String createEncodedJWTToken() {
        String alg = 'RS256';
        String typ = 'JWT';

        String iss = 'salesforce-integration-66@salesforce-integration-203615.iam.gserviceaccount.com';
        //String iss = 'dfp-import@da-flame-dfp.iam.gserviceaccount.com';
        String scope = 'https://www.googleapis.com/auth/dfp';
        String aud = CLIENT_AUTH_URL;
        String exp = String.valueOf(System.currentTimeMillis()/1000 + 60*60);
        String iat = String.valueOf(System.currentTimeMillis()/1000);

        String headerJson = JSON.serialize(new Header(alg, typ));
        String claimSetJson = JSON.serialize(new ClaimSet(iss, scope, aud, exp, iat));

        String token = encodeWithBase64url(Blob.valueOf(headerJson)) + '.' + encodeWithBase64url(Blob.valueOf(claimSetJson));
        System.debug('tokennnnn>>>'+token);
        String signature = encodeWithBase64url(Crypto.sign('RSA-SHA256', Blob.valueOf(token),EncodingUtil.base64Decode(PRIVATE_KEY)));
        System.debug('signature>>>'+signature);
        token += '.' + signature;

        return token;
    }

    private static string encodeWithBase64url(Blob b) {
        return EncodingUtil.base64Encode(b);
    }
    private class Header {
        String alg;
        String typ;
        public Header(String alg, String typ) {
            this.alg = alg;
            this.typ = typ;
        }
    }

    private class ClaimSet {
        String iss;
        String scope;
        String aud;
        String exp;
        String iat;
        public ClaimSet(String iss, String scope, String aud, String exp, String iat) {
            this.iss = iss;
            this.scope = scope;
            this.aud = aud;
            this.exp = exp;
            this.iat = iat;
        }
    }
}