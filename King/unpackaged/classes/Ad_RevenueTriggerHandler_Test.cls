@isTest
private class Ad_RevenueTriggerHandler_Test 
{
	private static testMethod void test() 
	{
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        //Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,  StageName = 'Sales Lead',
        //                Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
        //                DeliveryCountries__c = 'Australia', Budget__c = 20000, InventoryApprovalStatus__c = 'Approved', Region__c = 'US');
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,  StageName = 'Upcoming Campaign',
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, InventoryApprovalStatus__c = 'Approval Required', Region__c = 'US',
                                            AVApprovalStatus__c='Approval not Required',
                                            Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        
        Ad_Revenue__c objAd_Revenue = new Ad_Revenue__c(Date__c = Date.newInstance(2018, 01, 02), Opportunity__c = objOpp.Id, Revenue__c = 50000, Source__c = 'DFP');
        insert objAd_Revenue;
        
        System.assert([SELECT ID, Quarterly_Performance__c FROM Ad_Revenue__c WHERE Id =: objAd_Revenue.Id].Quarterly_Performance__c != null);
        
        Ad_Revenue__c objAd_Revenue1 = new Ad_Revenue__c(Date__c = Date.newInstance(2018, 04, 04), Opportunity__c = objOpp.Id, Revenue__c = 50000, Source__c = 'DFP');
        insert objAd_Revenue1;
        
        System.assert([SELECT ID, Quarterly_Performance__c FROM Ad_Revenue__c WHERE Id =: objAd_Revenue1.Id].Quarterly_Performance__c != null);
        
        Ad_Revenue__c objAd_Revenue2 = new Ad_Revenue__c(Date__c = Date.newInstance(2017, 04, 04), Opportunity__c = objOpp.Id, Revenue__c = 50000, Source__c = 'DFP');
        insert objAd_Revenue2;
        
	}
}