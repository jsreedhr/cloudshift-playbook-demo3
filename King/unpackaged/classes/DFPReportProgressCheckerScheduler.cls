public class DFPReportProgressCheckerScheduler implements Schedulable {
    
    private String fromType {get; set;}
    public DFPReportProgressCheckerScheduler(String fromType) {
        this.fromType = fromType;
    }
    
    public void execute(system.SchedulableContext sc) {
        System.enqueueJob(new DFPReportProgressCheckerQueable(fromType));    
    }
}