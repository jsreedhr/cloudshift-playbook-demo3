@isTest
public class WsdlApexCoverageTest {

    public static testMethod void LineItemServiceCoverageTest() {
		Test.startTest();
        
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());

        List<DFPLineItemService.LineItem> lItemList = new List<DFPLineItemService.LineItem>();
        DFPLineItemService.LineItem lItem = new DFPLineItemService.LineItem();
        DFPLineItemService.CreativePlaceholder cph = new DFPLineItemService.CreativePlaceholder();
        lItem.creativePlaceholders = new List<DFPLineItemService.CreativePlaceholder>{cph};

        DFPLineItemService.LineItemActivityAssociation liaa = new DFPLineItemService.LineItemActivityAssociation();
        lItem.activityAssociations = new List<DFPLineItemService.LineItemActivityAssociation>{liaa};

        lItem.stats = new DFPLineItemService.Stats();

        lItem.deliveryIndicator = new DFPLineItemService.DeliveryIndicator();

        lItem.deliveryData = new DFPLineItemService.DeliveryData();

        DFPLineItemService.AppliedLabel al = new DFPLineItemService.AppliedLabel();
        lItem.appliedLabels = new List<DFPLineItemService.AppliedLabel>{al};

        DFPLineItemService.BaseCustomFieldValue bcfv = new DFPLineItemService.BaseCustomFieldValue();
        lItem.customFieldValues = new List<DFPLineItemService.BaseCustomFieldValue>{bcfv};

        lItem.setTopBoxDisplayInfo = new DFPLineItemService.SetTopBoxInfo();

        lItem.grpSettings = new DFPLineItemService.GrpSettings();
        litem.creativeTargetings = new List<DFPLineItemService.CreativeTargeting>{new DFPLineItemService.CreativeTargeting()};
        
        DFPLineItemService.Targeting target = new DFPLineItemService.Targeting();
        target.geoTargeting = new DFPLineItemService.GeoTargeting();
        target.inventoryTargeting = new DFPLineItemService.InventoryTargeting();
        target.dayPartTargeting = new DFPLineItemService.DayPartTargeting();
        DFPLineItemService.TechnologyTargeting techtarget = new DFPLineItemService.TechnologyTargeting();
        techtarget.bandwidthGroupTargeting = new DFPLineItemService.BandwidthGroupTargeting();
        techtarget.browserTargeting = new DFPLineItemService.BrowserTargeting();
        techtarget.browserLanguageTargeting = new DFPLineItemService.BrowserLanguageTargeting();
        techtarget.deviceCapabilityTargeting = new DFPLineItemService.DeviceCapabilityTargeting();
        techtarget.deviceCategoryTargeting = new DFPLineItemService.DeviceCategoryTargeting();
        techtarget.deviceManufacturerTargeting = new DFPLineItemService.DeviceManufacturerTargeting();
        techtarget.mobileCarrierTargeting = new DFPLineItemService.MobileCarrierTargeting();
        techtarget.mobileDeviceTargeting = new DFPLineItemService.MobileDeviceTargeting();
        techtarget.mobileDeviceSubmodelTargeting = new DFPLineItemService.MobileDeviceSubmodelTargeting();
        techtarget.operatingSystemTargeting = new DFPLineItemService.OperatingSystemTargeting();
        techtarget.operatingSystemVersionTargeting = new DFPLineItemService.OperatingSystemVersionTargeting();
        target.technologyTargeting = techtarget;
        target.customTargeting = new DFPLineItemService.CustomCriteriaSet();
        target.userDomainTargeting = new DFPLineItemService.UserDomainTargeting();
        target.contentTargeting = new DFPLineItemService.ContentTargeting();
        target.videoPositionTargeting = new DFPLineItemService.VideoPositionTargeting();
        target.mobileApplicationTargeting = new DFPLineItemService.MobileApplicationTargeting();
        lItem.targeting = target;
        lItemList.add(lItem);

        DFPLineItemService.LineItemServiceInterfacePort lineItemSer = new DFPLineItemService.LineItemServiceInterfacePort('test token');
        lineItemSer.createLineItems(lItemList);
        
        Test.stopTest();
    }
    
    public static testMethod void ProposalServiceCoverageTest() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());

        DFPPropsalService.ProposalServiceInterfacePort proposalService = new DFPPropsalService.ProposalServiceInterfacePort('auth token');
        List<DFPPropsalService.Proposal> proposalList = new List<DFPPropsalService.Proposal>();
        DFPPropsalService.Proposal proposal = new DFPPropsalService.Proposal();
        proposal.customFieldValues = new List<DFPPropsalService.BaseCustomFieldValue>{new DFPPropsalService.BaseCustomFieldValue()};
        proposal.appliedLabels = new List<DFPPropsalService.AppliedLabel>{new DFPPropsalService.AppliedLabel()};
        proposal.workflowProgress = new DFPPropsalService.WorkflowProgress();
        proposal.resources = new List<DFPPropsalService.ProposalLink>{new DFPPropsalService.ProposalLink()};
        proposal.termsAndConditions = new List<DFPPropsalService.ProposalTermsAndConditions>{new DFPPropsalService.ProposalTermsAndConditions()};
        proposal.lastRetractionDetails = new DFPPropsalService.RetractionDetails();
        proposal.offlineErrors = new List<DFPPropsalService.OfflineError>{new DFPPropsalService.OfflineError()};
        DFPPropsalService.BuyerRfp buyerRef = new DFPPropsalService.BuyerRfp();
        DFPPropsalService.CreativePlaceholder cph = new DFPPropsalService.CreativePlaceholder();
        buyerRef.creativePlaceholders = new List<DFPPropsalService.CreativePlaceholder>{cph};
        DFPPropsalService.Targeting target = new DFPPropsalService.Targeting();
        target.geoTargeting = new DFPPropsalService.GeoTargeting();
        target.inventoryTargeting = new DFPPropsalService.InventoryTargeting();
        target.dayPartTargeting = new DFPPropsalService.DayPartTargeting();
        DFPPropsalService.TechnologyTargeting techTarget = new DFPPropsalService.TechnologyTargeting();
        techTarget.bandwidthGroupTargeting = new DFPPropsalService.BandwidthGroupTargeting();
        techTarget.browserTargeting = new DFPPropsalService.BrowserTargeting();
        techTarget.browserLanguageTargeting = new DFPPropsalService.BrowserLanguageTargeting();
        techTarget.deviceCapabilityTargeting = new DFPPropsalService.DeviceCapabilityTargeting();
        techTarget.deviceCategoryTargeting = new DFPPropsalService.DeviceCategoryTargeting();
        techTarget.deviceManufacturerTargeting = new DFPPropsalService.DeviceManufacturerTargeting();
        techTarget.mobileCarrierTargeting = new DFPPropsalService.MobileCarrierTargeting();
        techTarget.mobileDeviceTargeting = new DFPPropsalService.MobileDeviceTargeting();
        techTarget.mobileDeviceSubmodelTargeting = new DFPPropsalService.MobileDeviceSubmodelTargeting();
        techTarget.operatingSystemTargeting = new DFPPropsalService.OperatingSystemTargeting();
        techTarget.operatingSystemVersionTargeting = new DFPPropsalService.OperatingSystemVersionTargeting();
        target.technologyTargeting = techTarget;
        target.customTargeting = new DFPPropsalService.CustomCriteriaSet();
        target.userDomainTargeting = new DFPPropsalService.UserDomainTargeting();
        target.contentTargeting = new DFPPropsalService.ContentTargeting();
        target.videoPositionTargeting = new DFPPropsalService.VideoPositionTargeting();
        target.mobileApplicationTargeting = new DFPPropsalService.MobileApplicationTargeting();
        buyerRef.targeting = target;
        proposal.buyerRfp = buyerRef;
        proposalList.add(proposal);
        proposalService.createProposals(proposalList);
        
        Test.stopTest();
    }
}