@isTest
public class OpportunityDFPControllerTest {

    public static testMethod void createOrderAndLineitemInDFPTest() {
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Account accnt = new Account();
        accnt.Name = 'testing account 1';
        accnt.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Parent Company').getRecordTypeId(); 
        accnt.Advertiser_Id__c = '123456789';
        insert accnt;
        
        Opportunity testOpp = new Opportunity();
        testOpp.AccountId = accnt.Id;
        testOpp.Name = 'test opp testing';
        //testOpp.StageName = 'Discussing Requirements';
        testOpp.CloseDate = Date.valueOf('2019-12-12');
        testOpp.StageName = 'Upcoming Campaign';
        testOpp.InventoryApprovalStatus__c = 'Approval Required';
        testOpp.PricingApprovalStatus__c = 'Approval not Required';
        testOpp.PreferredDealApprovalStatus__c = 'Approval not Required';
        testOpp.Credit_Approval_Status__c = 'Approval not Required';
        insert testOpp;
        
        Id pricebookId = Test.getStandardPricebookId();

        //Create your product
        Product2 prod = new Product2(
             Name = 'Product X',
             ProductCode = 'Pro-X',
             isActive = true,
             CountryGroup__c = 'US',
             Country__c = 'US'
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
             Pricebook2Id = pricebookId,
             Product2Id = prod.Id,
             UnitPrice = 100.00,
             IsActive = true
        );
        insert pbEntry;

        
        OpportunityLineItem lItem = new OpportunityLineItem();
        lItem.OpportunityId = testOpp.Id;
        lItem.Inventory_Size__c = '640x480v';
        Date dt = Date.today();
        lItem.StartDate__c = dt.addDays(1);
        lItem.EndDate__c = dt.addDays(2);
        lItem.ImpressionsCompletions__c = 123456;
        lItem.UnitPrice = 0.24;
        lItem.PricebookEntryId = pbEntry.Id;
        insert lItem;
        
        PageReference pageRef = Page.DFP_Opportunity_Popup_Page;
        pageRef.getParameters().put('id', String.valueOf(testOpp.Id));
        Test.setCurrentPage(pageRef);
        
        Test.StartTest();
        OpportunityDFPController oppDfp = new OpportunityDFPController();
        oppDfp.createOrderAndLineitemInDFP();
        Test.stopTest();
    }
    
    public static testMethod void createProposalAndLineItemInDFPTest() {
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Account accnt = new Account();
        accnt.Name = 'testing account 1';
        accnt.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Parent Company').getRecordTypeId(); 
        accnt.Advertiser_Id__c = '123456789';
        insert accnt;
        
        Opportunity testOpp = new Opportunity();
        testOpp.AccountId = accnt.Id;
        testOpp.Name = 'test opp testing';
        //testOpp.StageName = 'Discussing Requirements';
        testOpp.CloseDate = Date.valueOf('2019-12-12');
        testOpp.StageName = 'Upcoming Campaign';
        testOpp.InventoryApprovalStatus__c = 'Approval Required';
        testOpp.PricingApprovalStatus__c = 'Approval not Required';
        testOpp.PreferredDealApprovalStatus__c = 'Approval not Required';
        testOpp.Credit_Approval_Status__c = 'Approval not Required';
        insert testOpp;
        
        Id pricebookId = Test.getStandardPricebookId();

        //Create your product
        Product2 prod = new Product2(
             Name = 'Product X',
             ProductCode = 'Pro-X',
             isActive = true,
             CountryGroup__c = 'US',
             Country__c = 'US'
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
             Pricebook2Id = pricebookId,
             Product2Id = prod.Id,
             UnitPrice = 100.00,
             IsActive = true
        );
        insert pbEntry;

        
        OpportunityLineItem lItem = new OpportunityLineItem();
        lItem.OpportunityId = testOpp.Id;
        lItem.Inventory_Size__c = '640x480v';
        Date dt = Date.today();
        lItem.StartDate__c = dt.addDays(1);
        lItem.EndDate__c = dt.addDays(2);
        lItem.ImpressionsCompletions__c = 123456;
        lItem.UnitPrice = 0.24;
        lItem.PricebookEntryId = pbEntry.Id;
        insert lItem;
        
        PageReference pageRef = Page.DFP_Opportunity_Popup_Page;
        pageRef.getParameters().put('id', String.valueOf(testOpp.Id));
        Test.setCurrentPage(pageRef);
        
        Test.StartTest();
        OpportunityDFPController oppDfp = new OpportunityDFPController();
        oppDfp.createProposalAndLineItemInDFP();
        Test.stopTest();
    }

}