public class Ad_Revenue_Deal_RollupTriggerHandler {

    private static Boolean hasRunRollupLogic = false;
    private Set<Id> dealIdSet = new Set<Id>();
    private Set<String> oppLiIdSet = new Set<String>();
    private List<Ad_Revenue__c> adRevenueRollupList;
    private List<Deal__c> dealList;


    public void onAfterChange(List<Ad_Revenue__c> adRevenueList) {
        if(adRevenueList.size() == 0 || hasRunRollupLogic) {
            return;
        }
        for(Ad_Revenue__c adRevenue : adRevenueList) {
            System.debug('adRevenue.Deal__c-----'+adRevenue.Deal__c);
            if(adRevenue.Deal__c != null) {
            	dealIdSet.add(adRevenue.Deal__c);
            }
            //logic implemented for dlrs rollup
            System.debug('adRevenue.OpportunityLineItem_Id__c------'+adRevenue.OpportunityLineItem_Id__c.substring(0,15));
            if(adRevenue.OpportunityLineItem_Id__c != null) {
                oppLiIdSet.add(adRevenue.OpportunityLineItem_Id__c.substring(0,15));
            }
        }
        List<AggregateResult> queryResult = [
            SELECT SUM(Revenue__c) localTotal, SUM(Revenue_USD__c) usdTotal, Deal__c
            FROM Ad_Revenue__c
            WHERE Deal__c IN :dealIdSet
            GROUP BY Deal__c
        ];
        List<Deal__c> dealsToUpdate = new List<Deal__c>();
        for (AggregateResult results : queryResult) {
            dealsToUpdate.add(
                new Deal__c(
                    Id = (Id) results.get('Deal__c'),
                    Revenue_local__c = (Decimal) results.get('localTotal'),
                    Revenue_USD__c = (Decimal) results.get('usdTotal')
                )
            );
        }
        //logic implemented for dlrs rollup
        List<AggregateResult> oppLiQueryResult = [
            SELECT SUM(Revenue__c) localTotal, SUM(Revenue_USD__c) usdTotal, OpportunityLineItem_Id__c
            FROM Ad_Revenue__c
            WHERE OpportunityLineItem_Id__c IN :oppLiIdSet
            GROUP BY OpportunityLineItem_Id__c
        ];
        //WHERE OpportunityLineItem_Id__c IN :oppLiIdSet
        System.debug('oppLiIdSet------'+oppLiIdSet);
        System.debug('result------'+oppLiQueryResult);
        List<OpportunityLineItem> oppLisToUpdate = new List<OpportunityLineItem>();
        for (AggregateResult results : oppLiQueryResult) {
            oppLisToUpdate.add(
                new OpportunityLineItem(
                    Id = (Id) results.get('OpportunityLineItem_Id__c'),
                    Revenue_local__c = (Decimal) results.get('localTotal'),
                    Revenue_USD__c = (Decimal) results.get('usdTotal')
                )
            );
        }
        try {
            update dealsToUpdate;
            update oppLisToUpdate;
        } catch(DmlException ex) {
            for (Ad_Revenue__c adRevenue : adRevenueList) {
                adRevenue.addError('An error has occured while rolling up the revenue to the deal/opportunitylineitem object, error : ' + ex.getMessage());
            }
        }
        hasRunRollupLogic = true;
        //adRevenueRollupList = new List<Ad_Revenue__c>([select id, Revenue__c, Revenue_USD__c, Deal__c from Ad_Revenue__c where Deal__c IN :dealIdSet]);
        //dealList = new List<Deal__c>([select id, Revenue_local__c, Revenue_USD__c, DMA__c from Deal__c where Id IN :dealIdSet]);
        //System.debug('dealList------'+dealList);
        //for(Deal__c deal : dealList) {
        //    deal.Revenue_local__c = getRollupOfRevenueLocal(deal.id);
        //    deal.Revenue_USD__c = getRollupOfRevenueUSD(deal.id);
        //}
        //if(dealList.size() > 0) {
        //    try {
        //        System.debug('dealList----'+dealList);
        //        update dealList;
        //    } catch (exception e) {

        //        System.debug('rollup update of deal from ad_revenue failed----'+e);
        //    }
        //}
    }

    //private Double getRollupOfRevenueLocal(Id dealId) {
    //    System.debug('Deal id---'+dealId);
    //    Double revenueLocal = 0;
    //    for(Ad_Revenue__c adRevenue : adRevenueRollupList) {
    //        if(adRevenue.Deal__c == dealId) {
    //            if(adRevenue.Revenue__c != null) {
    //            	revenueLocal += adRevenue.Revenue__c;
    //            }
    //        }
    //    }
    //    System.debug('Deal revenueLocal---'+revenueLocal);
    //    return revenueLocal;
    //}

    //private Double getRollupOfRevenueUSD(Id dealId) {
    //    System.debug('Deal id---'+dealId);
    //    Double revenueUSD = 0;
    //    for(Ad_Revenue__c adRevenue : adRevenueRollupList) {
    //        if(adRevenue.Deal__c == dealId) {
    //            if(adRevenue.Revenue_USD__c != null) {
    //            	revenueUSD += adRevenue.Revenue_USD__c;
    //            }
    //        }
    //    }
    //    System.debug('Deal Revenue_USD__c---'+revenueUSD);
    //    return revenueUSD;
    //}
}