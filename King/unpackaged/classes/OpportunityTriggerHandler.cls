/*
    Class Name : OpportunityTriggerHandler
    Description: Trigger Handler for Opportunity object
*/
public with sharing class OpportunityTriggerHandler
{
    private static Boolean hasCreatedQuarterlyPerfomances = false;
    /*
        Method Name : onAfterUpdate
        Description: This method will call on after update
    */
    public void onAfterUpdate(List<Opportunity> lstNewOpp, map<Id, Opportunity> mapOldOpp)
    {
        createQuarterly_PerformanceRecords(lstNewOpp, mapOldOpp);
    }

    //method to create quarterly performance records
    public void createQuarterly_PerformanceRecords(List<Opportunity> lstNewOpp, map<Id, Opportunity> mapOldOpp)
    {
        if (!hasCreatedQuarterlyPerfomances) {
            //fetch all related Quarterly_Performance__c recorts related to the oppIds
            set<Id> setOppIds = mapOldOpp.keySet();
            List<Quarterly_Performance__c> qtrPerformanceList = [SELECT Id, Opportunity__c, Start_Date__c, End_Date__c, OwnerId, Sales_Rep_1__c FROM Quarterly_Performance__c where Opportunity__c IN: setOppIds];
            //fetch the Quarterly_Date_Range records from custom metadata
            List<Quarterly_Date_Range__mdt> lstQuarterly_Date_Range = [SELECT Id, Start_Date__c, End_Date__c FROM Quarterly_Date_Range__mdt ORDER BY Start_Date__c];
            List<Quarterly_Performance__c> lstQuarterly_PerformanceToInsert = new List<Quarterly_Performance__c>();
			System.debug('objNewOpp. size----'+lstNewOpp.size());
            for(Opportunity objNewOpp:lstNewOpp) {
                //check if the oportunity changed to closed won
                System.debug('objNewOpp.StageName----'+objNewOpp.StageName);
                System.debug('objNewOpp.isWon---------'+objNewOpp.isWon);
                System.debug('mapOldOpp.get(objNewOpp.id).isWon-----'+mapOldOpp.get(objNewOpp.id).isWon);
                if(objNewOpp.StartDate__c != null && objNewOpp.EndDate__c != null && objNewOpp.isWon && (objNewOpp.isWon != mapOldOpp.get(objNewOpp.id).isWon
                                                                                                        || objNewOpp.StartDate__c != mapOldOpp.get(objNewOpp.id).StartDate__c
                                                                                                        || objNewOpp.EndDate__c != mapOldOpp.get(objNewOpp.id).EndDate__c))
                {
                    List<Date[]> quarterDateRangeList = getQuarterlyDateRange(lstQuarterly_Date_Range, objNewOpp.StartDate__c, objNewOpp.EndDate__c);
                    for(Date[] qtrDateRange : quarterDateRangeList) {
                        List<Quarterly_Performance__c> tempList = getQuarterlyPerformanceObjectByOppId(qtrPerformanceList, objNewOpp.Id, qtrDateRange);
                        if(tempList != null) {
                            Quarterly_Performance__c temp = tempList[0];
                            temp.OwnerId = objNewOpp.OwnerId;
                            temp.Sales_Rep_1__c = objNewOpp.Sales_Rep_1__c;
                            lstQuarterly_PerformanceToInsert.add(temp);
                        } else {
                            lstQuarterly_PerformanceToInsert.add(new Quarterly_Performance__c(Start_Date__c = qtrDateRange[0], End_Date__c = qtrDateRange[1],
                                                                                                Opportunity__c = objNewOpp.Id, OwnerId = objNewOpp.OwnerId,
                                                                                                Sales_Rep_1__c = objNewOpp.Sales_Rep_1__c));
                        }
                    }
                }
            }
            if(lstQuarterly_PerformanceToInsert != null && lstQuarterly_PerformanceToInsert.size() > 0) {
                System.debug('qtrly performance tst size----'+lstQuarterly_PerformanceToInsert.size());
             	upsert lstQuarterly_PerformanceToInsert;
                hasCreatedQuarterlyPerfomances = true;
            }
            //hasCreatedQuarterlyPerfomances = true;
        }
    }

    //method to get quarterly date ranges list from metadata using startdate and endDate of opportunity
    public List<List<Date>> getQuarterlyDateRange(List<Quarterly_Date_Range__mdt> qtrDateRangeListMdt, Date startDate, Date endDate) {
        List<List<Date>> quarterDateRangeList = new List<List<Date>>();
        List<Date> currentRange;
        for(Quarterly_Date_Range__mdt objQDRange: qtrDateRangeListMdt) {
            if(startDate >= objQDRange.Start_Date__c && startDate <=  objQDRange.End_Date__c) {
                currentRange = new List<Date>();
                currentRange.add(objQDRange.Start_Date__c);
                currentRange.add(objQDRange.End_Date__c);
                quarterDateRangeList.add(currentRange);
                if(endDate >= objQDRange.End_Date__c) {
                    startDate = objQDRange.End_Date__c + 1;
                }
            }
        }
        return quarterDateRangeList;
    }

    // method to return Quarterly_Performance__c if it exist for a given opportunity and date ranges
    public List<Quarterly_Performance__c> getQuarterlyPerformanceObjectByOppId(List<Quarterly_Performance__c> qtrPerformanceList, Id oppId, List<Date> dateRange) {
        List<Quarterly_Performance__c> perfList = new List<Quarterly_Performance__c>();
        for(Quarterly_Performance__c qtrPerformance : qtrPerformanceList) {
            if(qtrPerformance.Opportunity__c == oppId && qtrPerformance.Start_Date__c == dateRange[0] && qtrPerformance.End_Date__c == dateRange[1]) {
                perfList.add(qtrPerformance);
                return perfList;
            }
        }
        return null;
    }
}