@isTest
public class Ad_RevenueTriggerTest {

    public static testMethod void AdRevenueBeforeInsertTest() {
        Test.startTest();
        
		Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Brand').getRecordTypeId();
        Account acc = new Account(Name='TestAccountTest123987');
        acc.RecordTypeId = accRecordTypeId;
        insert acc;

        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PMP').getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.name = 'test name';
        opp.StageName = 'Upcoming Campaign';
        opp.InventoryApprovalStatus__c = 'Approval Required';
        opp.PricingApprovalStatus__c = 'Approval not Required';
        opp.PreferredDealApprovalStatus__c = 'Approval not Required';
        opp.Credit_Approval_Status__c = 'Approval not Required';
        opp.CloseDate = System.today();
		opp.AccountId = acc.id;
        opp.RecordTypeId = oppRecordTypeId;
        opp.Sales_Rep_1__c = UserInfo.getUserId();
        opp.Revenue_Split_1__c = 80;
        opp.CurrencyIsoCode = 'USD';
        insert opp;
        
        Id prodRecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Private Marketplace/Preferred Deal').getRecordTypeId();
        Product2 p = new Product2();
        p.CurrencyIsoCode = 'USD';
        p.Name = 'test productasd';
        p.CountryGroup__c  = 'US';
        p.Country__c = 'US';
        p.RecordTypeId = prodRecordTypeId;
        insert p;
        
        Deal__c deal = new Deal__c();
        deal.OpportunityName__c = opp.id;
        deal.Product__c = p.id;
        insert deal;
        
        Ad_Revenue__c adRev = new Ad_Revenue__c();
        adRev.Source__c = 'DFP';
        adRev.Opportunity__c = opp.id;
        adRev.Date__c = System.today();
        adRev.Deal__c = deal.id;
        insert adRev;
        
        delete adRev;
        
        Test.stopTest();
    }
}