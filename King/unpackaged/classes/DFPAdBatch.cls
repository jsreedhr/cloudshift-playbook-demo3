/**********************************************
* CLASS NAME - DFPAdBatch
* AUTHOR - AMAN SAWHNEY
* PURPOSE - A batch class to operate on 
* opportunities and opportunities line items
* ********************************************/

global class DFPAdBatch implements Database.Batchable<sObject>{
    
    //initialisations of maps for easy instance reference in DFPAdbatchOperator
    global String query{get;set;}
    global Set<String> scopeIdSet{get; set;}
    global String fromType{get;set;}
    global List<DFPAdBatchOperator.DFPData> dfpDataList{get;set;}
    
    global DFPAdBatch(String fromtype){
        this.fromType = fromType;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug(fromType + ' scope id set size :::: ' + scopeIdSet.size());
        System.debug(fromType + ' scope id set :::: ' + scopeIdSet);
        System.debug(fromType + ' query ::: '+Database.query(query));
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc,List<sObject> sfdcList) {
        
        //check for empty list from salesforce
        if(sfdcList!=null && !sfdcList.isEmpty()){
            
            if(fromType=='DFP'){
                
                Map<String,OpportunityLineItem> sfdcOppLiMap = new Map<String,OpportunityLineItem>();
                Map<String,Opportunity> sfdcOppMap = new Map<String,Opportunity>();
                
                //get the map of opportunity and opportunity line item
                for(OpportunityLineItem sfdcOppLi : (List<OpportunityLineItem>)sfdcList){
                    
                    //make opportunity line item map
                    sfdcOppLiMap.put(sfdcOppLi.DFP_Line_Item_Id__c,sfdcOppLi);
                    sfdcOppMap.put(sfdcOppLi.Opportunity.DFP_Order_Id__c, new Opportunity(Id = sfdcOppLi.OpportunityId));
                }
                System.debug('DFP>>>>>sfdcOppLiMap,dfpDataList>>>>>>>>>'+sfdcOppLiMap+'::::::'+dfpDataList);
               
                
                //update the opportunity line items
                DFPAdBatchOperator.updateOpportunityLineItems(sfdcOppLiMap,dfpDataList);
                
                //update opportunities
                DFPAdBatchOperator.updateOpportunity(sfdcOppMap,dfpDataList);
                
                 //create the relevant ads
                DFPAdBatchOperator.createAdRevenueDFP(sfdcOppLiMap,dfpDataList);
            }
            
            else if(fromType=='ADX'){
                
                Map<String,Deal__c> sfdcDealMap = new Map<String,Deal__c>();
                
                //get the map of opportunity and opportunity line item
                for(Deal__c sfdcDeal : (List<Deal__c>)sfdcList){
                    
                    //make opportunity line item map
                    sfdcDealMap.put(sfdcDeal.Deal_ID__c,sfdcDeal);
                    
                }
                System.debug('ADX>>>>>sfdcDealMap,dfpDataList>>>>>>>>>'+sfdcDealMap+'::::::'+dfpDataList);
                
                
                //update the deals
                DFPAdBatchOperator.updateDeals(sfdcDealMap,dfpDataList);
                
                //create the relevant ads
                DFPAdBatchOperator.createAdRevenueADX(sfdcDealMap,dfpDataList);
                
            }
            
        }
        
        
    }
    
    global void finish(Database.BatchableContext bc) {
        //after batch operations
    }
    
    
}