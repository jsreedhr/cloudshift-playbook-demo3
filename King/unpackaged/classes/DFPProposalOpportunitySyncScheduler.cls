public class DFPProposalOpportunitySyncScheduler implements Schedulable {
	public void execute(system.SchedulableContext sc){
        System.enqueueJob(new DFPOrderIdSyncJob());
    }
}