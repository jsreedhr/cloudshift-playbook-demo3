/**********************************************
 * CLASS NAME - DFPServerCallHandler
 * AUTHOR - AMAN SAWHNEY
 * PURPOSE - A class create a report request
 * and make its server call and return report 
 * id.
 * ********************************************/
public class DFPServerCallHandler {
    
    public static DFPReportService.ReportServiceInterfacePort reportServiceP{get;set;}
    static{
        //make serverinterfaceport service object
        reportServiceP = new DFPReportService.ReportServiceInterfacePort();
        
        //fill the request header
        reportServiceP.RequestHeader =  new DFPReportService.SoapRequestHeader();
        reportServiceP.RequestHeader.networkCode = '32730525';
        reportServiceP.RequestHeader.applicationName = 'kingdfp';        
    }
  
    // make DFP server call for DFP data for opportunity line items
    public static Long makeServerCallDFP(){
        
        // make report query
        DFPReportService.ReportQuery reportQuery= new DFPReportService.ReportQuery();
        
        // initialize the properties
        List<String> dimensionList = new List<String>();
        List<String> columnList = new List<String>();
        List<String> dimensionAttributeList = new List<String>();
    
        //add the required dimensions
        dimensionList.add('LINE_ITEM_ID');
        dimensionList.add('ORDER_NAME');
        
        //add columns and properties
        columnList.add('TOTAL_LINE_ITEM_LEVEL_IMPRESSIONS');
        columnList.add('TOTAL_LINE_ITEM_LEVEL_CLICKS');
        columnList.add('VIDEO_VIEWERSHIP_COMPLETE');
        columnList.add('TOTAL_LINE_ITEM_LEVEL_CTR');
        columnList.add('VIDEO_VIEWERSHIP_COMPLETION_RATE');
        
        //add dimension attributes
        dimensionAttributeList.add('LINE_ITEM_START_DATE_TIME');
        dimensionAttributeList.add('LINE_ITEM_END_DATE_TIME');
        dimensionAttributeList.add('ORDER_LIFETIME_IMPRESSIONS');
        dimensionAttributeList.add('ORDER_LIFETIME_CLICKS');
        
        //reportQuery.dateRangeType = 'REACH_LIFETIME';
        reportQuery.startDate = getCustomDate('start');
        reportQuery.endDate = getCustomDate('end');
        System.debug('startdate-------'+getCustomDate('start'));
        System.debug('enddate---------'+getCustomDate('end'));
        reportQuery.dateRangeType = 'CUSTOM_DATE';
        reportQuery.columns = columnList;
        reportQuery.dimensions = dimensionList;
        reportQuery.dimensionAttributes = dimensionAttributeList;
        
        DFPReportService.ReportJob requestReportJob = new DFPReportService.ReportJob();
        DFPReportService.ReportJob responseReportJob = null;
            
        requestReportJob.reportQuery = reportQuery;
        responseReportJob = reportServiceP.runReportJob(requestReportJob);
        
        return responseReportJob.id;
        
    }
    
    // make DFP server call for DFP data for deals
    public static Long makeServerCallADX(){
        
        // make report query
        DFPReportService.ReportQuery reportQuery= new DFPReportService.ReportQuery();
        
        // initialize the properties
        List<String> dimensionList = new List<String>();
        List<String> columnList = new List<String>();
        List<String> dimensionAttributeList = new List<String>();
    
        //add the required dimensions
        dimensionList.add('AD_EXCHANGE_DEAL_ID');
        dimensionList.add('AD_EXCHANGE_VIDEO_AD_TYPE');
        dimensionList.add('AD_EXCHANGE_VIDEO_AD_DURATION');
        dimensionList.add('AD_EXCHANGE_ADVERTISER_NAME');
        
        //add columns and properties
        columnList.add('AD_EXCHANGE_IMPRESSIONS');
        columnList.add('AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS');
        columnList.add('AD_EXCHANGE_CLICKS');
        columnList.add('AD_EXCHANGE_MATCHED_ECPM');
        columnList.add('AD_EXCHANGE_CTR');
        columnList.add('AD_EXCHANGE_VIDEO_TRUEVIEW_VTR');
        
        
        //reportQuery.dateRangeType = 'REACH_LIFETIME';
        reportQuery.startDate = getCustomDate('start');
        reportQuery.endDate = getCustomDate('end');
        System.debug('startdate-------'+getCustomDate('start'));
        System.debug('enddate---------'+getCustomDate('end'));
        reportQuery.dateRangeType = 'CUSTOM_DATE';
        reportQuery.columns = columnList;
        reportQuery.dimensions = dimensionList;
        
        DFPReportService.ReportJob requestReportJob = new DFPReportService.ReportJob();
        DFPReportService.ReportJob responseReportJob = null;
            
        requestReportJob.reportQuery = reportQuery;
        responseReportJob = reportServiceP.runReportJob(requestReportJob);
        
        return responseReportJob.id;
        
    }
    
    public static DFPReportService.Date_x getCustomDate(String param) {
        Date currentDate = Date.today();
        Integer year;
        Integer month;
        Integer day;
        if(param == 'start') {
            Date dateYesterday = currentDate.addDays(-61);
            year = dateYesterday.year();
            month = dateYesterday.month();
            day = dateYesterday.day();
        } else if(param == 'end') {
            Date dateYesterday = currentDate.addDays(-1);
            year = dateYesterday.year();
            month = dateYesterday.month();
            day = dateYesterday.day();
        }
        DFPReportService.Date_x customDate = new DFPReportService.Date_x();
        customDate.year = year;
        customDate.month = month;
        customDate.day = day;
        return customDate;
    }
    
    public class ReportFailedException extends Exception{
        public String message;
    }
    
}