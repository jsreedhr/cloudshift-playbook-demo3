@isTest
public class DFPSchedulerClassesTest {
    
    public static testmethod void DFPReportProgressCheckerSchedulerTest(){
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Test.startTest();
        Datetime dt = Datetime.now().addSeconds(10);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId1 = System.schedule('DFP_report_progress_scheduler_test', CRON_EXP, new DFPReportProgressCheckerScheduler('DFP'));
        String jobId2 = System.schedule('ADX_report_progress_scheduler_test', CRON_EXP, new DFPReportProgressCheckerScheduler('ADX'));
        Test.stopTest();
        System.assert(jobId1 != null && jobId2 != null);
    }
    
    public static testmethod void DFPAdDataGetSchedularTest(){
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Test.startTest();
        Datetime dt = Datetime.now().addSeconds(10);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId = System.schedule('DFP_Ad_get_scheduler_Test', CRON_EXP, new DFPAdDataGetSchedular());
        Test.stopTest();
        System.assert(jobId != null);
    }
    
    @isTest
    static void DFPProposalOpportunitySyncSchedulerTest() {
        /*
		Account accnt = new Account();
        accnt.Name = 'testing account 1';
        accnt.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Parent Company').getRecordTypeId(); 
        accnt.Advertiser_Id__c = '123456789';
        insert accnt;
        
        Opportunity testOpp = new Opportunity();
        testOpp.AccountId = accnt.Id;
        testOpp.Name = 'test opp testing';
        testOpp.StageName = 'Discussing Requirements';
        testOpp.CloseDate = Date.valueOf('2019-12-12');
        testOpp.DFP_ProposalId__c = '12345';
        insert testOpp;
        
        Id pricebookId = Test.getStandardPricebookId();

        //Create your product
        Product2 prod = new Product2(
             Name = 'Product X',
             ProductCode = 'Pro-X',
             isActive = true,
             CountryGroup__c = 'US',
             Country__c = 'US'
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
             Pricebook2Id = pricebookId,
             Product2Id = prod.Id,
             UnitPrice = 100.00,
             IsActive = true
        );
        insert pbEntry;

        
        OpportunityLineItem lItem = new OpportunityLineItem();
       	lItem.OpportunityId = testOpp.Id;
        lItem.Inventory_Size__c = '640x480';
        Date dt_x = Date.today();
        lItem.StartDate__c = dt_x.addDays(1);
        lItem.EndDate__c = dt_x.addDays(2);
        lItem.ImpressionsCompletions__c = 123456;
        lItem.UnitPrice = 0.24;
        lItem.PricebookEntryId = pbEntry.Id;
        lItem.DFP_ProposalLineItemId__c = '123456';
        insert lItem;
        */
        Test.startTest();
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Datetime dt = Datetime.now().addSeconds(10);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId = System.schedule('DFPProposalOpportunitySyncScheduler_Test', CRON_EXP, new DFPProposalOpportunitySyncScheduler());
        Test.stopTest();
        
        System.assert(jobId != null);
    }
}