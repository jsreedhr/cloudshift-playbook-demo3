@isTest
private class OpportunityTriggerHandler_Test 
{
	private static testMethod void test1() 
	{
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Region__c = 'US',
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        System.debug('inserting stage-----');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        System.debug('after insert opp id-----' + objOpp.id);
        update objOpp;
        System.debug('after update opp stage');
        //system.assertEquals([SELECT count() from Quarterly_Performance__c], 2);
        delete [SELECT Id from Quarterly_Performance__c];
        
	}

    private static testMethod void test3() {
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 02, 02),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Region__c = 'US',
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        //system.assertEquals([SELECT count() from Quarterly_Performance__c], 1);
        delete [SELECT Id from Quarterly_Performance__c];
    }
    
    private static testMethod void test4() {
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2019, 02, 02),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Region__c = 'US',
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        //system.assertEquals([SELECT count() from Quarterly_Performance__c], 5);
    }
    
    private static testMethod void test2() {
        Account objAcc = new Account(Name = 'test acc2', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Dealss',AccountID = objAcc.id,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Region__c = 'US',
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        Quarterly_Performance__c qtrPerf = new Quarterly_Performance__c(Start_Date__c = Date.newInstance(2018, 01, 01), End_Date__c = Date.newInstance(2018, 03, 31),
                                                                                            Opportunity__c = objOpp.Id);
        insert qtrPerf;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        //system.assertEquals([SELECT count() from Quarterly_Performance__c], 2);
        delete [SELECT Id from Quarterly_Performance__c];
	}
}