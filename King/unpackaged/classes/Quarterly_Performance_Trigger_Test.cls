@isTest
private class Quarterly_Performance_Trigger_Test 
{
	private static testMethod void test() 
	{
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id, 
                        RecordTypeId = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('Direct IO/Programmatic Guaranteed/Preferred Deal').RecordTypeId,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Sales_Rep_1__c = UserInfo.getUserId(), Region__c = 'US',
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        Sales_Quota__c sq = new Sales_Quota__c(StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c= Date.newInstance(2018, 04, 04), Name__c=UserInfo.getUserId());
        insert sq;
        Quarterly_Performance__c objQuarterly_Performance = new Quarterly_Performance__c(Opportunity__c = objOpp.Id, Start_Date__c = Date.newInstance(2018, 01, 01),
                                                                                        End_Date__c = Date.newInstance(2018, 04, 04),Sales_Rep_2__c = UserInfo.getUserId(), Sales_Rep_1__c = UserInfo.getUserId());
        insert objQuarterly_Performance;
        
	}
	private static testMethod void test1() 
	{
        Account objAcc = new Account(Name = 'test acc1', RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Parent Company').RecordTypeId);
        insert objAcc;
        
        Opportunity objOpp = new Opportunity(Name ='00New mAWS Deal',AccountID = objAcc.id,
                        RecordTypeId = Schema.SObjectType.Opportunity.RecordTypeInfosByName.get('PMP').RecordTypeId,
                        Amount = 3000, CloseDate = System.today(), StartDate__c = Date.newInstance(2018, 01, 01), EndDate__c = Date.newInstance(2018, 04, 04),
                        DeliveryCountries__c = 'Australia', Budget__c = 20000, Sales_Rep_2__c = UserInfo.getUserId(),
                        Region__c = 'US', Revenue_Split_1__c  = 10, Revenue_Split_2__c = 10,
                        StageName = 'Upcoming Campaign',
                        InventoryApprovalStatus__c = 'Approval Required',
                        PricingApprovalStatus__c = 'Approval not Required',
                        PreferredDealApprovalStatus__c = 'Approval not Required',
                        Credit_Approval_Status__c = 'Approval not Required');
        insert objOpp;
        //objOpp.StageName = 'Closed Won';
        objOpp.StageName = 'Discussing Requirements';
        update objOpp;
        
        Quarterly_Performance__c objQuarterly_Performance = new Quarterly_Performance__c(Opportunity__c = objOpp.Id, Start_Date__c = Date.newInstance(2018, 01, 01),
                                                                                        End_Date__c = Date.newInstance(2018, 04, 04));
        insert objQuarterly_Performance;
        
        objQuarterly_Performance.End_Date__c = Date.newInstance(2018, 02, 02);
        update objQuarterly_Performance;
        
	}
}