/*
    Class Name : Quarterly_Performance_TriggerHandler
    Description: Trigger Handler for Quarterly_Performance object
*/
public with sharing class Quarterly_Performance_TriggerHandler 
{
    /*
        Method Name : onBeforeInsert
        Description: This method will call on before insert
    */
    public void onBeforeInsert(List<Quarterly_Performance__c> lstQuarterlyPerformance) {
        populateSalesQuotaMap(lstQuarterlyPerformance);
        updateSalesQuotaLookup(lstQuarterlyPerformance);
        updateCurrencyISOCode(lstQuarterlyPerformance);
    }

    public void onBeforeUpdate(Map<Id, Quarterly_Performance__c> mapOldQuarterlyPerformance, Map<Id, Quarterly_Performance__c> mapNewQuarterlyPerformance) {
        List<Quarterly_Performance__c> lstQuarterlyPerformanceUpdated = new List<Quarterly_Performance__c>();
        Set<Id> setQtrPerformanceIds = mapOldQuarterlyPerformance.keySet();
        for(Id qtrPerfId : setQtrPerformanceIds) {
            if((mapOldQuarterlyPerformance.get(qtrPerfId).Sales_Rep_1__c != mapNewQuarterlyPerformance.get(qtrPerfId).Sales_Rep_1__c)
                || (mapOldQuarterlyPerformance.get(qtrPerfId).Sales_Rep_2__c  != mapNewQuarterlyPerformance.get(qtrPerfId).Sales_Rep_2__c )) {
                    lstQuarterlyPerformanceUpdated.add(mapNewQuarterlyPerformance.get(qtrPerfId));
                }
        }
        populateSalesQuotaMap(lstQuarterlyPerformanceUpdated);
        updateSalesQuotaLookup(lstQuarterlyPerformanceUpdated);
    }

    // Map<Id, Quarterly_Performance__c> mapOldQuarterlyPerformance
    private set<Id> setOppIds = new set<Id>();
    private set<String> setSalesQuotaName = new set<String>();
    private List<Sales_Quota__c> lstSalesQuota;
    public void populateSalesQuotaMap(List<Quarterly_Performance__c> lstQuarterlyPerformance) {
        for(Quarterly_Performance__c objQuarterly_Performance : lstQuarterlyPerformance) {
            if(objQuarterly_Performance.Opportunity__c != null) {
               setOppIds.add(objQuarterly_Performance.Opportunity__c);
            }
            if(objQuarterly_Performance.Sales_Rep_1__c != null) {
                setSalesQuotaName.add(objQuarterly_Performance.Sales_Rep_1__c);
            }
            if(objQuarterly_Performance.Sales_Rep_2__c != null) {
                setSalesQuotaName.add(objQuarterly_Performance.Sales_Rep_2__c);
            }
        }
        lstSalesQuota = new List<Sales_Quota__c>([SELECT Id, StartDate__c, EndDate__c, Name__c FROM Sales_Quota__c 
                                                                            WHERE Name__c IN: setSalesQuotaName]);
    }

    public void updateSalesQuotaLookup(List<Quarterly_Performance__c> lstQuarterlyPerformance) {
        for(Quarterly_Performance__c objQuarterly_Performance : lstQuarterlyPerformance) {
            if(objQuarterly_Performance.Revenue_Split_1_Percent__c == 100 && objQuarterly_Performance.Revenue_Split_2_Percent__c == 0) {
                Sales_Quota__c lookupSalesQuota = getSalesQuotaLookup(objQuarterly_Performance.Sales_Rep_1__c, objQuarterly_Performance.Start_Date__c, objQuarterly_Performance.End_Date__c);
                if(lookupSalesQuota != null) {
                    objQuarterly_Performance.Quota_1__c = lookupSalesQuota.Id;
                }
            }else if(objQuarterly_Performance.Revenue_Split_1_Percent__c < 100 && objQuarterly_Performance.Revenue_Split_2_Percent__c > 0) {
                Sales_Quota__c lookupSalesQuota1 = getSalesQuotaLookup(objQuarterly_Performance.Sales_Rep_1__c, objQuarterly_Performance.Start_Date__c, objQuarterly_Performance.End_Date__c);
                if(lookupSalesQuota1 != null) {
                    objQuarterly_Performance.Quota_1__c = lookupSalesQuota1.Id;
                }
                Sales_Quota__c lookupSalesQuota2 = getSalesQuotaLookup(objQuarterly_Performance.Sales_Rep_2__c, objQuarterly_Performance.Start_Date__c, objQuarterly_Performance.End_Date__c);
                if(lookupSalesQuota2 != null) {
                    objQuarterly_Performance.Quota_2__c = lookupSalesQuota2.Id;
                }
            }
        }
    }

    public Sales_Quota__c getSalesQuotaLookup(String salesQuotaName, Date startDate, Date endDate) {
        Sales_Quota__c lookupSalesQuota;
        for(Sales_Quota__c salesQuotaObj : lstSalesQuota) {
            if(salesQuotaObj.Name__c == salesQuotaName && salesQuotaObj.StartDate__c == startDate && salesQuotaObj.EndDate__c == endDate) {
                lookupSalesQuota = salesQuotaObj;
                return lookupSalesQuota;
            }
        }
        return lookupSalesQuota;
    }
    
    public void updateCurrencyISOCode(List<Quarterly_Performance__c> lstQuarterlyPerformance) {
        Map<Id, Opportunity> oppList = new Map<Id, Opportunity>([select Id, CurrencyIsoCode from Opportunity where id IN :setOppIds]);
        for(Quarterly_Performance__c objQuarterly_Performance : lstQuarterlyPerformance) {
            if(objQuarterly_Performance.Opportunity__c != null) {
            	objQuarterly_Performance.CurrencyIsoCode = oppList.get(objQuarterly_Performance.Opportunity__c).CurrencyIsoCode;   
            }
        }
    }

}