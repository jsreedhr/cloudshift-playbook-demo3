public class DFPOrderIdSyncJob implements Queueable, Database.AllowsCallouts {
    
    private DFPPropsalService.ProposalServiceInterfacePort proposalSer;
    private DFPPropsalLineItemService.ProposalLineItemServiceInterfacePort proposalLineItemSer;

    private final String networkCode = '32730525';
    private final String applicationName = 'kingdfp';
    private String authToken;

    public void execute(QueueableContext context) {
		
        authToken = DfpConnectorMaster.getClientAuth2Token();
        
        proposalSer = new DFPPropsalService.ProposalServiceInterfacePort(authToken);
        proposalSer.RequestHeader =  new DFPPropsalService.SoapRequestHeader();
        proposalSer.RequestHeader.networkCode = networkCode;
        proposalSer.RequestHeader.applicationName = applicationName;

        proposalLineItemSer = new DFPPropsalLineItemService.ProposalLineItemServiceInterfacePort(authToken);
        proposalLineItemSer.RequestHeader =  new DFPPropsalLineItemService.SoapRequestHeader();
        proposalLineItemSer.RequestHeader.networkCode = networkCode;
        proposalLineItemSer.RequestHeader.applicationName = applicationName;

        //get proposal list from dfp
        DFPPropsalService.Statement filterStatement = new DFPPropsalService.Statement();
        String query = 'WHERE dfpOrderId != null';
        filterStatement.query = query;
        DFPPropsalService.ProposalPage proposalPage;

        //get proposallineitem list from dfp
        DFPPropsalLineItemService.Statement lineItemFilterStatement = new DFPPropsalLineItemService.Statement();
        //String lineItemQuery = 'WHERE dfpLineItemId != null';
        String lineItemQuery = 'WHERE proposalId != null';
        lineItemFilterStatement.query = lineItemQuery;
        DFPPropsalLineItemService.ProposalLineItemPage proposalLineItemPage;

        try {
            proposalPage = proposalSer.getProposalsByStatement(filterStatement);
            System.debug('proposalPage count ---- ' + proposalPage.totalResultSetSize);

            proposalLineItemPage = proposalLineItemSer.getProposalLineItemsByStatement(lineItemFilterStatement);
            System.debug('proposalLineItemPage count ---- ' + proposalLineItemPage.totalResultSetSize);
        } catch (Exception e) {
            System.debug('Error in getting proposal and lineitem list from dfp for opportunity sync - error: ' + e.getMessage());
        }

        if(proposalPage.totalResultSetSize == 0 && proposalLineItemPage.totalResultSetSize == 0) {
            System.debug('No proposals and line items found in DFP to Sync.');
            return;
        }
        
        //get opportunity list from sfdc
        List<Opportunity> oppList = new List<Opportunity>([select id, DFP_Order_Id__c, DFP_ProposalId__c from Opportunity where DFP_ProposalId__c != null and  DFP_Order_Id__c = null]);
        if(oppList.size() > 0) {
        
            //sync opportunity wrt proposal
            Map<String, Opportunity> oppProposalIdMap = new Map<String, Opportunity>();
            for(Opportunity opp : oppList) {
                oppProposalIdMap.put(opp.DFP_ProposalId__c, opp);
            }

            List<Opportunity> oppUpdateList = new List<Opportunity>();

            List<DFPPropsalService.Proposal> proposalList = proposalPage.results;
            for(DFPPropsalService.Proposal proposal : proposalList) {
                String proposalId = String.valueOf(proposal.id);
                System.debug('proposalId----'+proposalId);
                Opportunity opp;
                if(oppProposalIdMap.containsKey(proposalId)) {
                    opp = oppProposalIdMap.get(proposalId);
                    opp.DFP_Order_Id__c = String.valueOf(proposal.dfpOrderId);
                    oppUpdateList.add(opp);
                }
            }
            
            //update opportunity list
            if(oppUpdateList.size() > 0) {
                try {
                    update oppUpdateList;
                    System.debug('total updated opportunities --- ' + oppUpdateList.size());
                } catch (Exception e) {
                    System.debug('Error updating opportunityes --- ' + e.getMessage());
                }
            }
        }

        //get opportunitylineitem list from sfdc
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>([select id, DFP_Line_Item_Id__c, DFP_ProposalLineItemId__c from OpportunityLineItem where DFP_ProposalLineItemId__c != null and  DFP_Line_Item_Id__c = null]);
        System.debug('oppli list : ' + oppLineItemList);
        if(oppLineItemList.size() > 0) {
        
            //sync opportunity wrt proposal
            Map<String, OpportunityLineItem> oppLineItemProposalLineItemIdMap = new Map<String, OpportunityLineItem>();
            for(OpportunityLineItem lineItem : oppLineItemList) {
                oppLineItemProposalLineItemIdMap.put(lineItem.DFP_ProposalLineItemId__c, lineItem);
            }

            List<OpportunityLineItem> oppLineItemUpdateList = new List<OpportunityLineItem>();

            List<DFPPropsalLineItemService.ProposalLineItem> proposalLineItemList = proposalLineItemPage.results;
            for(DFPPropsalLineItemService.ProposalLineItem proposalLineItem : proposalLineItemList) {
                String proposalLineItemId = String.valueOf(proposalLineItem.id);
                System.debug('proposal ineitem id : '+proposalLineItemId);
                System.debug('dfpLineItemId ineitem id : '+proposalLineItem.dfpLineItemId);
                OpportunityLineItem oppLineItem;
                if(oppLineItemProposalLineItemIdMap.containsKey(proposalLineItemId)) {
                    oppLineItem = oppLineItemProposalLineItemIdMap.get(proposalLineItemId);
                    if(proposalLineItem.dfpLineItemId != null) {
                        oppLineItem.DFP_Line_Item_Id__c = String.valueOf(proposalLineItem.dfpLineItemId);
                    	oppLineItemUpdateList.add(oppLineItem);   
                    }
                }
            }
            
            //update opportunitylineitem list
            if(oppLineItemUpdateList.size() > 0) {
                try {
                    update oppLineItemUpdateList;
                    System.debug('total updated opportunity line items --- ' + oppLineItemUpdateList.size());
                } catch (Exception e) {
                    System.debug('Error updating opportunity line items --- ' + e.getMessage());
                }
            }
        }

    }
}