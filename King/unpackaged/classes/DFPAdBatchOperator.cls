/**********************************************
* CLASS NAME - DFPAdBatchOperator
* AUTHOR - AMAN SAWHNEY
* PURPOSE - To create the batch methods for
* ad revenue, opportunity line items and 
* opportunities.
* ********************************************/
public class DFPAdBatchOperator {
    
    //method to create revenue after every 24 hrs if the 
    //opportunity order in DFP system is updated
    public static void createAdRevenueDFP(Map<String,OpportunityLineItem> sfdcOppLiMap,List<DFPAdBatchOperator.DFPData> dfpDataList){
        
        //create ad revenue record for all the provided opportunities
        List<Ad_Revenue__c> adrevenueList = new List<Ad_Revenue__c>();
        
        for(DFPData dfpData : dfpDataList){

            //check for the changes
            OpportunityLineItem sfdcOppLi = sfdcOppLiMap.get(dfpData.LINE_ITEM_ID);
            
            //check if some change happened
            //Boolean isChanged = changeHappenedDFP(sfdcOppLi,dfpData);
            Boolean isChanged = false;
            if(sfdcOppLi !=null) {
                System.debug('sfdcOppLi.OpportunityId>>>>>>>>>>>>>>>'+sfdcOppLi.OpportunityId);
                if(dfpData.ORDER_ID == '2321983603') {
                    System.debug('dfpData.ORDER_ID == 2321983603>>>>>>>>>>>>>>>'+dfpData);
                }
            	isChanged = changeHappenedDFP(sfdcOppLi,dfpData);
            }
            
            //if change happened
            if(isChanged){
                Decimal impressions = dfpData.LINE_ITEM_LEVEL_IMPRESSIONS - ((sfdcOppLi.Impressions__c == null) ? 0 : sfdcOppLi.Impressions__c);
                Decimal clicks = dfpData.LINE_ITEM_LEVEL_CLICKS - ((sfdcOppLi.Clicks__c == null) ? 0 : sfdcOppLi.Clicks__c);
                System.debug('opp id ::::: '+sfdcOppLi.OpportunityId);
                System.debug('OpportunityLineItem_Id__c::::::'+sfdcOppLi.Id);
                System.debug('dfpData.LINE_ITEM_LEVEL_IMPRESSIONS :::: '+dfpData.LINE_ITEM_LEVEL_IMPRESSIONS);
                System.debug('impressions sfdc li :::: '+sfdcOppLi.Impressions__c);
                System.debug('impressions change:::::::'+impressions);
                if(impressions > 0 || clicks > 0) {
                    System.debug('change ad revenue :::: oppId : '+sfdcOppLi.OpportunityId);
                    adrevenueList.add(
                        new Ad_Revenue__c(
                            Opportunity__c = sfdcOppLi.OpportunityId,
                            DFP_LineItem_Id__c = dfpData.LINE_ITEM_ID,
                            OpportunityLineItem_Id__c = sfdcOppLi.Id,
                            Impressions__c = impressions,
                            Clicks__c = clicks,
                            //Impressions__c = dfpData.LINE_ITEM_LEVEL_IMPRESSIONS - sfdcOppLi.Impressions__c,
                            //Clicks__c = dfpData.LINE_ITEM_LEVEL_CLICKS - sfdcOppLi.Clicks__c,
                            Source__c  ='DFP',
                            Date__c = system.today().addDays(-1)	
                        ));
                }
               System.debug('adrevenue list ::::: '+adrevenueList);
            }
            
        }
        
        try{
            System.debug('updating ad revenue::::::::');
            //insert if list is not empty
            if(adrevenueList!=null && !adrevenueList.isEmpty()){
                insert adrevenueList;    
            }
            else{
                System.debug('Ad revenue List is empty or null.');
            }
        }
        catch(Exception e){
            system.debug(e.getLineNumber() +'-->'+ e.getMessage());
        }
        
    }
    
    //method to create revenue after every 24 hrs if the 
    //deal in ADX is updated
    public static void createAdRevenueADX(Map<String,Deal__c> sfdcDealMap,List<DFPAdBatchOperator.DFPData> dfpDataList){
        
        //create ad revenue record for all the provided opportunities
        List<Ad_Revenue__c> adrevenueList = new List<Ad_Revenue__c>();
        
        for(DFPData dfpData : dfpDataList){
            
            //check for the changes
            Deal__c sfdcDeal = sfdcDealMap.get(dfpData.AD_EXCHANGE_DEAL_ID);
            System.debug('sfdc deal for adexchange id ----: '+dfpData.AD_EXCHANGE_DEAL_ID + ': is :----'+sfdcDeal);
            //check if some change happened
            Boolean isChanged = false;
            if(sfdcDeal !=null) {
                
            	isChanged = changeHappenedADX(sfdcDeal,dfpData);    
            }
            
            
            //if change happened
            if(isChanged){
                System.debug('creating ad revenue if changed ---- '+sfdcDeal.Id);
                Decimal impressions = dfpData.AD_EXCHANGE_IMPRESSIONS - ((sfdcDeal.No_of_Impressions__c == null) ? 0 : sfdcDeal.No_of_Impressions__c);
                Decimal completions = dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS - ((sfdcDeal.No_of_Completions__c == null) ? 0 : sfdcDeal.No_of_Completions__c);
                Decimal clicks = dfpData.AD_EXCHANGE_CLICKS - ((sfdcDeal.Clicks__c == null) ? 0 : sfdcDeal.Clicks__c);
                if(impressions>0 || completions > 0 || clicks > 0) {
                    System.debug('creating ad revenue for deal id ---- '+sfdcDeal.Id);
                    adrevenueList.add(
                        new Ad_Revenue__c(
                            Opportunity__c = sfdcDeal.OpportunityName__c,
                            Deal__c = sfdcDeal.Id,
                            Advertisers__c = dfpData.AD_EXCHANGE_ADVERTISER_NAME,
                            Impressions__c = impressions,
                            Completions__c = completions,
                            Clicks__c = clicks,
                            //Impressions__c = dfpData.AD_EXCHANGE_IMPRESSIONS - sfdcDeal.No_of_Impressions__c,
                            //Completions__c = dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS - sfdcDeal.No_of_Completions__c,
                            //Clicks__c = dfpData.AD_EXCHANGE_CLICKS - sfdcDeal.Clicks__c,
                            Source__c  ='AdX',
                            Date__c = system.today().addDays(-1)
                        ));
                }
                
            }
            
        }
        
        try{
            //insert if list is not empty
            if(adrevenueList!=null && !adrevenueList.isEmpty()){
                insert adrevenueList;    
            }
            else{
                System.debug('Ad revenue List is empty or null.');
            }
        }
        catch(Exception e){
            system.debug(e.getLineNumber() +'-->'+ e.getMessage());
        }
        
    }
    
    //method to update opportunity line items every 24 hrs
    //with the latest value of DFP data
    public static void updateOpportunityLineItems(Map<String,OpportunityLineItem> sfdcOppLiMap,List<DFPAdBatchOperator.DFPData> dfpDataList){
        
        //update the relevant opp. line items
        List<OpportunityLineItem> updatableOppLineItemList = new List<OpportunityLineItem>();
        
        for(DFPData dfpData : dfpDataList){
            
            OpportunityLineItem sfdcOppLi = sfdcOppLiMap.get(dfpData.LINE_ITEM_ID);
            
            if(sfdcOppLi != null) {
            	updatableOppLineItemList.add(new OpportunityLineItem(
                    Id = sfdcOppLi.Id,
                    StartDate__c = dfpData.LINE_ITEM_START_DATE_TIME,
                    EndDate__c  = dfpData.LINE_ITEM_END_DATE_TIME,
                    Impressions__c =  dfpData.LINE_ITEM_LEVEL_IMPRESSIONS,
                    Completions__c = dfpData.VIDEO_VIEWERSHIP_COMPLETE,
                    Clicks__c = dfpData.LINE_ITEM_LEVEL_CLICKS,
                    CTR__c  = dfpData.LINE_ITEM_LEVEL_CTR,
                    Completion_Rate__c = dfpData.VIDEO_VIEWERSHIP_COMPLETION_RATE
            	));   
            }
        }                                              
        
        try{
            //update the opportunity line items 
            if(updatableOppLineItemList!=null && !updatableOppLineItemList.isEmpty()){
                update updatableOppLineItemList;    
            }
            else{
                System.debug('Opportunity line item list is empty or null.');
            }
        }
        catch(Exception e){
            System.debug('error updating>>>> opportunity line item....');
            system.debug(e.getLineNumber() +'-->'+ e.getMessage());
        }
        
    }
    
    //method to update deals every 24 hrs
    //with the latest value of DFP data
    public static void updateDeals(Map<String,Deal__c> sfdcDealMap,List<DFPAdBatchOperator.DFPData> dfpDataList){
        
        //update the relevant deals
        List<Deal__c> updatableDealList = new List<Deal__c>();
        
        Set<String> adxDealIdSet = new Set<String>();
        for(DFPData dfpData : dfpDataList){
        	adxDealIdSet.add(dfpData.AD_EXCHANGE_DEAL_ID);
        }
        List<DFPAdBatchOperator.DFPData> rollupAdxDataList = new List<DFPAdBatchOperator.DFPData>();
        for(String adDealId : adxDealIdSet) {
            Long totalImpressions = 0;
            Long totalCompletions = 0;
            Long totalClicks = 0;
            Double totalEcpm = 0;
            Double totalCtr = 0;
            Double totalVtr = 0;
        	for(DFPData dfpData : dfpDataList){
                if(dfpData.AD_EXCHANGE_DEAL_ID == adDealId) {
                	totalImpressions = (dfpData.AD_EXCHANGE_IMPRESSIONS != null) ? (totalImpressions + dfpData.AD_EXCHANGE_IMPRESSIONS) : totalImpressions;
                    totalCompletions = (dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS != null) ? (totalCompletions + dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS) : totalCompletions;
                    totalClicks = (dfpData.AD_EXCHANGE_CLICKS != null) ? (totalClicks + dfpData.AD_EXCHANGE_CLICKS) : totalClicks;
                    totalEcpm = (dfpData.AD_EXCHANGE_MATCHED_ECPM != null) ? (totalEcpm + dfpData.AD_EXCHANGE_MATCHED_ECPM) : totalEcpm;
                    totalCtr = (dfpData.AD_EXCHANGE_CTR != null) ? (totalCtr + dfpData.AD_EXCHANGE_CTR) : totalCtr;
                    totalVtr = (dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR != null) ? (totalVtr + dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR) : totalVtr;
                }
        	}
            DFPData rollupAdxData = new DFPData();
            rollupAdxData.AD_EXCHANGE_DEAL_ID = adDealId;
            rollupAdxData.AD_EXCHANGE_IMPRESSIONS = totalImpressions;
            rollupAdxData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS = totalCompletions;
            rollupAdxData.AD_EXCHANGE_CLICKS = totalClicks;
            rollupAdxData.AD_EXCHANGE_MATCHED_ECPM = totalEcpm;
            rollupAdxData.AD_EXCHANGE_CTR = totalCtr;
            rollupAdxData.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR = totalVtr;
            rollupAdxDataList.add(rollupAdxData);
        }
        for(DFPData dfpData : rollupAdxDataList){
            
            Deal__c sfdcDeal = sfdcDealMap.get(dfpData.AD_EXCHANGE_DEAL_ID);
            if(sfdcDeal != null) {
                System.debug('deals duplicate ids---------'+sfdcDeal.Id);
                System.debug('dfp deal rollup data ------- '+dfpData);
                updatableDealList.add(new Deal__c(
                    Id = sfdcDeal.Id,
                    No_of_Impressions__c = dfpData.AD_EXCHANGE_IMPRESSIONS,
                    No_of_Completions__c  = dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS,
                    Clicks__c = dfpData.AD_EXCHANGE_CLICKS,
                    eCPM__c = dfpData.AD_EXCHANGE_MATCHED_ECPM,
                    CTR__c  = dfpData.AD_EXCHANGE_CTR,
                    VTR__c = dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR
                ));                
            }

        }                                              
        
        try{
            //update the deals
            if(updatableDealList!=null && !updatableDealList.isEmpty()){
                update updatableDealList;    
            }
            else{
                System.debug('Deal List is empty or null.');
            }
        }
        catch(Exception e){
            system.debug(e.getLineNumber() +'-->'+ e.getMessage());
        }
        
    }
    
    //method to update opportunities with relevaant DFp values
    public static void updateOpportunity(Map<String,Opportunity> sfdcOppMap,List<DFPAdBatchOperator.DFPData> dfpDataList){
        
        //update the relevant opportunities
        List<Opportunity> updatableOppList = new List<Opportunity>();
        
        Set<Id> oppId = new Set<Id>();
        for(DFPData dfpOrder : dfpDataList){
            System.debug('update opportunity dfp order>>>>>>>'+dfpOrder);
            Opportunity sfdcOpp = sfdcOppMap.get(dfpOrder.ORDER_ID);
            System.debug('update opportunity dfp sfdc opp >>>>>'+sfdcopp);
            if(sfdcOpp != null) {
                if(oppId.contains(sfdcOpp.Id) == false) {
                    updatableOppList.add(new Opportunity(
                        Id = sfdcOpp.Id,
                        NoofImpressions__c = dfpOrder.ORDER_LIFETIME_IMPRESSIONS,
                        NoofClicks__c = dfpOrder.ORDER_LIFETIME_CLICKS,
                        Opportunity_Billing_Name__c = 'Closed Won' 
                    ));   
                }
                oppId.add(sfdcOpp.Id);
				System.debug('updated opp ::::: '+updatableOppList);                
            }

        }
        
        try{
                            System.debug('updating opp--- full before if');
            //update opportunities
            if(updatableOppList!=null && !updatableOppList.isEmpty()){
                System.debug('updating opp--- full inside if');
                update updatableOppList;    
            }
        }
        catch(Exception e){
            System.debug('exception on opp update----'+e.getMessage());
            system.debug(e.getLineNumber() +'-->'+ e.getMessage());
        }
        
    }
    
    //method to check if some change is happened in 24 hrs in order line items
    public static Boolean changeHappenedDFP(OpportunityLineItem sfdcOppli, DFPData dfpData){
        System.debug('change happend dfp : sfdcOppli : dfpData : '+sfdcOppli+':'+dfpData);
        Boolean changeHappened = false;
        if(sfdcOppli.Clicks__c!=dfpData.LINE_ITEM_LEVEL_CLICKS ||
           sfdcOppli.Completion_Rate__c!=dfpData.VIDEO_VIEWERSHIP_COMPLETION_RATE ||
           sfdcOppli.CTR__c!=dfpData.AD_EXCHANGE_CTR ||
           sfdcOppli.Completions__c!=dfpData.VIDEO_VIEWERSHIP_COMPLETE ||
           sfdcOppli.Impressions__c!=dfpData.LINE_ITEM_LEVEL_IMPRESSIONS
          ){
              changeHappened = true;
          }
        
        return changeHappened;
        
    }
    
    //method to check if some change is happened in 24 hrs in order line items
    public static Boolean changeHappenedADX(Deal__c sfdcDeal, DFPData dfpData){
        System.debug('change happend adx : sfdcDeal : dfpData : '+sfdcDeal+':'+dfpData);
        Boolean changeHappened = false;
        if(sfdcDeal.Clicks__c!=dfpData.AD_EXCHANGE_CLICKS ||
           sfdcDeal.No_of_Completions__c!=dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS ||
           sfdcDeal.No_of_Impressions__c!=dfpData.AD_EXCHANGE_IMPRESSIONS
          ){
              changeHappened = true;
          }
        
        return changeHappened;
        
    }
    
    //Wrapper class construct for the DFP data
    public class DFPData{
        public  String ORDER_ID{get;set;}
        public  Long ORDER_LIFETIME_IMPRESSIONS{get;set;}
        public  Long ORDER_LIFETIME_CLICKS{get;set;}
        public  String LINE_ITEM_ID{get;set;}
        public  Long LINE_ITEM_LEVEL_IMPRESSIONS{get;set;}
        public  Long LINE_ITEM_LEVEL_CLICKS{get;set;}
        public  Date LINE_ITEM_START_DATE_TIME{get;set;}
        public  Date LINE_ITEM_END_DATE_TIME{get;set;}
        public  Double LINE_ITEM_LEVEL_CTR{get;set;}
        public  Double VIDEO_VIEWERSHIP_COMPLETION_RATE{get;set;}
        public  Long VIDEO_VIEWERSHIP_COMPLETE{get;set;}
        
        public Long AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS{get;set;}
        public String AD_EXCHANGE_DEAL_ID{get;set;}
        public String AD_EXCHANGE_ADVERTISER_NAME {get;set;}
        public Long AD_EXCHANGE_IMPRESSIONS{get;set;}
        public Long AD_EXCHANGE_CLICKS{get;set;}
        public Double AD_EXCHANGE_MATCHED_ECPM{get;set;}
        public Double AD_EXCHANGE_VIDEO_TRUEVIEW_VTR{get;set;}
        public Double AD_EXCHANGE_CTR{get;set;}
        
    }
}