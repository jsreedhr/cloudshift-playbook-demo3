public class DFPReportProgressCheckerQueable implements Queueable, Database.AllowsCallouts {
	
    public String fromType {get; set;}
    public DFPReportProgressCheckerQueable(String fromType) {
        this.fromType = fromType;
    }
    public void execute(QueueableContext context) {
		Server_Job__c job = Server_Job__c.getInstance();
        
        DFPReportService.ReportServiceInterfacePort rs = new DFPReportService.ReportServiceInterfacePort();
        rs.RequestHeader =  new DFPReportService.SoapRequestHeader();
        rs.RequestHeader.networkCode = '32730525';
        rs.RequestHeader.applicationName = 'kingdfp';
        Long jobId;
        String dfpStatus;
        if(fromType == 'DFP') {
        	jobId = (Long)job.DFP_Job_Id__c;
            dfpStatus = rs.getReportJobStatus(jobId);
        } else if(fromType == 'ADX') {
            jobId = (Long)job.ADX_Job_Id__c;
            dfpStatus = rs.getReportJobStatus(jobId);
        }
        if(dfpStatus == 'COMPLETED') {
            System.debug('reports generated');
            System.enqueueJob(new DFPEnqueProcessJob(fromType));
        } else {
            System.debug('re scheduling');
            DateTime getDateTime = System.now().addMinutes(5);

            String getday = string.valueOf(getDateTime.day());
            String getmonth = string.valueOf(getDateTime.month());
            String gethour = string.valueOf(getDateTime.hour());
            String getminute = string.valueOf(getDateTime.minute());
            String getsecond = string.valueOf(getDateTime.second());
            String getyear = string.valueOf(getDateTime.year());
            
            String strJobDFPGet = 'JobCheckProgress-' + getsecond + '_' + getminute + '_' + gethour + '_' + getday + '_' + getmonth + '_' + getyear;
            String strScheduleGet = '0 ' + getminute + ' ' + gethour + ' ' + getday + ' ' + getmonth + ' ?' + ' ' + getyear;
            if(fromType == 'DFP') {
            	System.schedule('Dfp-'+strJobDFPGet, strScheduleGet, new DFPReportProgressCheckerScheduler('DFP'));   
            } else if(fromType == 'ADX') {
            	System.schedule('Adx-'+strJobDFPGet, strScheduleGet, new DFPReportProgressCheckerScheduler('ADX'));   
            }
        }
    }
    
}