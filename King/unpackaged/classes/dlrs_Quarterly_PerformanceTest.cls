/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Quarterly_PerformanceTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Quarterly_PerformanceTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Quarterly_Performance__c());
    }
}