/**********************************************
* CLASS NAME - DFPEnqueDataJob
* AUTHOR - AMAN SAWHNEY
* PURPOSE - A class to set the job id
* into custom setting.
* ********************************************/
public class DFPEnqueDataJob implements Queueable, Database.AllowsCallouts {
    
    public String fromType{get;set;}
    public DFPEnqueDataJob(String fromType){
        this.fromType = fromType;
    }
    public void execute(QueueableContext context) {
        
        Server_Job__c jobSetting  = Server_Job__c.getInstance(UserInfo.getOrganizationId());
        System.debug('jobSetting-------'+jobSetting);
        if(fromType=='DFP'){
            
            //call orders and line items from the DFP
            Long jobId = DFPServerCallHandler.makeServerCallDFP();
            
            
            if(jobSetting==null || jobSetting.Id==null){
               jobSetting =  new Server_Job__c();
                jobSetting.DFP_Job_Id__c = jobId;
                jobSetting.SetupOwnerId = UserInfo.getOrganizationId();
                insert jobSetting;
            }
            else{
                jobSetting.DFP_Job_Id__c = jobId;
                update jobSetting;
            }
            
        }
        
        else if(fromType=='ADX'){
            
            //call orders and line items from the DFP
            Long jobId = DFPServerCallHandler.makeServerCallADX();
            
            if(jobSetting == null || jobSetting.Id == null){
                jobSetting =  new Server_Job__c();
                jobSetting.ADX_Job_Id__c = jobId;
                jobSetting.SetupOwnerId = UserInfo.getOrganizationId();
                insert jobSetting;
            }
            else{
                jobSetting.ADX_Job_Id__c = jobId;
                update jobSetting;
            }
            
        }
        
        
    }
}