@isTest
global class DFPReportWebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           System.debug('request----'+request);
           System.debug('endpoint----'+endpoint);
           System.debug('soapAction----'+soapAction);
           System.debug('requestName----'+requestName);
           System.debug('responseName----'+responseName);
           System.debug('responseType----'+responseType);
               if(requestName == 'getCompaniesByStatement') {
                   List<DFPCompanyService.Company> comList = new List<DFPCompanyService.Company>();
                   DFPCompanyService.getCompaniesByStatementResponse_element resElement= new DFPCompanyService.getCompaniesByStatementResponse_element();
                   DFPCompanyService.Company com = new DFPCompanyService.Company();
                   com.id = 123456789;
                   com.name = 'test company name from dfp';
                   comList.add(com);
                   DFPCompanyService.CompanyPage comPage = new DFPCompanyService.CompanyPage();
                   comPage.totalResultSetSize = 0;
                   comPage.results = comList;
                   
                   resElement.rval = comPage;
                   response.put('response_x', resElement);   
               } else if(requestName == 'createCompanies') {
                   DFPCompanyService.createCompaniesResponse_element resElement = new DFPCompanyService.createCompaniesResponse_element();
                   DFPCompanyService.Company com = new DFPCompanyService.Company();
                   com.id = 123456789;
                   com.name = 'create test company';
                   List<DFPCompanyService.Company> comList = new List<DFPCompanyService.Company>();
                   comList.add(com);
                   resElement.rval = comList;
                   response.put('response_x', resElement);
               } else if(requestName == 'createOrders'){
                   DFPOrderService.createOrdersResponse_element resElement = new DFPOrderService.createOrdersResponse_element();
                   DFPOrderService.Order ord = new DFPOrderService.Order();
                   ord.id = 123456789;
                   ord.name = 'create order in dfp';
                   List<DFPOrderService.Order> orderList = new List<DFPOrderService.Order>();
                   orderList.add(ord);
                   resElement.rval = orderList;
                   response.put('response_x', resElement);
               } else if(requestName == 'createLineItems'){
                   DFPLineItemService.createLineItemsResponse_element resElement = new  DFPLineItemService.createLineItemsResponse_element();
                   DFPLineItemService.LineItem lItem = new  DFPLineItemService.LineItem();
                   lItem.id = 123456789;
                   lItem.name = 'test line item for dfp';
                   List<DFPLineItemService.LineItem> lItemList = new List<DFPLineItemService.LineItem>();
                   lItemList.add(lItem);
                   resElement.rval = lItemList;
                   response.put('response_x', resElement);
               } else if(requestName == 'runReportJob'){
                   DFPReportService.runReportJobResponse_element resElement = new  DFPReportService.runReportJobResponse_element();
                   DFPReportService.ReportJob rJob = new DFPReportService.ReportJob();
                   resElement.rval = rJob;
                   response.put('response_x', resElement);
               } else if(requestName == 'getReportJobStatus'){
                   DFPReportService.getReportJobStatusResponse_element resElement = new  DFPReportService.getReportJobStatusResponse_element();
                   resElement.rval = 'COMPLETE';
                   response.put('response_x', resElement);
               } else if(requestName == 'getReportDownloadUrlWithOptions'){
                   DFPReportService.getReportDownloadUrlWithOptionsResponse_element resElement = new  DFPReportService.getReportDownloadUrlWithOptionsResponse_element();
                   resElement.rval = 'www.mocktesturl.com';
                   response.put('response_x', resElement);
               } else if(requestName == 'createProposals'){
                   DFPPropsalService.createProposalsResponse_element resElement = new  DFPPropsalService.createProposalsResponse_element();
                   List<DFPPropsalService.Proposal> proposalList = new List<DFPPropsalService.Proposal>();
                   DFPPropsalService.Proposal proposal = new DFPPropsalService.Proposal();
                   proposalList.add(proposal);
                   resElement.rval = proposalList;
                   response.put('response_x', resElement);
               } else if(requestName == 'createProposalLineItems'){
                   DFPPropsalLineItemService.createProposalLineItemsResponse_element resElement = new  DFPPropsalLineItemService.createProposalLineItemsResponse_element();
                   List<DFPPropsalLineItemService.ProposalLineItem> proposalLineItemList = new List<DFPPropsalLineItemService.ProposalLineItem>();
                   DFPPropsalLineItemService.ProposalLineItem proposalLineItem = new DFPPropsalLineItemService.ProposalLineItem();
                   proposalLineItemList.add(proposalLineItem);
                   resElement.rval = proposalLineItemList;
                   response.put('response_x', resElement);
               } else if(requestName == 'getProposalsByStatement'){
                   DFPPropsalService.getProposalsByStatementResponse_element resElement = new  DFPPropsalService.getProposalsByStatementResponse_element();
                   DFPPropsalService.ProposalPage proposalPage = new DFPPropsalService.ProposalPage();
                   proposalPage.totalResultSetSize = 1;
                   List<DFPPropsalService.Proposal> proposalList = new List<DFPPropsalService.Proposal>();
                   DFPPropsalService.Proposal proposalOne = new DFPPropsalService.Proposal();
                   proposalOne.id = 12345;
                   proposalOne.dfpOrderId = 123456;
                   proposalList.add(proposalOne);
                   proposalPage.results = proposalList;
                   resElement.rval = proposalPage;
                   response.put('response_x', resElement);
               } else if(requestName == 'getProposalLineItemsByStatement'){
                   DFPPropsalLineItemService.getProposalLineItemsByStatementResponse_element resElement = new  DFPPropsalLineItemService.getProposalLineItemsByStatementResponse_element();
                   DFPPropsalLineItemService.ProposalLineItemPage proposalLineItemPage = new DFPPropsalLineItemService.ProposalLineItemPage();
                   proposalLineItemPage.totalResultSetSize = 1;
                   List<DFPPropsalLineItemService.ProposalLineItem> proposalLineItemList = new List<DFPPropsalLineItemService.ProposalLineItem>();
                   DFPPropsalLineItemService.ProposalLineItem proposalLineOne = new DFPPropsalLineItemService.ProposalLineItem();
                   proposalLineOne.id = 123456;
                   proposalLineOne.dfpLineItemId = 123456;
                   proposalLineItemList.add(proposalLineOne);
                   proposalLineItemPage.results = proposalLineItemList;
                   resElement.rval = proposalLineItemPage;
                   response.put('response_x', resElement);
               } else {
                   DFPReportService.runReportJobResponse_element resElement = 
                   new DFPReportService.runReportJobResponse_element();
                   DFPReportService.ReportJob rJob= new DFPReportService.ReportJob();
                   rJob.id = 123456789;
                   resElement.rval = rJob;
                   response.put('response_x', resElement);   
               }
   }
}