@isTest
public class DFPAdXIntegrationTest {
	
    private static  final String xmlTestString = '<?xml version="1.0" encoding="UTF-8"?><Report reportTimeStamp="2018-06-08T11:32:24.963+01:00"><HeaderData /><ReportData><ColumnHeaders><ColumnHeader name="lineItemId" localizedName="Line item ID" /><ColumnHeader name="orderName" localizedName="Order" /><ColumnHeader name="orderId" localizedName="Order ID" /><ColumnHeader name="lineitemStartDate" localizedName="Line item start date" /><ColumnHeader name="lineitemEndDate" localizedName="Line item end date" /><ColumnHeader name="orderLifetimeXfpImpressions" localizedName="Order lifetime impressions" /><ColumnHeader name="orderLifetimeXfpClicks" localizedName="Order lifetime clicks" /><ColumnHeader name="totalReservationImpressionsDelivered" localizedName="Total impressions" /><ColumnHeader name="totalReservationClicksDelivered" localizedName="Total clicks" /><ColumnHeader name="complete" localizedName="Complete" /><ColumnHeader name="totalReservationCtrDelivered" localizedName="Total CTR" /><ColumnHeader name="completionRate" localizedName="Completion rate" /></ColumnHeaders><DataSet><Row rowNum="1"><Column name="lineItemId"><Val>99071405</Val></Column><Column name="orderName"><Val>STAGE House Ads</Val></Column><Column name="orderId"><Val>346648685</Val></Column><Column name="lineitemStartDate"><Val>2016-01-05T00:00:00Z</Val></Column><Column name="lineitemEndDate"><Val>Unlimited</Val></Column><Column name="orderLifetimeXfpImpressions"><Val>53,812</Val></Column><Column name="orderLifetimeXfpClicks"><Val>4,624</Val></Column><Column name="totalReservationImpressionsDelivered"><Val>13,162</Val></Column><Column name="totalReservationClicksDelivered"><Val>775</Val></Column><Column name="complete"><Val>12,761</Val></Column><Column name="totalReservationCtrDelivered"><Val>5.89%</Val></Column><Column name="completionRate"><Val>96.95%</Val></Column></Row><Row rowNum="2"><Column name="lineItemId"><Val>99280805</Val></Column><Column name="orderName"><Val>STAGE House Ads</Val></Column><Column name="orderId"><Val>346648685</Val></Column><Column name="lineitemStartDate"><Val>2016-01-08T00:00:00Z</Val></Column><Column name="lineitemEndDate"><Val>Unlimited</Val></Column><Column name="orderLifetimeXfpImpressions"><Val>53,812</Val></Column><Column name="orderLifetimeXfpClicks"><Val>4,624</Val></Column><Column name="totalReservationImpressionsDelivered"><Val>2,000</Val></Column><Column name="totalReservationClicksDelivered"><Val>78</Val></Column><Column name="complete"><Val>1,902</Val></Column><Column name="totalReservationCtrDelivered"><Val>3.90%</Val></Column><Column name="completionRate"><Val>94.96%</Val></Column></Row><Row rowNum="173"><Column name="lineItemId"><Val>4697747724</Val></Column><Column name="orderName"><Val>601_Feedmob - Orbitz - test_75647</Val></Column><Column name="orderId"><Val>2316030628</Val></Column><Column name="lineitemStartDate"><Val>2018-06-06T11:43:00+01:00</Val></Column><Column name="lineitemEndDate"><Val>2018-06-12T23:59:00+01:00</Val></Column><Column name="orderLifetimeXfpImpressions"><Val>236,531</Val></Column><Column name="orderLifetimeXfpClicks"><Val>1,507</Val></Column><Column name="totalReservationImpressionsDelivered"><Val>44,382</Val></Column><Column name="totalReservationClicksDelivered"><Val>198</Val></Column><Column name="complete"><Val>44,069</Val></Column><Column name="totalReservationCtrDelivered"><Val>0.45%</Val></Column><Column name="completionRate"><Val>99.12%</Val></Column></Row><Total rowNum="174"><Column name="lineItemId"><Val /></Column><Column name="orderName"><Val /></Column><Column name="orderId"><Val /></Column><Column name="lineitemStartDate"><Val>-</Val></Column><Column name="lineitemEndDate"><Val>-</Val></Column><Column name="orderLifetimeXfpImpressions"><Val>-</Val></Column><Column name="orderLifetimeXfpClicks"><Val>-</Val></Column><Column name="totalReservationImpressionsDelivered"><Val>211,156,767</Val></Column><Column name="totalReservationClicksDelivered"><Val>2,554,273</Val></Column><Column name="complete"><Val>205,569,861</Val></Column><Column name="totalReservationCtrDelivered"><Val>1.21%</Val></Column><Column name="completionRate"><Val>97.06%</Val></Column></Total></DataSet></ReportData></Report>';
    public static testMethod void DFPXMLResolverTest() {
        Test.startTest();
        DFPXMLResolver.getDFPDataList(xmlTestString);  
        Test.stopTest();
    }
    private static Map<String,Deal__c> sfdcDealMap = new Map<String,Deal__c>();
    private static List<DFPAdBatchOperator.DFPData> dfpDataList = new List<DFPAdBatchOperator.DFPData>();
    
    public static testMethod void createAdRevenueADXTest() {
        DFPAdBatchOperator.DFPData dfpData1 = new DFPAdBatchOperator.DFPData();
        dfpData1.AD_EXCHANGE_ADVERTISER_NAME = 'Nestle';
        dfpData1.AD_EXCHANGE_CLICKS = 1135;
        dfpData1.AD_EXCHANGE_CTR = 0.14;
        dfpData1.AD_EXCHANGE_DEAL_ID = '602838732039569475';
        dfpData1.AD_EXCHANGE_IMPRESSIONS = 380148;
        dfpData1.AD_EXCHANGE_MATCHED_ECPM = 10.6;
        dfpData1.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS = 0;
        dfpData1.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR = 0.0;
        
        DFPAdBatchOperator.DFPData dfpData2 = new DFPAdBatchOperator.DFPData();
        dfpData2.AD_EXCHANGE_ADVERTISER_NAME = 'Activision Blizzard';
        dfpData2.AD_EXCHANGE_CLICKS = 1135;
        dfpData2.AD_EXCHANGE_CTR = 0.14;
        dfpData2.AD_EXCHANGE_DEAL_ID = '602838732038472073';
        dfpData2.AD_EXCHANGE_IMPRESSIONS = 380148;
        dfpData2.AD_EXCHANGE_MATCHED_ECPM = 10.6;
        dfpData2.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS = 0;
        dfpData2.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR = 0.0;
        
        dfpDataList.add(dfpData1);
        dfpDataList.add(dfpData2);
        
        Deal__c deal1 = new Deal__c(  
          Id='a094E0000033Y1NQAU',
          Deal_ID__c='602838732039569475',
          No_of_Impressions__c=3494189.0,
          No_of_Completions__c=0.0,
          Clicks__c=25074.0,
          CTR__c=0.3,
          VTR__c=0.0,
          OpportunityName__c='0064E000006ZWCHQA4'
       	);
        Deal__c deal2 = new Deal__c(  
          Id='a094E0000033Y1NQAU',
          Deal_ID__c='602838732038472073',
          No_of_Impressions__c=3494189.0,
          No_of_Completions__c=0.0,
          Clicks__c=25074.0,
          CTR__c=0.3,
          VTR__c=0.0,
          OpportunityName__c='0064E000006ZWCHQA4'
       	);
        sfdcDealMap.put('602838732039569475', deal1);
        sfdcDealMap.put('602838732038472073', deal2);
        Test.startTest();
        DFPAdBatchOperator.createAdRevenueADX(sfdcDealMap, dfpDataList);
        Test.stopTest();
    }
    
    public static testMethod void testGetJobIdQueable() {
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Test.startTest();
        ID jobID1 = System.enqueueJob(new DFPEnqueDataJob('DFP'));
        ID jobID2 = System.enqueueJob(new DFPEnqueDataJob('ADX'));
        Test.stopTest();
    }
    
    public static testMethod void DFPEnqueProcessJobTest() {
        Test.setMock(WebServiceMock.class, new DFPReportWebServiceMockImpl());
        Test.setMock(HttpCalloutMock.class, new DFPHttpServiceMockImpl());
        Test.startTest();
        ID jobID1 = System.enqueueJob(new DFPEnqueProcessJob('DFP'));
        ID jobID2 = System.enqueueJob(new DFPEnqueProcessJob('ADX'));
        Test.stopTest();
    }
    
    public static testMethod void DFPAdBatchTest() {
        DFPAdBatch dfpAdBatchDFP = new DFPAdBatch('DFP');
    }

}