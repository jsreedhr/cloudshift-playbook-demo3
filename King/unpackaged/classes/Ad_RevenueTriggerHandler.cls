/*
Class Name : Ad_RevenueTriggerHandler
Description: Trigger Handler for Ad Revenue object
*/
public with sharing class Ad_RevenueTriggerHandler
{
    /*
        Method Name : onBeforeInsert
        Description: This method will call on before insert to create Quarterly_Performance__c record and populate the Quarterly_Performance__c field.
    */
    public void onBeforeInsert(List<Ad_Revenue__c> lstAd_Revenue)
    {
        set<Id> setOppIds = new set<Id>();
        for(Ad_Revenue__c objAd_Revenue: lstAd_Revenue)
        {
            setOppIds.add(objAd_Revenue.Opportunity__c);
        }
        map<Id, List<Quarterly_Performance__c>> mapOppIdTolstQuarterlyPerformance = new map<Id, List<Quarterly_Performance__c>>();
        for(Quarterly_Performance__c objQuarterly_Performance: [SELECT Id, Start_Date__c, End_Date__c, Opportunity__c
                                                                  FROM Quarterly_Performance__c
                                                                 WHERE Opportunity__c IN: setOppIds])
        {
             if(!mapOppIdTolstQuarterlyPerformance.containsKey(objQuarterly_Performance.Opportunity__c))
                mapOppIdTolstQuarterlyPerformance.put(objQuarterly_Performance.Opportunity__c, new List<Quarterly_Performance__c>());
            mapOppIdTolstQuarterlyPerformance.get(objQuarterly_Performance.Opportunity__c).add(objQuarterly_Performance);
        }
        List<Quarterly_Performance__c> lstQuarterly_Performance = new List<Quarterly_Performance__c>();
        List<Quarterly_Date_Range__mdt> lstQuarterly_Date_Range = [SELECT Id, Start_Date__c, End_Date__c FROM Quarterly_Date_Range__mdt ORDER BY Start_Date__c];
        for(Ad_Revenue__c objAd_Revenue: lstAd_Revenue)
        {
            if(!mapOppIdTolstQuarterlyPerformance.containsKey(objAd_Revenue.Opportunity__c))
            {
                for(Quarterly_Date_Range__mdt objQDRange: lstQuarterly_Date_Range)
                {
                    if(objQDRange.Start_Date__c <= objAd_Revenue.Date__c && objAd_Revenue.Date__c <= objQDRange.End_Date__c )
                    {
                        lstQuarterly_Performance.add(new Quarterly_Performance__c(Start_Date__c = objQDRange.Start_Date__c, End_Date__c = objQDRange.End_Date__c,
                                                                                            Opportunity__c = objAd_Revenue.Opportunity__c));
                    }
                }
            }
            else
            {
                Boolean checkiftheQuarterly = false;
                for(Quarterly_Performance__c objQuarterly_Performance: mapOppIdTolstQuarterlyPerformance.get(objAd_Revenue.Opportunity__c))
                {
                    if(objQuarterly_Performance.Start_Date__c <= objAd_Revenue.Date__c && objAd_Revenue.Date__c <= objQuarterly_Performance.End_Date__c )
                    {
                        checkiftheQuarterly = true;
                    }
                }
                if(!checkiftheQuarterly)
                {
                    for(Quarterly_Date_Range__mdt objQDRange: lstQuarterly_Date_Range)
                    {
                        if(objQDRange.Start_Date__c <= objAd_Revenue.Date__c && objAd_Revenue.Date__c <= objQDRange.End_Date__c )
                        {
                            lstQuarterly_Performance.add(new Quarterly_Performance__c(Start_Date__c = objQDRange.Start_Date__c, End_Date__c = objQDRange.End_Date__c,
                                                                                                Opportunity__c = objAd_Revenue.Opportunity__c));
                        }
                    }
                }
            }
        }
        insert lstQuarterly_Performance;
        mapOppIdTolstQuarterlyPerformance = new map<Id, List<Quarterly_Performance__c>>();
        for(Quarterly_Performance__c objQuarterly_Performance: [SELECT Id, Start_Date__c, End_Date__c, Opportunity__c
                                                                  FROM Quarterly_Performance__c
                                                                 WHERE Opportunity__c IN: setOppIds])
        {
             if(!mapOppIdTolstQuarterlyPerformance.containsKey(objQuarterly_Performance.Opportunity__c))
                mapOppIdTolstQuarterlyPerformance.put(objQuarterly_Performance.Opportunity__c, new List<Quarterly_Performance__c>());
            mapOppIdTolstQuarterlyPerformance.get(objQuarterly_Performance.Opportunity__c).add(objQuarterly_Performance);
        }
        for(Ad_Revenue__c objAd_Revenue: lstAd_Revenue)
        {
            if(mapOppIdTolstQuarterlyPerformance.containsKey(objAd_Revenue.Opportunity__c))
            {
                for(Quarterly_Performance__c objQuarterly_Performance: mapOppIdTolstQuarterlyPerformance.get(objAd_Revenue.Opportunity__c))
                {
                    if(objQuarterly_Performance.Start_Date__c <= objAd_Revenue.Date__c && objAd_Revenue.Date__c <= objQuarterly_Performance.End_Date__c )
                    {
                        objAd_Revenue.Quarterly_Performance__c = objQuarterly_Performance.Id;
                    }
                }
            }
        }
    }

    /*
        Method Name : onBeforeInsertCopyDealInfo
        Description: This method copies deal info to the ad_revenue before create
    */
    public void onBeforeInsertCopyDealInfo(List<Ad_Revenue__c> lstAd_Revenue) {
        Set<String> dfpLineItemIds = new Set<String>();
        for(Ad_Revenue__c ad_revenue : lstAd_Revenue) {
            System.debug('PMP_Preferred_Deal------'+ad_revenue.Opportunity__r);
            if(ad_revenue.Opportunity_Record_Type__c == 'PMP_Preferred_Deal' || ad_revenue.Opportunity_Record_Type__c == 'PMP_Preferred_Deal_Closed_Won') {
                ad_revenue.Currency_Code__c = ad_revenue.Opportunity_Currency_Code__c;
                ad_revenue.Cost_Structure__c = ad_revenue.Deal_Cost_Structure__c;
            //} else if (ad_revenue.Opportunity_Record_Type__c == 'Direct_Programmatic_Guaranteed' || ad_revenue.Opportunity_Record_Type__c == 'Direct_Programmatic_Guaranteed_Closed_Won') {
            } else if (ad_revenue.Opportunity_Record_Type__c == 'Direct_IO_Programmatic_Guaranteed' || ad_revenue.Opportunity_Record_Type__c == 'Direct_Programmatic_Guaranteed_Closed_Won') {
                dfpLineItemIds.add(ad_revenue.DFP_LineItem_Id__c);
            }
        }
        if(dfpLineItemIds.size() > 0) {
            Map<String, OpportunityLineItem> lineItemMap = new Map<String, OpportunityLineItem>();
            for(OpportunityLineItem lItem : [select Id, CostStructure__c, PlacementName__c, UnitPrice, DFP_Line_Item_Id__c,CurrencyIsoCode from OpportunityLineItem where DFP_Line_Item_Id__c IN :dfpLineItemIds]) {
                if(lItem.DFP_Line_Item_Id__c != null) {
                    lineItemMap.put(lItem.DFP_Line_Item_Id__c, lItem);
                }
            }
            for(Ad_Revenue__c ad_revenue : lstAd_Revenue) {
                if (ad_revenue.Opportunity_Record_Type__c == 'Direct_IO_Programmatic_Guaranteed' || ad_revenue.Opportunity_Record_Type__c == 'Direct_Programmatic_Guaranteed_Closed_Won') {
                    OpportunityLineItem dfpLineItem = lineItemMap.get(ad_revenue.DFP_LineItem_Id__c);
                    if(dfpLineItem != null) {
                        ad_revenue.Cost_Structure__c = dfpLineItem.CostStructure__c;
                        ad_revenue.Currency_Code__c = dfpLineItem.CurrencyIsoCode;
                        ad_revenue.Line_Item_Description__c = dfpLineItem.PlacementName__c;
                        ad_revenue.Rate__c = dfpLineItem.UnitPrice;
                    }
                }
            }
        }
    }
}