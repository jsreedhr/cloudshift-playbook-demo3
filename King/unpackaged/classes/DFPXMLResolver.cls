public class DFPXMLResolver {
    
    public enum DFPTYPE {DOUBLE_T,LONG_T, STRING_T, PERCENT_T,CURRENCY_T, DATE_T}
    
    public static List<DFPAdBatchOperator.DFPData> getDFPDataList(String xmlResponse){
        System.debug('xml response>>>>'+xmlResponse);
        List<DFPAdBatchOperator.DFPData> dfpDataList = new List<DFPAdBatchOperator.DFPData>();
        DOM.Document doc=new DOM.Document();
        doc.load(xmlResponse);
        DOM.XmlNode rootNode=doc.getRootElement();
        Dom.XmlNode reportData = rootNode.getChildElement('ReportData', null);
        Dom.XmlNode dataSet = reportData.getChildElement('DataSet', null);
        List<Dom.XmlNode> rowList = dataSet.getChildElements();
        
        for(Dom.XmlNode row : rowList){
            System.debug('row name : '+row.getName());
            System.debug('row node type : '+row.getNodeType());
            DFPAdBatchOperator.DFPData dfpData = new DFPAdBatchOperator.DFPData();
            if(row.getName() == 'Row') {
            for(Dom.XmlNode column : row.getChildElements()){
                
                String value = column.getChildElement('Val', null).getText();
                
                if('lineItemId'==column.getAttribute('name',null)){
                    dfpData.LINE_ITEM_ID = value;    
                }
                
                else if('orderId'==column.getAttribute('name',null)){
                    dfpData.ORDER_ID = value;  
                }
                else if('lineitemStartDate'==column.getAttribute('name', null)) {
                    dfpData.LINE_ITEM_START_DATE_TIME = Date.valueOf(getValue(value,DFPTYPE.DATE_T));
                }
                else if('lineitemEndDate'==column.getAttribute('name', null)) {
                    if(value != 'Unlimited') {
						dfpData.LINE_ITEM_START_DATE_TIME = Date.valueOf(getValue(value,DFPTYPE.DATE_T));
                    }
                }
                else if('orderLifetimeXfpImpressions'==column.getAttribute('name',null)){
                    dfpData.ORDER_LIFETIME_IMPRESSIONS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));
                }
                
                else if('orderLifetimeXfpClicks'==column.getAttribute('name',null)){
                    dfpData.ORDER_LIFETIME_CLICKS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('totalReservationImpressionsDelivered'==column.getAttribute('name',null)){
                    dfpData.LINE_ITEM_LEVEL_IMPRESSIONS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('totalReservationClicksDelivered'==column.getAttribute('name',null)){
                    dfpData.LINE_ITEM_LEVEL_CLICKS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('totalReservationCtrDelivered'==column.getAttribute('name',null)){
                    dfpData.LINE_ITEM_LEVEL_CTR = Double.valueOf(getValue(value,DFPTYPE.PERCENT_T));    
                }
                else if('completionRate' == column.getAttribute('name',null)){
                    dfpData.VIDEO_VIEWERSHIP_COMPLETION_RATE = Double.valueOf(getValue(value,DFPTYPE.PERCENT_T));    
                }
                else if('complete' == column.getAttribute('name',null)){
                    dfpData.VIDEO_VIEWERSHIP_COMPLETE = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                
                else if('ADX_VIDEO_TRUEVIEWS' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VIEWS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('ADX_DEAL_ID' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_DEAL_ID = value;    
                } 
                else if('ADX_ADVERTISER_NAME' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_ADVERTISER_NAME = value;    
                }
                else if('ADX_AD_IMPRESSIONS_ADJUSTED' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_IMPRESSIONS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('ADX_CLICKS' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_CLICKS = Long.valueOf(getValue(value,DFPTYPE.LONG_T));    
                }
                else if('ADX_ECPM' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_MATCHED_ECPM = Double.valueOf(getValue(value,DFPTYPE.CURRENCY_T));    
                }
                else if('ADX_VIDEO_VTR' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_VIDEO_TRUEVIEW_VTR = Double.valueOf(getValue(value,DFPTYPE.PERCENT_T));    
                }
                else if('ADX_MATCHED_QUERIES_CTR' == column.getAttribute('name',null)){
                    dfpData.AD_EXCHANGE_CTR = Double.valueOf(getValue(value,DFPTYPE.PERCENT_T));    
                }
                
                
            }
                if(dfpData.AD_EXCHANGE_DEAL_ID != 'Open auction') {
                	dfpDataList.add(dfpData);   
                }
            }
            System.debug('dfpData>>>>>>'+dfpData);
            
        }
        System.debug('dfpDataList >>>>>'+dfpDataList);
        return dfpDataList;
        
    }
    
    public static String getValue( String value , DFPTYPE dfpType){
        
        if(value=='-' || value=='' || value==null){
            
            if(dfpType == DFPXMLResolver.DFPType.PERCENT_T || 
               dfpType == DFPXMLResolver.DFPType.CURRENCY_T||
               dfpType == DFPXMLResolver.DFPType.DOUBLE_T ||
               dfpType == DFPXMLResolver.DFPType.LONG_T){
                   value = '0';
               }
            else{
                value = null;    
            }
            
        }
        else {
            
            if(dfpType == DFPXMLResolver.DFPType.PERCENT_T){
                value = value.substring(0, value.length()-1);
            }
            else if(dfpType == DFPXMLResolver.DFPType.CURRENCY_T){
                value = value.substring(1, value.length()-1);
            }
            else if(dfpType == DFPXMLResolver.DFPType.LONG_T){
                value = value.remove(',');
            }
        }
        
        return value;
    }
    
}