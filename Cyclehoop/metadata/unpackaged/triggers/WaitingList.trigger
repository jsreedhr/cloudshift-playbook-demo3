trigger WaitingList on Waiting_List__c (before insert) {
    
    if(Trigger.isBefore){
        
        if(Trigger.isInsert){
            
            //to fill the contact in waiting list if contact is empty and email of contact already exist in system
            WaitingListTriggerFunctions.checkdupecontact(Trigger.new);
            
            //to fill inventory in waiting list if it is empty and inventory address already exist in system. 
            WaitingListTriggerFunctions.fillInventory(Trigger.new);
            
        }
        
    }
    
}