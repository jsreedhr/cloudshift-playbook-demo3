trigger Inventory on Inventory__c (before update,after update,after insert) {
    
    if(Trigger.isBefore){
        
        if(Trigger.isUpdate){
            
            system.debug('isUpdate---->');
            
        }
        
        if(Trigger.isInsert){
            system.debug('InventoryWork---->');
            //to remove spaces in postal code
            InventoryWork.stripSpaces(Trigger.New);
        }
        
    }
    
    else{
        
        if(Trigger.isUpdate){
            
            //subsidy code
            if(BlockActivator.inventorySubsidiesCreated==false){
                
                //set the block activator to run this code only once
                BlockActivator.inventorySubsidiesCreated = true;
                
                InventoryTriggerFunctions.createSubsidies(Trigger.NewMap, Trigger.OldMap);    
            }
            
            //Auto allocate allocation to space, map Waiting Lists to Historic Waiting Lists
            //if(BlockActivator.inventoryhistoricWLCreated == false)
            //InventoryTriggerFunctions.createHistoricWaitingList(Trigger.new); 
            
            //auto allocation code
            if(BlockActivator.inventoryAutoAllocationDone == false){
                
                //set the block activator to run this code only once
                BlockActivator.inventoryAutoAllocationDone = true;
                
                //create a cllone of this list
                List<inventory__c> inventoryList = [select Id,name,Borough_Payment_Frequency__c,RecordType.DeveloperName,
                                                    AllowedNumberofTenants__c,axOversubscribeAmount__c,(select Id from Spaces__r
                                                    where Status__c='Available'),
                                                    (select Id,Name,Contact__c,WaitingListException__c,DateRequested__c,Inventory__c,
                                                     Suggested_Location_Comments__c,TenantAdditionalComments__c,Priority__c,
                                                     Suggested_Location__c from Interested_Parties__r) 
                                                    from Inventory__c where Id In:Trigger.New AND No_Spaces_Available__c > 0 AND  
                                                    Stage__c='Installed' AND ReadyforAutoAllocation__c=true];
                
                //pass the list to autoallocate
                InventoryTriggerFunctions.autoAllocation(inventoryList);
            
            }
            
            
            //auto maintainence code if maintainence is not completed
            //for this inventory
            if(BlockActivator.inventoryMaintainenceCompleted == false){
                
                //set the block activator to run this code only once
                BlockActivator.inventoryMaintainenceCompleted = true;
                
                InventoryTriggerFunctions.autoMaintainence(Trigger.NewMap);    
            }
            
        }
        
        if(Trigger.isInsert){
            
            if(BlockActivator.inventoryMaintainenceCompleted == false){
                
                //set the block activator to run this code only once
                BlockActivator.inventoryMaintainenceCompleted = true;
                
                InventoryTriggerFunctions.autoMaintainence(Trigger.NewMap);    
            }
            
        }
        
    }
    
}