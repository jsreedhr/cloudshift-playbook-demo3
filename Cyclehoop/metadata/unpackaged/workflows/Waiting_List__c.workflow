<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_Response_to_Suggest_a_Location</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>Auto Response to Suggest a Location</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Suggest_a_Location_Auto_Response</template>
    </alerts>
    <rules>
        <fullName>Waiting List - Suggest a Location Created</fullName>
        <actions>
            <name>Auto_Response_to_Suggest_a_Location</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Waiting_List__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Suggested Location Record Type</value>
        </criteriaItems>
        <description>Auto email response when a Suggest a Location is Created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
