<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <tasks>
        <fullName>Email_Credit_Card_due_to_Expire_email_sent</fullName>
        <assignedTo>angela@cyclehoop.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Email: Credit Card due to Expire email sent</subject>
    </tasks>
</Workflow>
