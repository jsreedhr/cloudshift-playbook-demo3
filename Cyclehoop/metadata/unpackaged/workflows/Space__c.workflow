<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Space_Status_is_Full</fullName>
        <field>Status__c</field>
        <literalValue>Full</literalValue>
        <name>Space Status is Full</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Space_Update_Status_to_Available</fullName>
        <field>Status__c</field>
        <literalValue>Available</literalValue>
        <name>Space -Update Status to Available</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Space - Marked as Full when Active allocations are full</fullName>
        <actions>
            <name>Space_Status_is_Full</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the No. Active Allocations is greater than 1 update the status to Full</description>
        <formula>AND( 
NOT($Setup.Admin_Override__c.ValidationRules__c), 
				
				OR(
								
AND(
				InventoryType__c = &quot;Cycle Hub&quot;,
    NoActiveAllocations__c = (Inventory__r.AllowedNumberofTenants__c + Inventory__r.axOversubscribeAmount__c )
    ),

AND(
OR(
			 InventoryType__c =&quot;Bikehangar&quot;, 
    InventoryType__c = &quot;Bikelocker&quot;, 
    InventoryType__c = &quot;Cycle Shelter&quot;, 
    InventoryType__c = &quot;Cycle Room&quot;), 

NoActiveAllocations__c  = 1
    )
				
								) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Space -Available Status No related Active Allocations</fullName>
        <actions>
            <name>Space_Update_Status_to_Available</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Space__c.NoActiveAllocations__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <description>When the No. Active Allocations is 0 update the status to Available</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
