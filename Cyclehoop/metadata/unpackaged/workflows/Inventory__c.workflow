<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Inventory_Location_Cancelled_Alert</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>Inventory Location Cancelled Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Inventory_Location_Canncelled</template>
    </alerts>
    <alerts>
        <fullName>Inventory_Location_Confirmed_Alert</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>Inventory Location Confirmed Alert</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Inventory_Location_Confirmed</template>
    </alerts>
    <alerts>
        <fullName>Inventory_installed_and_ready_for_allocation</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>Inventory installed and ready for allocation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Inventory_Installed</template>
    </alerts>
    <alerts>
        <fullName>Inventory_scheduled_for_installation</fullName>
        <ccEmails>marcelle@cloudshiftgroup.com</ccEmails>
        <description>Inventory scheduled for installation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Inventory_Installation_Scheduled</template>
    </alerts>
    <fieldUpdates>
        <fullName>Inventory_Location_Proposed</fullName>
        <description>Updates the the inventory status to location proposed</description>
        <field>Stage__c</field>
        <literalValue>Location Proposed</literalValue>
        <name>Inventory - Location Proposed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Deposit</fullName>
        <description>Updates the deposit with the standard values - hangars 25 / locker 20 / Rooms 25 / Hubs 0 / Shelters  blank</description>
        <field>Deposit__c</field>
        <formula>CASE( RecordType.DeveloperName, &quot;Bikehangar&quot;, 25, &quot;Bikelocker&quot;, 20, &quot;Cycle_Hub&quot;,0, &quot;Cycle_Room&quot;,25,0)</formula>
        <name>Update Deposit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Inventory - Address Updated Status Update</fullName>
        <actions>
            <name>Inventory_Location_Proposed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the status of the inventory when any part of the address is updated from blank</description>
        <formula>AND(
     ISBLANK(PRIORVALUE(Post_Code__c)),
     NOT(ISBLANK(Post_Code__c))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inventory - Installed %26 ready for allocations</fullName>
        <actions>
            <name>Inventory_installed_and_ready_for_allocation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory__c.Stage__c</field>
            <operation>equals</operation>
            <value>Installed</value>
        </criteriaItems>
        <description>Auto email alert to rentals team to inform inventory is installed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inventory - Location confirmed</fullName>
        <actions>
            <name>Inventory_Location_Confirmed_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory__c.Stage__c</field>
            <operation>equals</operation>
            <value>Location Confirmed</value>
        </criteriaItems>
        <description>Auto email alert to rentals team to inform of &apos;confirmed location&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inventory - Scheduled for Installation</fullName>
        <actions>
            <name>Inventory_scheduled_for_installation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inventory__c.Stage__c</field>
            <operation>equals</operation>
            <value>Scheduled Installation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Inventory__c.ScheduledInstallationDate__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Auto email alert to rentals team to inform of  scheduled installation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Inventory - previously confirmed and now cancelled</fullName>
        <actions>
            <name>Inventory_Location_Cancelled_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto email alert to rentals team if an inventory record got to &apos;confirmed location&apos; and then was &apos;Cancelled&apos;</description>
        <formula>AND(
    ISCHANGED (Stage__c),
    ISPICKVAL(PRIORVALUE(Stage__c),&quot;Location Confirmed&quot;), 
    ISPICKVAL(Stage__c, &quot;Cancelled&quot;)
				)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inventory - setting default deposit amount</fullName>
        <actions>
            <name>Update_Deposit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an inventory is created set the standard deposit amount</description>
        <formula>AND(
				NOT (RecordType.DeveloperName = &quot;Cycle_Shelter&quot;),
 TRUE
				)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
