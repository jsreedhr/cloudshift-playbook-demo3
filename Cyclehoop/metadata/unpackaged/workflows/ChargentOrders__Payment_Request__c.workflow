<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Payment_Request_Annual_Subscriptions</fullName>
        <description>Payment Request Annual Subscriptions</description>
        <protected>false</protected>
        <recipients>
            <field>ChargentOrders__Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Annual_Application_Email_with_Payment_link</template>
    </alerts>
    <alerts>
        <fullName>Payment_Request_Monthly_Subscriptions</fullName>
        <description>Payment Request Monthly Subscriptions</description>
        <protected>false</protected>
        <recipients>
            <field>ChargentOrders__Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Monthly_Application_Email_with_Payment_link</template>
    </alerts>
    <rules>
        <fullName>Payment Request Email</fullName>
        <actions>
            <name>Payment_Request_Annual_Subscriptions</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.ChargentOrders__Send_Payment_Request_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.ChargentOrders__Status__c</field>
            <operation>notEqual</operation>
            <value>Paid</value>
        </criteriaItems>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.Payment_Frequency__c</field>
            <operation>equals</operation>
            <value>Annually</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Payment Request Email Monthly Subscriptions</fullName>
        <actions>
            <name>Payment_Request_Monthly_Subscriptions</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.ChargentOrders__Send_Payment_Request_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.ChargentOrders__Status__c</field>
            <operation>notEqual</operation>
            <value>Paid</value>
        </criteriaItems>
        <criteriaItems>
            <field>ChargentOrders__Payment_Request__c.Payment_Frequency__c</field>
            <operation>equals</operation>
            <value>Monthly</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
