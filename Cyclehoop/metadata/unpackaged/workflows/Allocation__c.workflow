<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Camden_Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_approval</fullName>
        <description>Camden Email Notification to Lead Tenant their Allocation is pending approval</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Pending_Approval_Camden</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Lead_Tenant_their_Allocation_has_been_declined</fullName>
        <description>Email Notification to Lead Tenant their Allocation has been declined</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Declined</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Lead_Tenant_their_Allocation_has_had_the_keys_tags_sent</fullName>
        <description>Email Notification to Lead Tenant their Allocation has had the keys / tags sent</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Official_welcome_email_for_bikehangar_or_bikelocker</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_approval</fullName>
        <description>Email Notification to Lead Tenant their Allocation is pending approval</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Pending_Approval</template>
    </alerts>
    <alerts>
        <fullName>Finsbury_Park_Email_Notification_to_Lead_Tenant_their_Allocation_has_had_the_key</fullName>
        <description>Finsbury Park Email Notification to Lead Tenant their Allocation has had the keys / tags sent</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Finsbury_Park_Cycle_Hub_welcome_email</template>
    </alerts>
    <alerts>
        <fullName>Hackney_Housing_Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_ap</fullName>
        <description>Hackney Housing  Email Notification to Lead Tenant their Allocation is pending approval</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Pending_Approval_Hackney_Housing</template>
    </alerts>
    <alerts>
        <fullName>Metropolitan_Housing_Bikehangars_Email_Notification_to_Lead_Tenant_their_Allocat</fullName>
        <description>Metropolitan Housing Bikehangars Email Notification to Lead Tenant their Allocation is pending approval</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Pending_Approval_Camden_Metropolitan_Housing</template>
    </alerts>
    <alerts>
        <fullName>Southwark_Bikelockers_Email_Notification_to_Lead_Tenant_their_Allocation_is_pend</fullName>
        <description>Southwark Bikelockers Email Notification to Lead Tenant their Allocation is pending approval</description>
        <protected>false</protected>
        <recipients>
            <field>LeadTenant__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Rentals_Team_Emails/Application_Pending_Approval_Southwark_Council_Bikelockers</template>
    </alerts>
    <fieldUpdates>
        <fullName>Allocation_Status_Expired</fullName>
        <field>Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Allocation Status Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_End_Date_to_Today</fullName>
        <field>EndDate__c</field>
        <formula>Today()</formula>
        <name>Update End Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Payment_Frequency</fullName>
        <field>RentPaymentFrequency__c</field>
        <literalValue>Monthly</literalValue>
        <name>Update Payment Frequency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Space_ID</fullName>
        <field>Space_ID__c</field>
        <formula>Space__r.Id</formula>
        <name>Update Space ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Tenant_Id</fullName>
        <field>Lead_Tenant_ID__c</field>
        <formula>LeadTenant__r.Id</formula>
        <name>Update Tenant Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Allocation - Camden new allocation waiting approval</fullName>
        <actions>
            <name>Camden_Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Emails the lead tenant when they have been allocated a space and its waiting Rentals Approval for Camden</description>
        <formula>AND(
			ISPICKVAL(Status__c,&quot;Draft&quot;),
	  Renewal__c = False,
Space__r.ClientBorough__c = &quot;Camden&quot;
				)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Finsbury Park Keys%2FTags sent to Customer</fullName>
        <actions>
            <name>Finsbury_Park_Email_Notification_to_Lead_Tenant_their_Allocation_has_had_the_key</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Keys/Tags sent to Customer has been ticked for the allocation for Finsbury Park</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Active let&quot;), 
Renewal__c = False, 
TagsKeysSent__c = True,
Space__r.ClientBorough__c = &quot;Finsbury Park&quot;
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Hackney Housing new allocation waiting approval</fullName>
        <actions>
            <name>Hackney_Housing_Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_ap</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Emails the lead tenant when they have been allocated a space and its waiting Rentals Approval for Hackney Housing</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Draft&quot;), 
Renewal__c = False, 
 Space__r.ClientBorough__c = &quot;Hackney Housing&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Is pending Payment for 7 days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Allocation__c.Status__c</field>
            <operation>equals</operation>
            <value>Pending Payment</value>
        </criteriaItems>
        <criteriaItems>
            <field>Allocation__c.Renewal__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When an allocation (not a renewal) is not paid for 7 days the let will expire</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Allocation_Status_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Allocation - Keys Returned%2FNever Returned Update Status</fullName>
        <actions>
            <name>Allocation_Status_Expired</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_End_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Allocation__c.Keys_Returned__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Allocation__c.Key_Never_Returned__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When the Keys Returned Checkbox or Keys Never Returned Checkbox updated the Space becomes available</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Keys%2FTags sent to Customer</fullName>
        <actions>
            <name>Email_Notification_to_Lead_Tenant_their_Allocation_has_had_the_keys_tags_sent</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Keys/Tags sent to Customer has been ticked for the allocation</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Active let&quot;), 
Renewal__c = False, 
TagsKeysSent__c = True,
NOT(Space__r.ClientBorough__c = &quot;Finsbury Park&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Metropolitan Housing Bikehanger new allocation waiting approval</fullName>
        <actions>
            <name>Metropolitan_Housing_Bikehangars_Email_Notification_to_Lead_Tenant_their_Allocat</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Emails the lead tenant when they have been allocated a space and its waiting Rentals Approval Metropolitan Housing Bikehangars</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Draft&quot;), 
Renewal__c = False, 
Space__r.ClientBorough__c = &quot;Metropolitan Housing&quot;, 
Space__r.InventoryType__c = &quot;Bikehangars&quot; 
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Southwark Bikelocker new allocation waiting approval</fullName>
        <actions>
            <name>Southwark_Bikelockers_Email_Notification_to_Lead_Tenant_their_Allocation_is_pend</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Emails the lead tenant when they have been allocated a space and its waiting Rentals Approval Southwark Bikelockers</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Draft&quot;), 
Renewal__c = False, 
Space__r.ClientBorough__c = &quot;Southwark Council&quot;, 
Space__r.InventoryType__c = &quot;Bikelocker&quot; 
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - Updates allocation update Ids</fullName>
        <actions>
            <name>Update_Space_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Tenant_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates Tenant &amp; Space Ids on the allocation record on create for batches</description>
        <formula>AND(
				NOT(ISNEW()),
				OR(
	ISCHANGED(Space__c),
	ISCHANGED(LeadTenant__c )
				))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - allocation has been declined</fullName>
        <actions>
            <name>Email_Notification_to_Lead_Tenant_their_Allocation_has_been_declined</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Allocation__c.Status__c</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>Emails the lead tenant when they have been allocated a space has been declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - created allocation update Ids</fullName>
        <actions>
            <name>Update_Space_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Tenant_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates Tenant &amp; Space Ids on the allocation record on create for batches</description>
        <formula>OR(
				NOT(ISBLANK(Space__c)),
	NOT(ISBLANK(LeadTenant__c ))
				)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - new allocation waiting approval</fullName>
        <actions>
            <name>Email_Notification_to_Lead_Tenant_their_Allocation_is_pending_approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Emails the lead tenant when they have been allocated a space and its waiting Rentals Approval</description>
        <formula>AND( 
ISPICKVAL(Status__c,&quot;Draft&quot;), 
Renewal__c = False, 
NOT( 
OR( Space__r.ClientBorough__c = &quot;Hackney Housing&quot;, 
Space__r.ClientBorough__c = &quot;Camden&quot;, 
AND( 
Space__r.ClientBorough__c = &quot;Southwark Council&quot;, 
Space__r.InventoryType__c = &quot;Bikelocker&quot; 
),
AND( 
Space__r.ClientBorough__c = &quot;Metropolitan Housing&quot;, 
Space__r.InventoryType__c = &quot;Bikehangar&quot;)
		)) 
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Allocation - transition to monthly update</fullName>
        <actions>
            <name>Update_Payment_Frequency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Allocation__c.Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Allocation__c.RentPaymentFrequency__c</field>
            <operation>equals</operation>
            <value>Annually</value>
        </criteriaItems>
        <criteriaItems>
            <field>Allocation__c.Transferring_to_Monthly_on_Renewal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>updates the payment frequency to monthly when transitioned to monthly</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Allocation__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
