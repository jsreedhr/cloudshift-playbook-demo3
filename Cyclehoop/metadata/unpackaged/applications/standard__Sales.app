<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-Product2</tab>
    <tab>standard-Forecasting3</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-File</tab>
    <tab>standard-Quote</tab>
    <tab>Space__c</tab>
    <tab>MaintenanceVisit__c</tab>
    <tab>Allocation__c</tab>
    <tab>Subsidy__c</tab>
    <tab>Waiting_List__c</tab>
</CustomApplication>
