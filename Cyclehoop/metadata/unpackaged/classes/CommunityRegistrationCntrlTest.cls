/**
 *  @Class Name:    CommunityRegistrationCntrlTest
 *  @Description:   This is a test class for CommunityRegistrationCntrl used in CommunityRegistration_vf page
 *  @Company:       Standav
 *  CreatedDate:    02/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aman Sawhney           02/03/2018                  Original Version
 */
@isTest
private class CommunityRegistrationCntrlTest {

	private static testMethod void testCreateUser() {
        CommunityRegistrationCntrl.RegistrationInfo info = new CommunityRegistrationCntrl.RegistrationInfo();
        info.firstName = 'FirstName';
        info.lastName = 'LastName';
        info.email = 'test@force.com';
        info.nickName = 'test';
        info.password = 'abcd1234';
        info.confirmPassword = 'abcd1234';
        
        Test.startTest();
            CommunityRegistrationCntrl.registerUser(info);
            system.debug('Status---->'+CommunityRegistrationCntrl.registerUser(info));
            // registerUser will always return true when the user is successfully registered.
            System.assertEquals(CommunityRegistrationCntrl.registerUser(info).status,false);    
        Test.stopTest();
	}

    private static testMethod void testCreateUserNullPassword() {
        CommunityRegistrationCntrl.RegistrationInfo info = new CommunityRegistrationCntrl.RegistrationInfo();
        info.firstName = 'FirstName';
        info.lastName = 'LastName';
        info.email = 'test@force.com';
        info.nickName = 'test';
        info.password = '';
        info.confirmPassword = '';
        
        Test.startTest();
            CommunityRegistrationCntrl.registerUser(info);
            system.debug('Status---->'+CommunityRegistrationCntrl.registerUser(info));
            // registerUser will always return true when the user is successfully registered.
            System.assertEquals(CommunityRegistrationCntrl.registerUser(info).status,false);    
        Test.stopTest();
	}
	
	/*private static testMethod void testCreateUserNullInfo() {
        CommunityRegistrationCntrl.RegistrationInfo info = new CommunityRegistrationCntrl.RegistrationInfo();
        info.firstName = '';
        info.lastName = '';
        info.email = '';
        info.nickName = '';
        info.password = '';
        info.confirmPassword = '';
        
        Test.startTest();
            CommunityRegistrationCntrl.registerUser(info);
            system.debug('Status---->'+CommunityRegistrationCntrl.registerUser(info));
            // registerUser will always return true when the user is successfully registered.
            System.assertEquals(CommunityRegistrationCntrl.registerUser(info).status,false);    
        Test.stopTest();
	}*/
}