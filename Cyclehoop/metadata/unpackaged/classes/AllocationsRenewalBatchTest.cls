/**
 *  @Class Name:    AllocationsRenewalBatchTest
 *  @Description:   This is a test class for AllocationsRenewalBatch 
 *  @Company:       Standav
 *  CreatedDate:    15/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Swetha TG           15/03/2018                  Original Version
 */
@isTest
public class AllocationsRenewalBatchTest {
static Space__c space;
static Allocation__c alloc;
    
    @isTest
	private static void createData(){
        //Inserting personal account record
        List<Account> accountList = CommonTestDataFactory.createAccounts(1,true);
        system.assertNotEquals(accountList, null);
		 //Inserting Contract record
        List<Contract> contractList =  CommonTestDataFactory.createContracts(1,false);
        contractList[0].AccountId = accountList[0].Id;
        insert contractList;        
        //Inserting Inventory record
        List<Inventory__c> inventoryList = CommonTestDataFactory.createInventories(1,false);
        inventoryList[0].Current_Contract__c = contractList[0].Id;
        insert inventoryList;
        
			space = new Space__c(
				name='1',
                inventory__c=inventoryList[0].id
			);
			insert space;
        alloc = new Allocation__c(
           space__c= space.id,
            status__c='Active Let',
            EndDate__c = System.today().addDays(-1) ,
            Termination_Requested_Date__c = NULL
        );
        insert alloc;
        
        }
    @isTest
	private static void AllocationsRenewalBatchTest(){
		createData();
		Test.StartTest();
        AllocationsRenewalBatch batch = new AllocationsRenewalBatch();
     	Database.executeBatch(batch);
		Test.stopTest();
       
	}
     @isTest
	private static void AllocationsRenewalBatchSchedulerTest(){
     Test.startTest();
        AllocationsRenewalBatchScheduler scheduler = new AllocationsRenewalBatchScheduler();
        scheduler.execute(null);
        Test.stopTest();
    }
}