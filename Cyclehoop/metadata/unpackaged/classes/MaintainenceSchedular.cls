/***************************************************
* Author        : Aman Sawhney
* Class Name    : MaintainenceSchedular
* Date Created  : 27 - Feb - 2018
* Purpose       : This schedular should run daily to check if 
*				  the maintainence record is existing on 
*				  inventory or not.
* *************************************************/
public class MaintainenceSchedular implements Schedulable {
    
    public void execute(SchedulableContext SC) {
        
        String inventoryQuery  = 'select Id,Name,Current_Contract__r.EndDate,ActualInstallationDate__c,'+
            'RecordType.DeveloperName,'+
            '(select Id from Maintenance_Visits__r where Planned_Date__c>=TODAY AND Actual_Date__c=null) '+
            'from Inventory__c where MM_Provider__c=\'Cyclehoop\' AND Stage__c=\'Installed\' AND '+ 
            'ActualInstallationDate__c!=null AND Current_Contract__c!=null AND Current_Contract__r.EndDate!=null';
        
        //make an instance of batch class with required query
        InventoryMaintainenceBatch maintainenceBatch = new InventoryMaintainenceBatch(inventoryQuery);
        
        //execute the batch
        database.executeBatch(maintainenceBatch);
        
    }
    
}