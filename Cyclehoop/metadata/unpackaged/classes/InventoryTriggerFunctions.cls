public without sharing class InventoryTriggerFunctions {
    
    public static void createSubsidies(Map < Id,Inventory__c > inventoryNewMap, Map < Id,Inventory__c > inventoryOldMap) {
        
        //Declare the variables
        List < Subsidy__c > lst_subs = new List < Subsidy__c > ();
        
        //for all inventories, check criteria for subsidy and create subsidy list to be inserted
        for(Id inventoryId : inventoryNewMap.keySet()){
            
            Inventory__c oldInventory = inventoryOldMap.get(inventoryId);
            Inventory__c newInventory = inventoryNewMap.get(inventoryId);
            
            //check for the installed status and actual installation date is filled
            if( (oldInventory.Stage__c != newInventory.Stage__c) && 
               newInventory.Stage__c=='Installed' && newInventory.ActualInstallationDate__c!=null){
                   
                   //check for subsidy amount and term
                   if(newInventory.Originating_Subsidy_Amount__c!=null && newInventory.Originating_Subsidy_Term__c!=null &&
                      newInventory.Current_Contract__c != null){
                          
                          Subsidy__c s = new Subsidy__c();
                          s.Amount__c = newInventory.Originating_Subsidy_Amount__c;
                          s.End_Date__c = newInventory.ActualInstallationDate__c.addMonths
                              (Integer.valueof(newInventory.Originating_Subsidy_Term__c)); 
                          s.Contract__c = newInventory.Current_Contract__c;
                          s.Start_Date__c = newInventory.ActualInstallationDate__c;
                          s.Status__c = 'Active';
                          s.Inventory__c = inventoryId;
                          lst_subs.add(s);
                          
                      }
                   
               }
            
        }
        
        //insert the subsidies
        if(lst_subs!=null && !lst_subs.isEmpty()){
            insert lst_subs;
        }
    }
   
    //creation of auto maintainence code
    public static void autoMaintainence(Map < Id, Inventory__c > newInventoryMap) {
        
        //basic initializations
        List < Inventory__c > newInventoryList = null;
        Set < Id > newInventoryIdSet = null;
        
        //if new inventory map is not null
        if (newInventoryMap != null && !newInventoryMap.keySet().isEmpty()) {
            
            newInventoryIdSet = new Set < Id > ();
            Inventory__c inventory = null;
            
            //filter all the Id's of inventory which are of
            //cyclehoop and having active contract
            for (Id newInventoryId: newInventoryMap.keySet()) {
                inventory = newInventoryMap.get(newInventoryId);
                if (inventory.MM_Provider__c == 'Cyclehoop' && inventory.Stage__c == 'Installed' &&
                    inventory.ActualInstallationDate__c != null && inventory.Current_Contract__c != null) {
                        newInventoryIdSet.add(newInventoryId);
                    }
            }
            
            //fetch these inventories
            newInventoryList = [select Id, Name, Current_Contract__r.EndDate, Current_Contract__r.StartDate,
                                ActualInstallationDate__c, RecordType.DeveloperName,
                                (select Id, Planned_Date__c from Maintenance_Visits__r where Planned_Date__c >= TODAY AND Actual_Date__c = null AND Planned_Date__c != null)
                                from Inventory__c
                                where Id In: newInventoryIdSet
                               ];
            
            //call maintainence code
            InventoryWork.maintainInventories(newInventoryList);
            
        }
        
    }
    
    //creation of auto allocation
    public static void autoAllocation(List<Inventory__c> newInvntoryList){
        
        //map for inventory wrapper
        Map<Id,InventoryWork.InventoryInfo> inventoryInfoMap = new Map<Id,InventoryWork.InventoryInfo>();
        
        //map for all waiting lists with common contact
        Map<Id,List<Waiting_List__c>> reflectiveWaitingListMap = new Map<Id,List<Waiting_List__c>>(); 
        
        //reflective waiting list contact Id set
        Set<Id> contactIdSet = new Set<Id>();
        
        //make inventory wrapper list
        for(Inventory__c inv : newInvntoryList){
            
            InventoryWork.InventoryInfo invInfo = new InventoryWork.InventoryInfo();
            invInfo.inventory = inv;
            
            //make waiting list wrapper to override sort
            List<WaitingListCmpWrapper> waitingLstWrapperList = new List<WaitingListCmpWrapper>();
            
            //if waiting list is not empty
            if(inv.Interested_Parties__r!=null && !inv.Interested_Parties__r.isEmpty()){
                for(Waiting_List__c wl : inv.Interested_Parties__r){
                    
                    WaitingListCmpWrapper wlWrapper = new WaitingListCmpWrapper(wl);
                    waitingLstWrapperList.add(wlWrapper);
                	contactIdSet.add(wl.Contact__c);
                }    
            }
            invInfo.waitingLstWrpList = waitingLstWrapperList;
            
            //add inventory to map
            inventoryInfoMap.put(inv.id,invInfo);
        }
        
        //get all the waiting list
        List<Waiting_List__c> waitingLstList  = [select Id,name,WaitingListException__c,Contact__c,
                                                 DateRequested__c,Inventory__c,Suggested_Location_Comments__c,
                                                 TenantAdditionalComments__c,Priority__c,Suggested_Location__c  
                                                 from Waiting_List__c where WaitingListException__c=false AND 
                                                 Contact__c IN:contactIdSet LIMIT 10000];
        
        //aggrigate the waiting list records as per contactId
        for(Waiting_List__c wl : waitingLstList){
            if(wl.Contact__c!=null){
                //if key not exist
                if(!reflectiveWaitingListMap.containskey(wl.Contact__c)){
                    reflectiveWaitingListMap.put(wl.Contact__c,new List<Waiting_List__c>());
                }
                reflectiveWaitingListMap.get(wl.Contact__c).add(wl);
            }
            
        }
        
        //call the autoallocation code
        InventoryWork.allocate(inventoryInfoMap, reflectiveWaitingListMap);
        
    }

}