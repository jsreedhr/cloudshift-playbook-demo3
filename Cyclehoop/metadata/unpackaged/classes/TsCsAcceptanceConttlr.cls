public class TsCsAcceptanceConttlr {
    List<ChargentOrders__Payment_Request__c > lstPayReq = new  List<ChargentOrders__Payment_Request__c> ();
    public Boolean showFinsbury {get;set;} 
    public Boolean showOther {get;set;} 
    
    public TsCsAcceptanceConttlr(){
        
        String payRegID =ApexPages.currentPage().getParameters().get('payReqId');
        String invClientBorough =ApexPages.currentPage().getParameters().get('InventoryClientBorough');
        
        /*
        if(invClientBorough=='Finsbury Park'){
            showFinsbury=true;
            showOther=false;
        }
        
        else{
            showFinsbury=false;
            showOther=true;
        }*/
        
        showFinsbury=false;
            showOther=true;
        
        try {

            if(String.isNotBlank(payRegID)){
            lstPayReq = [select Payment_URL_Modified__c from ChargentOrders__Payment_Request__c where id = :payRegID];
       		 }

            }
            catch (System.NullPointerException e) {
        }     
    }
    
    public PageReference payMentRedirect(){
        if(lstPayReq!=null && !lstPayReq.isEmpty() && lstPayReq[0].Payment_URL_Modified__c!=null){
            String paymentUrl = lstPayReq[0].Payment_URL_Modified__c;
            PageReference pageRef = new PageReference(paymentUrl); 
            return pageRef;
        }
        else{
            return null;
        }
        
    }
    
    
}