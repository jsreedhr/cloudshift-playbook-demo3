/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Waiting_ListTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Waiting_ListTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Waiting_List__c());
    }
}