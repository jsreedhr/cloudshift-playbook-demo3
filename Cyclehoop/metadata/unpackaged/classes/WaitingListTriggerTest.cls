@isTest
public class WaitingListTriggerTest {
    
    @isTest
    public static void testsetupMethod1(){
       
        List<Waiting_List__c> listWaitingListRecords = new List<Waiting_List__c>();

        Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
        
        Account testAccount = new Account();
        testAccount.FirstName = 'testAccount firstname';
        testAccount.LastName = 'testAccount lastname';
        testAccount.BillingStreet ='test billing street';
        testAccount.BillingCity ='test billing city';
        testAccount.BillingCountry ='test billing country';
        testAccount.BillingState = 'test billing state';
        testAccount.PersonEmail ='testemail@gmail.com';
        testAccount.RecordTypeId = personAccountrecordTypeId;
        insert testAccount;
        
        /*Account testAccount1 = new Account();
        testAccount1.FirstName = 'testAccount firstname';
        testAccount1.LastName = 'testAccount lastname';
        testAccount1.BillingStreet ='test billing street';
        testAccount1.BillingCity ='test billing city';
        testAccount1.BillingCountry ='test billing country';
        testAccount1.BillingState = 'test billing state';
        testAccount1.PersonEmail ='testemail2@gmail.com';
        testAccount1.RecordTypeId = personAccountrecordTypeId;
        insert testAccount1;*/
        
        
              
        Waiting_List__c testWaitingListRecord = new Waiting_List__c();
		testWaitingListRecord.First_Name__c = 'test first name';
		testWaitingListRecord.Last_Name__c = 'test last name';
		testWaitingListRecord.Email__c = 'testemail@gmail.com';
        testWaitingListRecord.Building_Name_Number__c ='123';
		testWaitingListRecord.Street__c = 'test street';
		testWaitingListRecord.City__c = 'test city';
		testWaitingListRecord.Country__c = 'test country';
		testWaitingListRecord.State__c = 'test state';
		testWaitingListRecord.Post_Code__c = '12345';
        testWaitingListRecord.DateRequested__c = system.today();
		listWaitingListRecords.add(testWaitingListRecord);
		
        
        Waiting_List__c testWaitingListRecord1 = new Waiting_List__c();
		testWaitingListRecord1.First_Name__c = 'test first name1';
		testWaitingListRecord1.Last_Name__c = 'test last name1';
		testWaitingListRecord1.Email__c = 'testemail1@gmail.com';
		testWaitingListRecord1.Street__c = 'test street1';
		testWaitingListRecord1.City__c = 'test city1';
		testWaitingListRecord1.Country__c = 'test country1';
		testWaitingListRecord1.State__c = 'test state1';
		testWaitingListRecord1.Post_Code__c = '123456';
        testWaitingListRecord1.DateRequested__c = system.today();
		listWaitingListRecords.add(testWaitingListRecord1);
        
        Waiting_List__c testWaitingListRecord2 = new Waiting_List__c();
		testWaitingListRecord2.First_Name__c = 'test first name2';
		testWaitingListRecord2.Last_Name__c = 'test last name2';
		testWaitingListRecord2.Email__c = 'testemail@gmail.com';
		testWaitingListRecord2.Street__c = 'test street2';
		testWaitingListRecord2.City__c = 'test city2';
		testWaitingListRecord2.Country__c = 'test country2';
		testWaitingListRecord2.State__c = 'test state2';
		testWaitingListRecord2.Post_Code__c = '1234567';
        testWaitingListRecord2.DateRequested__c = system.today();
		listWaitingListRecords.add(testWaitingListRecord2);
        
		Test.startTest(); 
		insert listWaitingListRecords; 
        Test.stopTest();
           
    }
    @isTest
    public static void testsetupMethod2(){
        Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
        
        Account testAccount2 = new Account();
        testAccount2.FirstName = 'testAccount firstname';
        testAccount2.LastName = 'testAccount lastname';
        testAccount2.BillingStreet ='test billing street';
        testAccount2.BillingCity ='test billing city';
        testAccount2.BillingCountry ='test billing country';
        testAccount2.BillingState = 'test billing state';
        testAccount2.PersonEmail ='test1@gmail.com';
        testAccount2.RecordTypeId = personAccountrecordTypeId;
        insert testAccount2;
        
        Waiting_List__c testWaitingListRecord3 = new Waiting_List__c();
		testWaitingListRecord3.First_Name__c = 'test first name2';
		testWaitingListRecord3.Last_Name__c = 'test last name2';
		testWaitingListRecord3.Email__c = 'test@gmail.com';
        testWaitingListRecord3.Building_Name_Number__c='111';
		testWaitingListRecord3.Street__c = 'test street2';
		testWaitingListRecord3.City__c = 'test city2';
		testWaitingListRecord3.Country__c = 'test country2';
		testWaitingListRecord3.State__c = 'test state2';
		testWaitingListRecord3.Post_Code__c = '1234567';
        testWaitingListRecord3.DateRequested__c = system.today();
        
        Test.startTest();
 		insert testWaitingListRecord3;
        Test.stopTest();
    }
    
}