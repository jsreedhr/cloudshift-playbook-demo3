/**
 *  @Class Name:    HangerPlotCntrlTest
 *  @Description:   This is a test class for HangerPlotCntrl used in HangerPlot_vf page
 *  @Company:       Standav
 *  CreatedDate:    02/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aman Sawhney           02/03/2018                  Original Version
 */
@isTest
private class HangerPlotCntrlTest {

    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        //Inserting personal account record
        List<Account> accountList = CommonTestDataFactory.createAccounts(1,true);

        //Inserting contact record
        List<Contact> contactList = CommonTestDataFactory.createContacts(1,true);
        
        //Inserting Inventory record
        List<Inventory__c> inventoryList = CommonTestDataFactory.createInventories(1,false);
        inventoryList[0].Post_Code__c = 'LS1 3DD';
        insert inventoryList;
        
        //Inserting Space record
        List<Space__c> spaceList =  CommonTestDataFactory.createSpaces(1,false);
        spaceList[0].Inventory__c = inventoryList[0].Id;
        insert spaceList;
        
        //Inserting Waiting List record
        List<Waiting_List__c> waitingList =  CommonTestDataFactory.createWaitingLists(1,false);
        waitingList[0].Contact__c = contactList[0].Id;
        
    }

	private static testMethod void testStdController() {
        
        Test.startTest();
            HangerPlotCntrl controller = new HangerPlotCntrl();
            Id recordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Bikehangar').getRecordTypeId();
        	HangerPlotCntrl.getPoints('LS1 3DD', new String[]{'Bikehanger'});
        	HangerPlotCntrl.getAllPoints();
            Inventory__c inventory = [Select Id From Inventory__c Where Post_Code__c = 'LS1 3DD'];
            HangerPlotCntrl.FormData data = new HangerPlotCntrl.FormData();
            data.firstName = 'Test';
            data.lastName = 'Account_0';
            data.street = 'test street';
            data.city = 'test city';
            data.country = 'test country';
            data.state = 'test state';
            data.postalCode = 'LS1 3DD';
            data.suggestedStreet = 'test street';
            data.suggestedCity = 'test city';
            data.suggestedCountry = 'test country';
            data.suggestedState = 'test state';
            data.suggestedPostalCode = 'LS1 3DD';
            data.suggestedBuilding = 'suggested Building';
            data.email = 'test@email.com';
            data.phone = '999-999-1000';
            data.location = 'location';
            data.dateOfRequest = system.today();
            data.inventoryId = inventory.Id;
            data.comments = 'comments';
        	data.signUp = false;
            HangerPlotCntrl.applyforWaitingList(data);
            HangerPlotCntrl.applyforAllocation(data);
            HangerPlotCntrl.suggestLocation(data);
        Test.stopTest();
	}
	
	private static testMethod void testEmptyEmail() {
        Test.startTest();
            HangerPlotCntrl controller = new HangerPlotCntrl();
            HangerPlotCntrl.getPoints('LS1 3DD', null);
            Inventory__c inventory = [Select Id From Inventory__c Where Post_Code__c = 'LS1 3DD'];
            HangerPlotCntrl.FormData data = new HangerPlotCntrl.FormData();
            data.firstName = 'Test';
            data.lastName = 'Account_0';
            data.street = ' street';
            data.city = ' city';
            data.country = ' country';
            data.state = ' state';
            data.postalCode = 'LS1 3DD';
            data.suggestedStreet = ' street';
            data.suggestedCity = ' city';
            data.suggestedCountry = ' country';
            data.suggestedState = ' state';
            data.suggestedPostalCode = 'LS1 3DD';
            data.suggestedBuilding = 'suggested Building';
            data.phone = '999-999-1000';
            data.location = 'location';
            data.dateOfRequest = system.today();
            data.inventoryId = inventory.Id;
            data.comments = 'comments';
        	data.signUp = true;
        	
            HangerPlotCntrl.applyforWaitingList(data);
            HangerPlotCntrl.applyforAllocation(data);
            HangerPlotCntrl.suggestLocation(data);
        Test.stopTest();
	}
	
	private static testMethod void testEmptyPersonAccountList() {
        Test.startTest();
            Inventory__c inventory = [Select Id From Inventory__c Where Post_Code__c = 'LS1 3DD'];
            ApexPages.StandardController sc = new ApexPages.StandardController(inventory);
            HangerPlotCntrl controller = new HangerPlotCntrl(sc);
            HangerPlotCntrl.getPoints('LS1 3DD', null);
        	HangerPlotCntrl.getPoints('LS1 3DD', new String[]{'Bikehanger'});
            HangerPlotCntrl.FormData data = new HangerPlotCntrl.FormData();
            HangerPlotCntrl.applyforWaitingList(data);
            HangerPlotCntrl.applyforAllocation(data);
            HangerPlotCntrl.suggestLocation(data);
        Test.stopTest();
	}
	
	private static testMethod void testException() {
        Test.startTest();
            Inventory__c inventory = [Select Id From Inventory__c Where Post_Code__c = 'LS1 3DD'];
            ApexPages.StandardController sc = new ApexPages.StandardController(inventory);
            HangerPlotCntrl controller = new HangerPlotCntrl(sc);
            HangerPlotCntrl.getPoints('LS1 3DD', null);
            HangerPlotCntrl.FormData data = new HangerPlotCntrl.FormData();
            data.phone = 'wrong phone format';
            data.dateOfRequest = Date.newInstance( 2014, 1, 1 );
            HangerPlotCntrl.applyforWaitingList(data);
            HangerPlotCntrl.applyforAllocation(data);
            HangerPlotCntrl.suggestLocation(data);
        Test.stopTest();
	}
	
	private static testMethod void testInvalidRecordType() {
        Test.startTest();
            Inventory__c inventory = [Select Id From Inventory__c Where Post_Code__c = 'LS1 3DD'];
            ApexPages.StandardController sc = new ApexPages.StandardController(inventory);
            HangerPlotCntrl controller = new HangerPlotCntrl(sc);
        	HangerPlotCntrl.getPoints('LS1 3DD', new String[]{'addeew'});
            HangerPlotCntrl.FormData data = new HangerPlotCntrl.FormData();
            data.phone = 'wrong phone format';
            HangerPlotCntrl.applyforWaitingList(data);
            HangerPlotCntrl.applyforAllocation(data);
            HangerPlotCntrl.suggestLocation(data);
        Test.stopTest();
	}
}