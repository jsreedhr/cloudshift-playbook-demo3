/*
 *  Class Name:  AutoAllocationForAvailableSpceBatch
 *  Description: This batch class auto create the allocation for available spaces
 *  Company:     dQuotient
 *  CreatedDate: 20-May-2018
 */
global class AutoAllocationForAvailableSpceBatch implements Database.Batchable < sObject > {
    Set < Id > invIdSet = new Set < Id > ();
    Integer updatedInvCount = 0;
    Set < Id > invIdSetToDisplay = new Set < Id > ();

   /* global AutoAllocationForAvailableSpceBatch(Set < Id > invIdSet) {
     }*/

    global Database.QueryLocator start(Database.BatchableContext BC) {
        Set < Id > invIdSet = invIdSet;
        String queryString = 'Select Id,No_Spaces_Available__c,Stage__c,ReadyforAutoAllocation__c,Borough_Payment_Frequency__c From Inventory__c';
        return Database.getQueryLocator(queryString);
    }

    global void execute(Database.BatchableContext BC, List < Inventory__c > invList) {
        List < Historic_Waiting_List__c > lstHWLToInsertOnOtherInverntories = new List < Historic_Waiting_List__c > ();
        List < Waiting_List__c > lstWLToDelOnOtherInverntories = new List < Waiting_List__c > ();
        List < Historic_Waiting_List__c > lstHWLToInsert = new List < Historic_Waiting_List__c > ();
        List < Space__c > lstAvailableSpace = new List < Space__c > ();
        List < Waiting_List__c > lstWLToDelete = new List < Waiting_List__c > ();
        List < Waiting_List__c > lstWLToDeleteFinal = new List < Waiting_List__c > ();
        Set < id > invIds = new Set < id > ();
        List < Waiting_List__c > lstWL = new List < Waiting_List__c > ();
        List < Allocation__c > allToSpaceLst = new List < Allocation__c > ();
        List < Allocation__c > finalAllToInsert = new List < Allocation__c > ();
        Map < id, Waiting_List__c > spaceWLMap = new Map < id, Waiting_List__c > ();
        Map < id, Waiting_List__c > WLContactMap = new Map < id, Waiting_List__c > ();
        Map < id, id > WLIDsMap = new Map < id, id > ();
        Map < id, string > rentalFreq = new Map < id, string > ();
        Integer spaceSize;
        Integer waitingLstSize;
        
       // System.debug('-------invIdSet----'+invIdSet);
        System.debug('-------invList----'+invList);

        for (Inventory__c objInv: invList) {
            if (objInv.No_Spaces_Available__c > 0 && objInv.Stage__c == 'Installed' && objInv.ReadyforAutoAllocation__c == true) {
                invIds.add(objInv.id);
                rentalFreq.put(objInv.id, objInv.Borough_Payment_Frequency__c);
                invIdSetToDisplay.add(objInv.id);

            }
        }

        lstWL = [Select id, Priority__c, Suggested_Location__c, DateRequested__c, contact__c, Inventory__c, Building_Name_Number__c, City__c, Street__c, Post_Code__c, Country__c from Waiting_List__c where Inventory__c in: invIds order by DateRequested__c asc];
        lstAvailableSpace = [Select id, name, Inventory__r.RecordType.Name from Space__c where Inventory__c in: invIds and status__c = 'Available'
            order by name asc
        ];

        spaceSize = lstAvailableSpace.size();
        waitingLstSize = lstWL.size();

        System.debug('--spaceSize--' + spaceSize);
        System.debug('--waitingLstSize--' + waitingLstSize);


        if (!lstWL.isEmpty() && !lstAvailableSpace.isEmpty()) {
            //Make list WaitingList in Order (Priority,Suggested Location, Oldest WL)
            for (Waiting_List__c w: [Select id, Priority__c, Suggested_Location__c, DateRequested__c, contact__c, Inventory__c, Building_Name_Number__c, City__c, Street__c, Post_Code__c, Country__c from Waiting_List__c where Inventory__c in: invIds and Priority__c = true order by DateRequested__c asc]) {
                lstWLToDelete.add(w);
            }
            for (Waiting_List__c w2: [Select id, Priority__c, Suggested_Location__c, DateRequested__c, contact__c, Inventory__c, Building_Name_Number__c, City__c, Street__c, Post_Code__c, Country__c from Waiting_List__c where Inventory__c in: invIds and Suggested_Location__c = true order by DateRequested__c asc]) {
                lstWLToDelete.add(w2);
            }
            for (Waiting_List__c w3: [Select id, Priority__c, Suggested_Location__c, DateRequested__c, contact__c, Inventory__c, Building_Name_Number__c, City__c, Street__c, Post_Code__c, Country__c from Waiting_List__c where Inventory__c in: invIds and Suggested_Location__c != true and Priority__c != true order by DateRequested__c asc]) {
                lstWLToDelete.add(w3);
            }
        }

        //ALL TO SPACE
        if (!lstWL.isEmpty() && !lstAvailableSpace.isEmpty()) {
            Integer i2 = 0;
            for (Space__c spc: [Select id, Inventory__c, Name, Inventory__r.RecordType.Name, Inventory__r.AllowedNumberofTenants__c, Inventory__r.axOversubscribeAmount__c from Space__c where Inventory__c in: invIds and status__c = 'Available'
                    order by name asc
                ]) {
                if (spc.Inventory__r.RecordType.Name == 'Cycle Hub') {
                    Decimal noOfAciveTenants = 0;
                    if (spc.Inventory__r.axOversubscribeAmount__c != null)
                        noOfAciveTenants = spc.Inventory__r.AllowedNumberofTenants__c + spc.Inventory__r.axOversubscribeAmount__c;
                    else
                        noOfAciveTenants = spc.Inventory__r.AllowedNumberofTenants__c;
                    //  System.debug('--CycleHub-noOfAciveTenants---' + noOfAciveTenants);
                    for (integer i = 0; i < noOfAciveTenants; i++) {
                        if (allToSpaceLst.size() <= waitingLstSize) {
                            // System.debug('--CycleHubForLoop---' + i);
                            Allocation__c allcSp = new Allocation__c();
                            allcSp.space__c = spc.id;
                            allcSp.RentPaymentFrequency__c = rentalFreq.get(spc.Inventory__c);
                            allToSpaceLst.add(allcSp);
                        }
                    }

                } else {
                    if (allToSpaceLst.size() <= waitingLstSize) {
                        Allocation__c allcSp = new Allocation__c();
                        allcSp.space__c = spc.id;
                        allcSp.RentPaymentFrequency__c = rentalFreq.get(spc.Inventory__c);
                        allToSpaceLst.add(allcSp);
                    }
                }
            }

        }

        //Map WL to HWL, delete WL/insert HWL
        // Historic_Waiting_List__c objhWL;
        Integer i = 0;
        if (!allToSpaceLst.isEmpty() && !lstAvailableSpace.isEmpty()) {
            if (allToSpaceLst.size() < waitingLstSize) {

                for (Waiting_List__c wl: lstWLToDelete) {
                    if (i < allToSpaceLst.size()) {
                        Historic_Waiting_List__c objhWL = new Historic_Waiting_List__c(contact__c = wl.contact__c, Inventory__c = wl.Inventory__c, Original_Date_Requested__c = wl.DateRequested__c, Priority__c = wl.Priority__c, Suggested_Location__c = wl.Suggested_Location__c, Status__c = 'Offered', Building_Name_Number__c = wl.Building_Name_Number__c, City__c = wl.City__c, Street__c = wl.Street__c, Post_Code__c = wl.Post_Code__c, Country__c = wl.Country__c);
                        WLContactMap.put(wl.contact__c, wl);
                        WLIDsMap.put(wl.id, wl.id);
                        if (objhWL != null)
                            lstHWLToInsert.add(objhWL);
                        lstWLToDeleteFinal.add(wl);
                        i++;
                    }
                }
            }
            if (allToSpaceLst.size() > waitingLstSize) {
                //  Integer i = 0;
                for (Waiting_List__c wl: lstWLToDelete) {
                    if (i < waitingLstSize) {
                        Historic_Waiting_List__c objhWL = new Historic_Waiting_List__c(contact__c = wl.contact__c, Inventory__c = wl.Inventory__c, Original_Date_Requested__c = wl.DateRequested__c, Priority__c = wl.Priority__c, Suggested_Location__c = wl.Suggested_Location__c, Status__c = 'Offered', Building_Name_Number__c = wl.Building_Name_Number__c, City__c = wl.City__c, Street__c = wl.Street__c, Post_Code__c = wl.Post_Code__c, Country__c = wl.Country__c);
                        WLContactMap.put(wl.contact__c, wl);
                        WLIDsMap.put(wl.id, wl.id);
                        if (objhWL != null)
                            lstHWLToInsert.add(objhWL);
                        lstWLToDeleteFinal.add(wl);
                        i++;
                    }
                }
            }
            if (allToSpaceLst.size() == waitingLstSize) {
                for (Waiting_List__c wl: lstWLToDelete) {
                    Historic_Waiting_List__c objhWL = new Historic_Waiting_List__c(contact__c = wl.contact__c, Inventory__c = wl.Inventory__c, Original_Date_Requested__c = wl.DateRequested__c, Priority__c = wl.Priority__c, Suggested_Location__c = wl.Suggested_Location__c, Status__c = 'Offered', Building_Name_Number__c = wl.Building_Name_Number__c, City__c = wl.City__c, Street__c = wl.Street__c, Post_Code__c = wl.Post_Code__c, Country__c = wl.Country__c);
                    WLContactMap.put(wl.contact__c, wl);
                    WLIDsMap.put(wl.id, wl.id);
                    if (objhWL != null)
                        lstHWLToInsert.add(objhWL);
                    lstWLToDeleteFinal.add(wl);
                }
            }
            Integer i2 = 0;
            for (Allocation__c allc: allToSpaceLst) {
                //For Nor or allc to  <= WL  on inventory
                if (allToSpaceLst.size() <= waitingLstSize) {
                    if (i2 < allToSpaceLst.size()) {
                        Waiting_List__c wl = lstWLToDelete[i2];

                        //  System.debug('--final-1-'+lstWLToDelete[i2]+'---'+i2);

                        allc.LeadTenant__c = wl.Contact__c;
                        allc.Date_Requested__c = wl.DateRequested__c;
                        finalAllToInsert.add(allc);
                        i2++;
                    }
                }

                //For Nor of spaces > WL on inventory
                if (allToSpaceLst.size() > waitingLstSize) {

                    if (i2 < waitingLstSize) {
                        Waiting_List__c wl = lstWLToDelete[i2];
                        // System.debug('--final-1-'+lstWLToDelete[i2]+'---');
                        allc.LeadTenant__c = wl.Contact__c;
                        allc.Date_Requested__c = wl.DateRequested__c;
                        finalAllToInsert.add(allc);
                        i2++;
                    }
                }
            }

            //Deleteing Waiting_List__c across Inventories and creating Historic_Waiting_List__c for Contacts processed till now
            for (Waiting_List__c wl: [select Priority__c, Suggested_Location__c, DateRequested__c, contact__c, Inventory__c, Building_Name_Number__c, City__c, Street__c, Post_Code__c, Country__c from Waiting_List__c where contact__c in: WLContactMap.keyset() and WaitingListException__c = false]) {
                if (!WLIDsMap.containsKey(wl.id)) {
                    Historic_Waiting_List__c objhWL1 = new Historic_Waiting_List__c(contact__c = wl.contact__c, Inventory__c = wl.Inventory__c, Original_Date_Requested__c = wl.DateRequested__c, Priority__c = wl.Priority__c, Suggested_Location__c = wl.Suggested_Location__c, Status__c = 'Offered', Building_Name_Number__c = wl.Building_Name_Number__c, City__c = wl.City__c, Street__c = wl.Street__c, Post_Code__c = wl.Post_Code__c, Country__c = wl.Country__c);
                    lstHWLToInsertOnOtherInverntories.add(objhWL1);
                    lstWLToDelOnOtherInverntories.add(wl);
                }

            }
        }


        try {

            if (!finalAllToInsert.isEmpty()) {
                delete lstWLToDeleteFinal;
                insert finalAllToInsert;
                insert lstHWLToInsert;
                delete lstWLToDelOnOtherInverntories;
                insert lstHWLToInsertOnOtherInverntories;
            }

        } catch (Exception e) {
            System.debug('Error inserting /deleting');
        }

    }

    global void finish(Database.BatchableContext BC) {
        System.debug('=====invIdSetToDisplay==' + invIdSetToDisplay);
          AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                          From AsyncApexJob Where Id =: bc.getJobId()
                         ];
        
      /*  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Auto allocation to available spaces : ' + a.Status);
        mail.setPlainTextBody('Records processed : ' + a.TotalJobItems +
                              'with ' + a.NumberOfErrors + ' failures.' + '.'
                              );
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    */
        
    }

}