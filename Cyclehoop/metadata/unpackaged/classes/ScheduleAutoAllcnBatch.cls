/*
 *  Class Name:  ScheduleAutoAllcnBatch
 *  Description: This is a scheduler for the batch job AutoAllocationForAvailableSpceBatch
 *  Company:     dQuotient
 *  CreatedDate: 20-May-2018
 */
global class ScheduleAutoAllcnBatch  implements schedulable {
 global void execute(SchedulableContext SC) {
       
 AutoAllocationForAvailableSpceBatch alloctnbatch = new AutoAllocationForAvailableSpceBatch();
 Database.executeBatch(alloctnbatch ,1);

    }
}