public without sharing class ChargentOrderTriggerFunctions {
    
    public static void ChargentOrder(List <ChargentOrders__ChargentOrder__c> lst_orders){
        
        Set <Id> set_AllocationIds = new set <Id>();
        List <Allocation__c> lst_allocations = new List <Allocation__c>();
        
        for(ChargentOrders__ChargentOrder__c c: lst_orders){
            IF(c.AllocationSubscription__c != Null){
                set_AllocationIds.add(c.AllocationSubscription__c);                
            }
        }
        
        List <Allocation__c> lstallocations = new List <Allocation__c>([SELECT ID, ChargentToken__c, Deposit_Paid__c, EndDate__c, StartDate__c, Status__c FROM Allocation__c WHERE Id in: set_AllocationIds]);
        
        IF(!lstallocations.isempty()){
            for(ChargentOrders__ChargentOrder__c co: lst_orders){
                for(Allocation__c a: lstallocations){
                    IF(co.AllocationSubscription__c == a.id && co.ChargentOrders__Payment_Received__c == 'Full' && co.ChargentOrders__Payment_Frequency__c == 'Once' && a.Status__c == 'Pending Payment' && co.Renewal_Order__c ==FALSE && a.Status__c != 'Active Let'){
                        a.ChargentToken__c = co.ChargentOrders__Tokenization__c;
                        a.Deposit_Paid__c = True;
                        a.EndDate__c = System.today() + 365;
                        a.StartDate__c = System.today();
                        a.Status__c = 'Active Let';
                        lst_allocations.add(a);
                    }
                    IF(co.Renewal_Order__c == TRUE && co.ChargentOrders__Payment_Received__c == 'Full' && co.ChargentOrders__Payment_Frequency__c == 'Once'){
                        a.Deposit_Paid__c = True;
                        a.Status__c = 'Active Let';
                        lst_allocations.add(a);
                    }
                    IF(co.Renewal_Order__c ==True && co.AllocationSubscription__c == a.id && co.ChargentOrders__Payment_Status__c == 'Recurring' && co.ChargentOrders__Payment_Frequency__c == 'Monthly' && co.ChargentOrders__Payment_Received__c == 'Partial' && co.ChargentOrders__Transaction_Count__c >0){
                        a.Deposit_Paid__c = True;
                        a.Status__c = 'Active Let';
                        lst_allocations.add(a);                        
                    }
                }            
            }
            Update lst_allocations;
        }
    } 
}