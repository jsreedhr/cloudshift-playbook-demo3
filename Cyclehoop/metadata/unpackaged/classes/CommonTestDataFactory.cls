/**
 *  @Class Name:    CommonTestDataFactory
 *  @Description:   This is a utility class for creating object records
 *  @Company:       Standav
 *  CreatedDate:    02/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aman Sawhney           02/03/2018                  Original Version
 */
public class CommonTestDataFactory {
    
    // Method to create Records for Account object
    public static List<Account> createAccounts( Integer numToInsert, boolean doInsert){
        List<Account> accountList = new List<Account>(); 
        for( Integer i=0; i<numToInsert; i++ ){
            Account account = new Account(
                FirstName = 'Test',
                LastName = 'Account_'+i,
                Phone='999-999-'+(1000+i),
                Billingstreet = 'test street',
                BillingCity = 'test city',
                BillingPostalcode = 'LS1 3DD',
                BillingCountry = 'test country',
                BillingState ='test state',
                Industry ='Apparel',
                Website ='test@org.com'
            );
            accountList.add(account);
        }
        if(doInsert){
            
            insert accountList;
        }
        return accountList;
    }
    
    // Method to create Records for Contact object
    public static List<Contact> createContacts( Integer numToInsert, boolean doInsert){         
        List<Contact> contactList = new List<Contact>(); 
        for( Integer i=0; i<numToInsert; i++ ){
            Contact contact = new Contact(
                LastName='Contact_'+i,
                Phone='999-555-' + 1000 + i,
                Email='contact_' + i + '@example.com'
            );
            contactList.add(contact);
        }    
        if(doInsert){
            
            insert contactList;
        }
        return contactList;
    }
    
    // Method to create Records for Inventory object
    public static List<Inventory__c> createInventories( Integer numToInsert, boolean doInsert){
        List<Inventory__c> inventoryList = new List<Inventory__c>(); 
        Id recordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Bikehangar').getRecordTypeId();
        for( Integer i=0; i<numToInsert; i++ ){
            Inventory__c inventory = new Inventory__c(
                RecordTypeId = recordTypeId,
                Stage__c ='New'
            );
            inventoryList.add(inventory);
        }
        if(doInsert){
            
            insert inventoryList;
        }
        return inventoryList;
    }
    
    // Method to create Records for Waiting List object
    public static List<Waiting_List__c> createWaitingLists( Integer numToInsert, boolean doInsert){
        List<Waiting_List__c> waitingList = new List<Waiting_List__c>(); 
        for( Integer i=0; i<numToInsert; i++ ){
            Waiting_List__c waitInfo = new Waiting_List__c(
                DateRequested__c = System.today(),
                Status__c = 'New',
                Priority__c = false,
                Suggested_Location__c = false,
                Email__c = 'wl_' + i + '@example.com'
            );
            waitingList.add(waitInfo);
        }
        if(doInsert){
            
            insert waitingList;
        }
        return waitingList;
    }
    
    // Method to create Records for Space object
    public static List<Space__c> createSpaces( Integer numToInsert, boolean doInsert){
        List<Space__c> spaceList = new List<Space__c>(); 
        for( Integer i=0; i<numToInsert; i++ ){
            Space__c space = new Space__c(
                Name = 'Space_'+i
            );
            spaceList.add(space);
        }
        if(doInsert){
            
            insert spaceList;
        }
        return spaceList;
    }
        
    public static List<MaintenanceVisit__c> createMaintenanceVisits(Integer numToInsert, boolean doInsert){
        List<MaintenanceVisit__c> maintenanceVisitList = new List<MaintenanceVisit__c>(); 
        Id recordTypeId = Schema.SObjectType.MaintenanceVisit__c.getRecordTypeInfosByName().get('Bikehangar Maintenance').getRecordTypeId();
        for( Integer i=0; i<numToInsert; i++ ){
            MaintenanceVisit__c maintenanceVisit = new MaintenanceVisit__c(
                RecordTypeId = recordTypeId
            );
            maintenanceVisitList.add(maintenanceVisit);
        }
        if(doInsert){
            
            insert maintenanceVisitList;
        }
        return maintenanceVisitList;
    }
    
    public static List<Contract> createContracts( Integer numToInsert, boolean doInsert){
        List<Contract> contractList = new List<Contract>(); 
        for( Integer i=0; i<numToInsert; i++ ){
            Contract contract = new Contract(
                Status = 'Draft',
                StartDate = system.today(),
                ContractTerm = 6
            );
            contractList.add(contract);
        }
        if(doInsert){
            
            insert contractList;
        }
        return contractList;
    }
        
    //method to create records of inventories to test auto allocation
    public static List<Inventory__c> createInventories(String recordTypeLabel){
        
        Id recordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();
        Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        
        List<Account> personAccountList = new List<Account>();
        List<Inventory__c> inventoryList = new List<Inventory__c>();
        Account accountX = null;
        Contract contractX = null;
        Contact contactX = null;
        Opportunity oppX = null;
        
        //insert the person accounts
        personAccountList.add(new Account(
            FirstName  = 'TEST-ALLOCATION-1-FIRSTNAME',
            LastName  = 'TEST-ALLOCATION-1-LASTNAME',
            BillingStreet = 'TEST-ALLOCATION-1-BILLINGSTREET',
            BillingState = 'TEST-ALLOCATION-1-BILLINGSTATE',
            BillingCity = 'TEST-ALLOCATION-1-BILLINGCITY',
            BillingCountry = 'TEST-ALLOCATION-1-BILLINGCOUNTRY',
            BillingPostalCode = 'POSTALCODE',
            PersonEmail = 'TESTALLOCATION1FIRSTNAME@gm.com',
            PersonHomePhone = '123456789', 
            RecordTypeId = personAccountrecordTypeId
        ));
        
        personAccountList.add(new Account(
            FirstName  = 'TEST-ALLOCATION-2-FIRSTNAME',
            LastName  = 'TEST-ALLOCATION-2-LASTNAME',
            BillingStreet = 'TEST-ALLOCATION-2-BILLINGSTREET',
            BillingState = 'TEST-ALLOCATION-2-BILLINGSTATE',
            BillingCity = 'TEST-ALLOCATION-2-BILLINGCITY',
            BillingCountry = 'TEST-ALLOCATION-2-BILLINGCOUNTRY',
            BillingPostalCode = 'POSTALCODE',
            PersonEmail = 'TESTALLOCATION1FIRSTNAME@gm.com',
            PersonHomePhone = '123456789', 
            RecordTypeId = personAccountrecordTypeId
        ));
        insert personAccountList;
        
        //insert simple account
       accountX = new Account(
            OwnerId = UserInfo.getUserId(),
           Name = 'SIMPLE-TEST-ACCOUNT',
           Sector__c = 'Local Authority',
           Borough_Manager__c = UserInfo.getUserId(),
           Borough_Rent_Payment_Frequency__c = 'Annually',
           BoroughRequirements__c='Can never be on the website'
       );
       insert accountX;
        
       //insert contact 
       contactX = new Contact(
        
           OwnerId = UserInfo.getUserId(),
           LastName = 'SIMPLE-TEST-CONTACT',
           AccountId = accountX.Id,
           Title = 'Director of Public Affairs',
           Phone = '123456789'
       );
       insert contactX; 
       
       //insert opportunity
       oppX = new Opportunity(
            OwnerId = UserInfo.getUserId(),
            Name = 'SIMPLE-TEST-OPPORTUNITY',
            AccountId = accountX.Id,
            Contact_Name__c = contactX.Id,
            CloseDate = system.today(),
            Sales_Type__c = 'Direct Sale',
           StageName='Closed Won',
           Probability=100,
           ForecastCategoryName='Closed',
           Amount=12345,
           Payment_Type__c='100% up front',
           Expected_Project_Completion_Date__c=system.today().addMonths(2),
           Project_Type__c='Supply only',
           PONumber__c='12345'
       );
        
       insert oppX; 
        
        //insert the contract
        contractX = new Contract(
            OwnerId = UserInfo.getUserId(),
            AccountId = accountX.Id,
            StartDate = System.today().addMonths(-6),
            ContractTerm = 12,
            Originating_Opportunity__c = oppX.Id
        );
        
        insert contractX;
        
        
        //insert inventories
        for(Integer i=1;i<=200;i++){
            
            if(recordTypeLabel=='Bikehangar'){
                
                inventoryList.add(
                    new Inventory__c(
                        ClientBorough__c = personAccountList[0].Id,
                        ReadyforAutoAllocation__c = true,
                        RecordTypeId = recordTypeId,
                        Stage__c = 'Installed',
                        ScheduledInstallationDate__c = System.today().addDays(-6),
                        ActualInstallationDate__c = System.today(),
                        Deposit__c =25,
                        Originating_Contract__c = contractX.Id,
                        Current_Contract__c = contractX.Id,
                        Originating_Opportunity__c = oppX.Id,
                        MM_Provider__c = 'Cyclehoop',
                        AllowedNumberofTenants__c = 3,
                        axOversubscribeAmount__c = 3
                    )
                );
                
            }
            
            else if(recordTypeLabel=='Cycle Hub'){
                
                inventoryList.add(
                    new Inventory__c(
                        ClientBorough__c = personAccountList[1].Id,
                        ReadyforAutoAllocation__c = true,
                        RecordTypeId = recordTypeId,
                        Stage__c = 'Installed',
                        ScheduledInstallationDate__c = System.today().addDays(-6),
                        ActualInstallationDate__c = System.today(),
                        Deposit__c =25,
                        Originating_Contract__c = contractX.Id,
                        Current_Contract__c = contractX.Id,
                        Originating_Opportunity__c = oppX.Id,
                        MM_Provider__c = 'Cyclehoop',
                        AllowedNumberofTenants__c = 3,
                        axOversubscribeAmount__c = 3
                    )
                );
                
            }
            
            else{
                
                inventoryList.add(
                    new Inventory__c(
                        ClientBorough__c = personAccountList[1].Id,
                        ReadyforAutoAllocation__c = true,
                        RecordTypeId = recordTypeId,
                        Stage__c = 'Installed',
                        ScheduledInstallationDate__c = System.today().addDays(-6),
                        ActualInstallationDate__c = System.today(),
                        Deposit__c =25,
                        Originating_Contract__c = contractX.Id,
                        Current_Contract__c = contractX.Id,
                        Originating_Opportunity__c = oppX.Id,
                        MM_Provider__c = 'Cyclehoop',
                        AllowedNumberofTenants__c = 3,
                        axOversubscribeAmount__c =3
                    )
                );
                
            }      
            
            
            
        }
        
        insert inventoryList;
        
        return inventoryList;
        
    }
    
    //method to create records of waiting list in given inventories
    public static List<Waiting_List__c> createWaitingLists(List<Inventory__c> inventoryList){
        
        List<Waiting_List__c> waitingLstList = new List<Waiting_List__c>();
        Account accountX = null;
        Contact contact1 = null;
        Contact contact2 = null;
        Id waitingListRecordTypeId = Schema.SObjectType.Waiting_List__c.getRecordTypeInfosByName().get('Waiting List Record Type').getRecordTypeId();
        
        //insert simple account
       accountX = new Account(
            OwnerId = UserInfo.getUserId(),
           Name = 'SIMPLE-TEST-ACCOUNT',
           Sector__c = 'Local Authority',
           Borough_Manager__c = UserInfo.getUserId(),
           Borough_Rent_Payment_Frequency__c = 'Annually',
           BoroughRequirements__c='Can never be on the website'
       );
       insert accountX;
        
       //insert contact 
       contact1 = new Contact(
        
           OwnerId = UserInfo.getUserId(),
           LastName = 'SIMPLE-TEST-CONTACT1',
           AccountId = accountX.Id,
           Title = 'Director of Public Affairs',
           Phone = '123456789'
       );
       contact2 = new Contact(
        
           OwnerId = UserInfo.getUserId(),
           LastName = 'SIMPLE-TEST-CONTACT2',
           AccountId = accountX.Id,
           Title = 'Director of Public Affairs',
           Phone = '123456789'
       );
        
        //insert contacts
       insert contact1;  
       insert contact2; 
        
        for(Inventory__c inv : inventoryList){
        	
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today(),
                    Priority__c = true,
                    Suggested_Location__c = false,
                    Contact__c = contact1.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today().addDays(-2),
                    Priority__c = true,
                    Suggested_Location__c = false,
                     Contact__c = contact2.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today(),
                    Priority__c = false,
                    Suggested_Location__c = true,
                     Contact__c = contact1.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today().addDays(-2),
                    Priority__c = false,
                    Suggested_Location__c = true,
                     Contact__c = contact2.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today().addDays(-2),
                    Priority__c = false,
                    Suggested_Location__c = false,
                     Contact__c = contact1.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
                waitingLstList.add(new Waiting_List__c(
                    RecordTypeId = waitingListRecordTypeId,
                    DateRequested__c = system.today(),
                    Priority__c = false,
                    Suggested_Location__c = false,
                     Contact__c = contact2.Id,
                    Status__c = 'New',
                    Inventory__c = inv.Id
                ));
            
        }
        
        //insert the waiting lists
        insert waitingLstList;
        
        return waitingLstList;
    }
    
    //method to create spaces in the iven inventories
    public static List<Space__c> createSpaces(List<Inventory__c> inventoryList){
   		
        List<Space__c> spaceList = new List<Space__c>();
		Integer inventoryListSize = inventoryList.size();
        
        for(Integer i=0;i< inventoryListSize ; i++){
            
            spaceList.add(new Space__c(
                Inventory__c = inventoryList[i].Id,
                Status__c = 'Available',
                NoActiveAllocations__c = 0,
                Name = inventoryList[i].Name + i + '-1'
            ));
            spaceList.add(new Space__c(
                Inventory__c = inventoryList[i].Id,
                Status__c = 'Available',
                NoActiveAllocations__c = 0,
                Name = inventoryList[i].Name + i + '-2'
            ));
            
        }
        
        //insert the spaces
        insert spaceList;
        
        return spaceList;
    }
    
}