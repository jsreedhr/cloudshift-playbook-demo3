@isTest
public class TsCsAcceptanceConttlrTest {
    
    @isTest
    private static  void testMethod1() {
        Id recordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Bikehangar').getRecordTypeId();
        Id recordTypeId2 = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Cycle Hub').getRecordTypeId();
        Id recordTypeIdWL = Schema.SObjectType.Waiting_List__c.getRecordTypeInfosByName().get('Waiting List Record Type').getRecordTypeId();
        
        List<Inventory__c> lstInven = new List<Inventory__c>();
        
        Inventory__c inventory = new Inventory__c(
            RecordTypeId = recordTypeId,
            ReadyforAutoAllocation__c = true,
            Deposit__c = 20.00
            
        );
        insert inventory;
        Space__c space = new Space__c(
            Name = 'Space_1',
            Status__c='Available',
            Inventory__c=inventory.id
        );
        insert space;
        Contact contact = new Contact(
            LastName='Contact_1',
            Phone='999-555-1000',
            Email='contact_1@example.com'
            // AccountId=accountList2[0].id
        );
        insert contact;
        Allocation__c al = new Allocation__c();
        al.space__c = space.id;
        al.LeadTenant__c= contact.id;
        insert al;
        
        ChargentOrders__ChargentOrder__c co = new ChargentOrders__ChargentOrder__c();
        co.AllocationSubscription__c = al.id;
        insert co;
        ChargentOrders__Payment_Request__c payReq = new ChargentOrders__Payment_Request__c();
        payReq.ChargentOrders__ChargentOrder__c=co.id;
        payReq.ChargentOrders__Pay_Link__c='---';
        insert payReq;
        apexpages.currentpage().getparameters().put('payReqId' , payReq.id);
        PageReference pageRef = Page.TsCsAcceptance_vf;
        TsCsAcceptanceConttlr controller = new TsCsAcceptanceConttlr();
        
        controller.payMentRedirect();
    }   
    
    @isTest
    private static void testMethod2()
    {
        PageReference pageRef = Page.communityContact_vf;
        CommunityContactController controller = new CommunityContactController();
        controller.contactUs();
        
    }
}