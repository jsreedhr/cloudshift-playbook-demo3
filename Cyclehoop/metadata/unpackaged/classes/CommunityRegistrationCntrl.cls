/***************************************************
* Author        : Aman Sawhney
* Class Name    : CommunityRegistrationCntrl
* Date Created  : 26 - Feb - 2018
* Purpose       : This is the controller class for 
*                 the community registration page.
* *************************************************/
public class CommunityRegistrationCntrl {
    
    @RemoteAction
    public static RegistrationResult registerUser(RegistrationInfo info){
        
        RegistrationResult result = new RegistrationResult();
        result.error = null;
        result.status = false;
        result.message = null;
        
        Id profileId = null;
        
        //check for backend validation for all relevant fields
        if(info.password==null || info.password=='' || info.password!=info.confirmPassword || info.lastName==null || info.lastName==''){
            result.error = 'Some Error has occurred, please check your inputs';
            result.status = false;
            result.message = null;
        }
        
        //if validated
        else{
            
            //get the profile Id of the community user    
            profileId = [Select Id From Profile Where Name = 'Cyclehoop Customer Community Login User' LIMIT 1].Id;
            
            //make the instance of user
            User communityUser = new User();
            communityUser.Username = info.email;
            communityUser.Email = info.email;
            communityUser.FirstName = info.firstName;
            communityUser.LastName = info.lastName;
            communityUser.CommunityNickname = info.nickName;
            communityUser.ProfileId = profileId;
            
            //create a community user
            try{
                
                System.debug('Success-->'+communityUser);
                //String userId='efwe';
                Id userId = Site.createExternalUser(communityUser, '' , info.password);
                
                //if user created
                if(userId!=null){
                    result.error = null;
                    result.status = true;
                    result.message = 'User successsfully registered';
                }    
                
                //if user not created
                else{
                    result.error = 'Some error occurred while registering user.';
                    result.status = false;
                    result.message = null;
                }
            }
            
            catch(Exception e){
                result.error = e.getMessage();
                result.status = false;
                result.message = null;
            }
            
            return result;
            
        }
        
        return result;
    }      
    
    public Class RegistrationInfo{
        
        public String firstName{get;set;}
        public String lastName{get;set;}
        public String nickName{get;set;}
        public String email{get;set;}
        public String password{get;set;}
        public String confirmPassword{get;set;}
        
    }
    
    public Class RegistrationResult{
        
        public String error{get;set;}
        public String message{get;set;}
        public Boolean status{get;set;}
        
    }
    
}