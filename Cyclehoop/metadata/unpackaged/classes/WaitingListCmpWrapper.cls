global class WaitingListCmpWrapper implements Comparable{
    
    public Waiting_List__c waitingListRecord{get;set;}
    
    public WaitingListCmpWrapper(Waiting_List__c w1) {
        this.waitingListRecord = w1;
    }
    
    // Compare waitinglist based on the requested date.
    global Integer compareTo(Object compareTo) {
        
        // Cast argument to OpportunityWrapper
        WaitingListCmpWrapper waitListWrapper = (WaitingListCmpWrapper)compareTo;
        
        Waiting_List__c w1 = this.waitingListRecord;
        Waiting_List__c w2 = waitListWrapper.waitingListRecord;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        
        if(w1.Priority__c==false && w2.Priority__c==false){
            
            if(w1.Suggested_Location__c==false && w2.Suggested_Location__c==false){
                
                if(w1.DateRequested__c < w2.DateRequested__c){
                    returnValue = -1;
                }
                else if(w1.DateRequested__c > w2.DateRequested__c){
                    returnValue = 1;
                }
                else{
                    returnValue = 0;
                }
                
            }
            else if(w1.Suggested_Location__c==false && w2.Suggested_Location__c==true){
                
                returnValue = 1;
                
            }
            else if(w1.Suggested_Location__c==true && w2.Suggested_Location__c==false){
                
                returnValue = -1;
                
            }
            else{
                
                if(w1.DateRequested__c < w2.DateRequested__c){
                    returnValue = -1;
                }
                else if(w1.DateRequested__c > w2.DateRequested__c){
                    returnValue = 1;
                }
                else{
                    returnValue = 0;
                }
                
            }
            
        }
        
        else if(w1.Priority__c==false && w2.Priority__c==true){
            
            returnValue = 1;
            
        }
        
        else if(w1.Priority__c==true && w2.Priority__c==false){
            
            returnValue = -1;
            
        }
        
        else{
            
            if(w1.Suggested_Location__c==false && w2.Suggested_Location__c==false){
                
                if(w1.DateRequested__c < w2.DateRequested__c){
                    returnValue = -1;
                }
                else if(w1.DateRequested__c > w2.DateRequested__c){
                    returnValue = 1;
                }
                else{
                    returnValue = 0;
                }
                
            }
            else{
                
                if(w1.DateRequested__c < w2.DateRequested__c){
                    returnValue = -1;
                }
                else if(w1.DateRequested__c > w2.DateRequested__c){
                    returnValue = 1;
                }
                else{
                    returnValue = 0;
                }
                
            }
            
        }
        
        return returnValue;   
    }
    
}