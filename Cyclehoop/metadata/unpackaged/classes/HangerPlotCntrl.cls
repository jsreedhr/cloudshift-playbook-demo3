/***************************************************
* Author        : Aman Sawhney
* Class Name    : HangerPlotCntrl
* Date Created  : 15 - Feb - 2018
* Purpose       : This is the controller class for 
*                 the HangerPlot_vf page and extension
*				  for inventoryDetail_vf
* *************************************************/
public class HangerPlotCntrl {
    
    public static List<SelectOption> inventoryRecordTypeList{get;set;}
    public static List<SelectOption> sourceList{get;set;}
     
    //constructor to initialize the inventoryDetail_vf
    public HangerPlotCntrl(ApexPages.StandardController std){
        init();
    }
    
    //constructor to initialize the HangerPlot_vf
    public HangerPlotCntrl(){
        
        List<Schema.RecordTypeInfo> invRecordTypeList = Inventory__c.SObjectType.getDescribe().getRecordTypeInfos();
        
        if(invRecordTypeList!=null && !invRecordTypeList.isEmpty()){
            
            inventoryRecordTypeList = new List<SelectOption>();
            //add all for record types
            inventoryRecordTypeList.add(new SelectOption('All','All'));
            
            for(Schema.RecordTypeInfo recordType : invRecordTypeList){
                
                if(recordType.getName()!='Master'){
                    inventoryRecordTypeList.add(new SelectOption(recordType.getRecordTypeId(),recordType.getName()));
                }
                
            }
            
        }
        
        //initialize page
        init();
        
    }
    
    //page initialize
    public void init(){
        
        
        //set the source picklist
        Schema.DescribeFieldResult sourceFieldResult = Waiting_List__c.HowdidyouhearaboutCyclehoop__c.getDescribe();
        List<Schema.PicklistEntry> sourcePickList = sourceFieldResult.getPicklistValues();
        
        if(sourcePickList!=null && !sourcePickList.isEmpty()){
            
            sourceList = new List<SelectOption>();
            for(Schema.PicklistEntry f : sourcePickList){
                
                sourceList.add(new SelectOption(f.getLabel(), f.getValue()));
                
            }
            
        }
        
    }
    
    //this method gets the inventories when input is pincode and inventory type
    @RemoteAction
    public static List<Inventory__c> getPoints(String pinCode,List<String> recordTypeDevelopernameList){
        
        //returnable list of inventories
        List<Inventory__c> inventoryList = null;
        
        //return the list of inventories with given postal code
        
        //if all record types are called
        if(recordTypeDevelopernameList==null || recordTypeDevelopernameList.isEmpty()){
            
            //if pincode is not null
            if(pinCode!=null && pinCode!=''){
            	inventoryList = [select Id,Name,Stage__c,BikeHangar_Number__c,RecordType.Name,Picture__c,No_Spaces_Available__c,Location__c,
                                            FinalPricePerUser__c,Building_Name_Number__c,Street__c,City__c,Post_Code__c,Available__c,
                                            ClientBorough__r.Borough_Waiting_List__c,ClientBorough__r.Borough_Waiting_List_URL__c
                                            from Inventory__c where Post_Code__c LIKE :pinCode+'%' LIMIT 10000];    
            }
            
        }
        
        //for a specific record type
        else{
            inventoryList = [select Id,Name,Stage__c,BikeHangar_Number__c,RecordType.Name,Picture__c,No_Spaces_Available__c,Location__c,
                                            FinalPricePerUser__c,Building_Name_Number__c,Street__c,City__c,Post_Code__c,Available__c,
                                            ClientBorough__r.Borough_Waiting_List__c,ClientBorough__r.Borough_Waiting_List_URL__c
                                            from Inventory__c where Post_Code__c LIKE :pinCode+'%' 
                                            AND RecordType.DeveloperName IN:recordTypeDevelopernameList  LIMIT 10000];
        }
       
        return inventoryList;
    }
    
    //this method gets the inventories when input is pincode and inventory type
    @RemoteAction
    public static List<Inventory__c> getAllPoints(){
        
        //returnable list of inventories
        List<Inventory__c> inventoryList = null;
        
        //return the list of all inventories in system
        inventoryList = [select Id,Name,Stage__c,BikeHangar_Number__c,RecordType.Name,Picture__c,No_Spaces_Available__c,Location__c,
                             FinalPricePerUser__c,Building_Name_Number__c,Street__c,City__c,Post_Code__c,Available__c,
                             ClientBorough__r.Borough_Waiting_List__c,ClientBorough__r.Borough_Waiting_List_URL__c
                             from Inventory__c where Available_on_Website__c=true LIMIT 10000];    
        
        return inventoryList;
    }
    
    
    //this method handles the application for waiting list of inventory
    @RemoteAction
    public static String applyforWaitingList(FormData waitingInfo){
        
        //returnable message
        String message = '';
        
        Savepoint sp = Database.setSavepoint();
        
        //basic initializations
        List<Account> personAccountList = null;
        Account personAccount = null;
        Id personAccountContactId = null;
        
        //if email is provided get the person account with the same email
        if(waitingInfo.email!=null){
            personAccountList = [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                 BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                 PersonContactId,RecordTypeId from Account where 
                                 IsPersonAccount=true AND
                                 PersonEmail = :waitingInfo.email
                                 Limit 1
                                ];
        }
        
        //if email is not provided
        else{
            personAccountList =  [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                  BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                  PersonContactId,RecordTypeId 
                                  from Account where 
                                  IsPersonAccount=true AND
                                  FirstName = :waitingInfo.firstName AND
                                  LastName = :waitingInfo.LastName
                                  Limit 1
                                 ];
        }
        
        //if no person account exist, create one and find the contact id of that account
        if(personAccountList.isEmpty()){
            
            Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
            
            personAccount = new Account(
                FirstName  = waitingInfo.firstName,
                LastName  = waitingInfo.lastName,
                BillingStreet = waitingInfo.street,
                BillingState = waitingInfo.state,
                BillingCity = waitingInfo.city,
                BillingCountry = waitingInfo.country,
                BillingPostalCode = waitingInfo.postalCode,
                PersonEmail = waitingInfo.email,
                PersonHomePhone = waitingInfo.phone, 
                RecordTypeId = personAccountrecordTypeId
            );
            
            try{
                
                //insert person account
                insert personAccount;
                
                //query for the contact related to this person acount
                personAccountContactId = [select PersonContactId from Account where Id=:personAccount.Id].PersonContactId;
                
            }
            
            catch(Exception e){
                
                message = e.getMessage();
                System.debug('ERROR OCCURED WHILE INSERTING PERSON ACCOUNT - '+e.getMessage());
                
            }
            
        }
        
        //assign the contact id of existing person account
        else{
            
            personAccount = personAccountList[0];
            personAccountContactId = personAccount.PersonContactId;
            
        }
        
        //create waiting list only if person account and contact is created
        if(personAccount!=null && personAccountContactId!=null){
            
            Id waitingListRecordTypeId = Schema.SObjectType.Waiting_List__c.getRecordTypeInfosByName().
            get('Waiting List Record Type').getRecordTypeId();
            
            //initialize the waiting record
            Waiting_List__c waitInfo = new Waiting_List__c(
                First_Name__c = personAccount.FirstName,
                Last_Name__c = personAccount.LastName,
                Email__c = personAccount.PersonEmail,
                DateRequested__c = System.today(),
                Inventory__c = waitingInfo.inventoryId,
                Status__c = 'New',
                Contact__c = personAccountContactId,
                Priority__c = false,
                Suggested_Location__c = false,
                RecordTypeId = waitingListRecordTypeId
            );
            
            try{
                
                insert waitInfo;
                
            }
            catch(Exception e){
                
                System.debug('ERROR OCCURED WHILE INSERTING WAITING LIST - '+e.getMessage());
                
                //delete the person account by rollbacking the whole transaction
                Database.rollback( sp );
                
                message = e.getMessage();
            }
        }
        else{
            //delete the person account if created by rollbacking the whole transaction
            Database.rollback( sp );
            
            message = 'ERROR OCCURRED...';
        }
        
        return message;
        
    }
    
    //this method handles the application for space allocation in inventory
    @RemoteAction
    public static String applyforAllocation(FormData allocationInfo){
        
        String message = '';
        SavePoint sp =Database.setSavepoint();
        
        List<Account> personAccountList = null;
        Account personAccount = null;
        Id personAccountContactId = null;
        
        //if email is provided get the person account with the same email
        if(allocationInfo.email!=null){
            
            personAccountList =  [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                  BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                  PersonContactId,RecordTypeId from Account where 
                                  IsPersonAccount=true AND
                                  PersonEmail = :allocationInfo.email
                                  Limit 1
                                 ]; 
            
        }
        
        //if email is not provided
        else{
            personAccountList =  [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                  BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                  PersonContactId,RecordTypeId from Account where 
                                  IsPersonAccount=true AND
                                  FirstName = :allocationInfo.firstName AND
                                  LastName = :allocationInfo.LastName 
                                  Limit 1
                                 ]; 
        }
        
        //if no person account exist, create one and find the contact id of that account
        if(personAccountList.isEmpty()){
            
            Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
            
            personAccount = new Account(
                FirstName  = allocationInfo.firstName,
                LastName  = allocationInfo.lastName,
                BillingStreet = allocationInfo.street,
                BillingState = allocationInfo.state,
                BillingCity = allocationInfo.city,
                BillingCountry = allocationInfo.country,
                BillingPostalCode = allocationInfo.postalCode,
                PersonEmail = allocationInfo.email,
                PersonHomePhone = allocationInfo.phone, 
                RecordTypeId = personAccountrecordTypeId
            );
            
            try{
                
                //insert person account
                insert personAccount;
                
                //query for the contact related to this person acount
                personAccountContactId = [select PersonContactId from Account where Id=:personAccount.Id].PersonContactId;
                
            }
            
            catch(Exception e){
                
                message = e.getMessage();
                System.debug('ERROR OCCURED WHILE INSERTING PERSON ACCOUNT - '+e.getMessage());
                
            }
            
        }
        
        else{
            
            personAccount = personAccountList[0];
            personAccountContactId = personAccount.PersonContactId;
        }
        
        //get the space record
        List<Space__c> spaceList = [Select Id,Name from Space__c where Status__c='Available' AND 
                                    Inventory__c=:allocationInfo.inventoryId LIMIT 1];
        
        //proceed only if person account and contact is not null
        if(personAccount!=null && personAccountContactId!=null){
            
            //if space record is found, create an allocation
            if(!spaceList.isEmpty()){
                Allocation__c allocation = new Allocation__c(
                    Space__c = spaceList[0].Id,
                    LeadTenant__c = personAccountContactId,
                    Status__c = 'Draft',
                    Date_Requested__c = allocationInfo.dateOfRequest
                );
                
                try{
                    insert allocation;
                }
                catch(Exception e){
                    message = e.getMessage();
                    System.debug('ERROR OCCURED WHILE INSERTING ALLOCATION - '+e.getMessage());
                    
                    //delete the person account by rollbacking the whole transaction
                    Database.rollback( sp );
                }
                
            }
            
            else{
                message = 'No empty space found for this inventory';
                //delete the person account if created by rollbacking the whole transaction
                Database.rollback( sp );
            }
            
        }
        
        else{
            message = 'ERROR OCCURRED...';
            //delete the person account if created by rollbacking the whole transaction
            Database.rollback( sp );
            
        }
        
        return message;
    }
    
    //this method handles location suggestion input for future inventories
    @RemoteAction
    public static String suggestLocation(FormData suggestionInfo){
        
        //returnable message
        String message = '';
        
        Savepoint sp = Database.setSavepoint();
        
        //basic initializations
        List<Account> personAccountList = null;
        Account personAccount = null;
        Id personAccountContactId = null;
        
        //if email is provided get the person account with the same email
        if(suggestionInfo.email!=null){
            personAccountList = [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                 BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                 PersonContactId,RecordTypeId from Account where 
                                 IsPersonAccount=true AND
                                 PersonEmail = :suggestionInfo.email
                                 Limit 1
                                ];
        }
        
        //if email is not provided
        else{
            personAccountList =  [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                  BillingCountry,BillingPostalCode,PersonEmail,PersonHomePhone,
                                  PersonContactId,RecordTypeId from Account where 
                                  IsPersonAccount=true AND
                                  FirstName = :suggestionInfo.firstName AND
                                  LastName = :suggestionInfo.LastName
                                  Limit 1
                                 ];
        }
        
        //if no person account exist, create one and find the contact id of that account
        if(personAccountList.isEmpty()){
            
            Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
            
            personAccount = new Account(
                FirstName  = suggestionInfo.firstName,
                LastName  = suggestionInfo.lastName,
                BillingStreet = suggestionInfo.street,
                BillingState = suggestionInfo.state,
                BillingCity = suggestionInfo.city,
                BillingCountry = suggestionInfo.country,
                BillingPostalCode = suggestionInfo.postalCode,
                PersonEmail = suggestionInfo.email,
                PersonHomePhone = suggestionInfo.phone, 
                RecordTypeId = personAccountrecordTypeId,
                Cyclehoop_Rentals_Mailing_List__pc=suggestionInfo.signUp
               
            );
            
            try{
                
                //insert person account
                insert personAccount;
                
                //query for the contact related to this person acount
                personAccountContactId = [select PersonContactId from Account where Id=:personAccount.Id].PersonContactId;
                
            }
            
            catch(Exception e){
                
                message = e.getMessage();
                System.debug('ERROR OCCURED WHILE INSERTING PERSON ACCOUNT - '+e.getMessage());
                
            }
            
        }
        
        //assign the contact id of existing person account
        else{
            
            personAccount = personAccountList[0];
            //update Cyclehoop_Rentals_Mailing_List__pc on person account
            personAccount.Cyclehoop_Rentals_Mailing_List__pc =suggestionInfo.signUp ;
            update personAccount;
            personAccountContactId = personAccount.PersonContactId;
            
        }
        
        //create waiting list suggestion only if person account and contact is created
        if(personAccount!=null && personAccountContactId!=null){
            
            Id suggestLocationRecordTypeId = Schema.SObjectType.Waiting_List__c.getRecordTypeInfosByName().
            get('Suggested Location Record Type').getRecordTypeId();
            
            //initialize the waiting record to suggest location
            Waiting_List__c waitInfo = new Waiting_List__c(
                First_Name__c = personAccount.FirstName,
                Last_Name__c = personAccount.LastName,
                Email__c = personAccount.PersonEmail,
                SuggestedStreet__c = suggestionInfo.suggestedStreet,
                SuggestedCity__c = suggestionInfo.suggestedCity,
                SuggestedPostCode__c = suggestionInfo.suggestedPostalCode,
                SuggestedCountry__c = suggestionInfo.suggestedCountry,
                SuggestedState__c = suggestionInfo.suggestedState,
                SuggestedBuildingNameNoLocation__c = suggestionInfo.suggestedBuilding,
                DateRequested__c = System.today(),
                Status__c = 'New',
                Contact__c = personAccountContactId,
                Priority__c = false,
                Suggested_Location__c = true,
                Suggested_Location_Comments__c = suggestionInfo.description,
                RecordTypeId = suggestLocationRecordTypeId,
                HowdidyouhearaboutCyclehoop__c = suggestionInfo.source
                
            );
            
            try{
                
                insert waitInfo;
                
            }
            catch(Exception e){
                
                System.debug('ERROR OCCURED WHILE INSERTING WAITING LIST - '+e.getMessage());
                
                //delete the person account by rollbacking the whole transaction
                Database.rollback( sp );
                
                message = e.getMessage();
            }
            
        }
        
        else{
            
            //delete the person account if created by rollbacking the whole transaction
            Database.rollback( sp );
            
            message = 'ERROR OCCURRED...';
            
        }
        
        
        return message;
        
    }
    
    //Data wrapper to get the form input for all types
    public class FormData{
        
        public String firstName{get;set;}
        public String lastName{get;set;}
        public String street{get;set;}
        public String city{get;set;}
        public String country{get;set;}
        public String state{get;set;}
        public String postalCode{get;set;}
        public String suggestedStreet{get;set;}
        public String suggestedCity{get;set;}
        public String suggestedCountry{get;set;}
        public String suggestedState{get;set;}
        public String suggestedPostalCode{get;set;}
        public String suggestedBuilding{get;set;}
        public String email{get;set;}
        public String phone{get;set;}
        public String location{get;set;}
        public Date dateOfRequest{get;set;}
        public String inventoryId{get;set;}
        public String source{get;set;}
        public String comments{get;set;}
        public Boolean signUp{get;set;}
        public String description{get; set;}
        
    }
    
}