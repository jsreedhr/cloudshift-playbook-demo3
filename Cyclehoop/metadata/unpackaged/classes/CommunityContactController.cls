public class CommunityContactController {
    public PageReference contactUs() {
        PageReference pageRef = new PageReference('https://www.cyclehoop.rentals/contact/'); 
        return pageRef;
    }
}