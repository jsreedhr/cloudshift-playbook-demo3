/**
 *  @Class Name:    InventoryWorkTest
 *  @Description:   This is a test class for InventoryWork used in Inventory trigger
 *  @Company:       Standav
 *  CreatedDate:    02/03/2018
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Aman Sawhney           02/03/2018                  Original Version
 */
@isTest
private class InventoryWorkTest {
    
    /**
    * Description: This method is to creating Data.
    */
    @testSetup
    static void createData(){
        
        //Inserting personal account record
        List<Account> accountList = CommonTestDataFactory.createAccounts(1,true);
        system.assertNotEquals(accountList, null);

        //Inserting contact record
        List<Contact> contactList = CommonTestDataFactory.createContacts(1,true);
        system.assertNotEquals(contactList, null);
        
        //Inserting Contract record
        List<Contract> contractList =  CommonTestDataFactory.createContracts(1,false);
        contractList[0].AccountId = accountList[0].Id;
        insert contractList;
        system.assertNotEquals(contractList, null);

        //Inserting Inventory record
        List<Inventory__c> inventoryList = CommonTestDataFactory.createInventories(1,false);
        inventoryList[0].Current_Contract__c = contractList[0].Id;
        insert inventoryList;
        system.assertNotEquals(inventoryList, null);
        
        List<MaintenanceVisit__c> maintenanceVisitList = CommonTestDataFactory.createMaintenanceVisits(1,false);
        maintenanceVisitList[0].Inventory__c = inventoryList[0].Id;
        maintenanceVisitList[0].Planned_Date__c = system.today();
        maintenanceVisitList[0].Planned_Date__c.addMonths(8);
        insert maintenanceVisitList;
        system.assertNotEquals(maintenanceVisitList, null);
    }

	private static testMethod void test1() {
	    Test.startTest();
            List<Inventory__c> inventoryList = [Select Id, Name, Post_Code__c, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName,  (select Id, Inventory__c, Planned_Date__c from Maintenance_Visits__r) From Inventory__c];
            InventoryWork.maintainInventories(inventoryList);
            InventoryWork.stripSpaces(inventoryList);
        Test.stopTest();
	}
	
	private static testMethod void test2() {
        List<Inventory__c> inventoryList = [Select Id, Name, Post_Code__c, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='Cycle_Hub';
        inventoryList[0].Post_Code__c = 'LS1 3DD';
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
            InventoryWork.stripSpaces(inventoryList);
        Test.stopTest();
	}
	
	private static testMethod void test3() {
        List<Inventory__c> inventoryList = [Select Id, Name, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName  From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='Bikehangar';
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
        Test.stopTest();
	}
	
	private static testMethod void test4() {
        List<Inventory__c> inventoryList = [Select Id, Name, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='Bikelocker';
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
        Test.stopTest();
	}

    private static testMethod void test5() {
        List<Inventory__c> inventoryList = [Select Id, Name, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='Cycle_Room';
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
        Test.stopTest();
	}
	
	private static testMethod void test6() {
        List<Inventory__c> inventoryList = [Select Id, Name, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='';
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
        Test.stopTest();
	}
	
	private static testMethod void test7() {
        List<Inventory__c> inventoryList = [Select Id, Name, Post_Code__c, Current_Contract__c, Current_Contract__r.StartDate, Current_Contract__r.EndDate, ActualInstallationDate__c, RecordType.DeveloperName From Inventory__c];
        inventoryList[0].RecordType.DeveloperName='Cycle_Hub';
        inventoryList[0].Post_Code__c = '';
        inventoryList[0].Current_Contract__r.StartDate = null;
        update inventoryList;
        Test.startTest();
            InventoryWork.maintainInventories(inventoryList);
            InventoryWork.stripSpaces(inventoryList);
        Test.stopTest();
	}
    
    //////////////////////////////////////////////////////////////////////
    
    @isTest
    private static void testAutoAllocationOfBikehangar(){
        //insert the inventories
        List<Inventory__c> inventoryList = CommonTestDataFactory.createInventories('Bikehangar');
        
        Test.startTest();
       	
        //assert that inventories are inserted with correct data
        System.assert(inventoryList!=null && !inventoryList.isEmpty());
        
        //set this flag to deactvate the trigger firing during rollup update
        BlockActivator.inventoryAutoAllocationDone  = true;
        
        //insert the waiting lists in these inventories
        List<Waiting_List__c> waitingLstList = CommonTestDataFactory.createWaitingLists(inventoryList);
        
        //assert that waiting list has been inserted with correct data
        System.assert(waitingLstList!=null && !waitingLstList.isEmpty());
        
        //insert the spaces
        List<Space__c> spaceList = CommonTestDataFactory.createSpaces(inventoryList);
        
        //set this flag to deactvate the trigger firing during rollup update
        BlockActivator.inventoryAutoAllocationDone  = false;
        
        //get all the relevant fields in inventories
        inventoryList = [select Id,name,Borough_Payment_Frequency__c,RecordType.DeveloperName,
                                                    AllowedNumberofTenants__c,axOversubscribeAmount__c,(select Id from Spaces__r
                                                    where Status__c='Available'),
                                                    (select Id,Name,Contact__c,WaitingListException__c,DateRequested__c,Inventory__c,
                                                     Suggested_Location_Comments__c,TenantAdditionalComments__c,Priority__c,
                                                     Suggested_Location__c from Interested_Parties__r) 
                                                    from Inventory__c where Id IN:inventoryList AND RecordType.developerName='Bikehangar'];
        
      
        //call auto allocate
        InventoryTriggerFunctions.autoAllocation(inventoryList);
        
        Test.stopTest();
        
    }
    
	@isTest
    private static void testAutoAllocationOfcycleHub(){
        
        //insert the inventories
        List<Inventory__c> inventoryList = CommonTestDataFactory.createInventories('Cycle Hub');
        
        Test.startTest();
       	
        //assert that inventories are inserted with correct data
        System.assert(inventoryList!=null && !inventoryList.isEmpty());
        
        //set this flag to deactvate the trigger firing during rollup update
        BlockActivator.inventoryAutoAllocationDone  = true;
        
        //insert the waiting lists in these inventories
        List<Waiting_List__c> waitingLstList = CommonTestDataFactory.createWaitingLists(inventoryList);
        
        //assert that waiting list has been inserted with correct data
        System.assert(waitingLstList!=null && !waitingLstList.isEmpty());
        
        //insert the spaces
        List<Space__c> spaceList = CommonTestDataFactory.createSpaces(inventoryList);
        
        //set this flag to deactvate the trigger firing during rollup update
        BlockActivator.inventoryAutoAllocationDone  = false;
        
        //get all the relevant fields in inventories
        inventoryList = [select Id,name,Borough_Payment_Frequency__c,RecordType.DeveloperName,
                                                    AllowedNumberofTenants__c,axOversubscribeAmount__c,(select Id from Spaces__r
                                                    where Status__c='Available'),
                                                    (select Id,Name,Contact__c,WaitingListException__c,DateRequested__c,Inventory__c,
                                                     Suggested_Location_Comments__c,TenantAdditionalComments__c,Priority__c,
                                                     Suggested_Location__c from Interested_Parties__r) 
                                                    from Inventory__c where Id IN:inventoryList AND RecordType.developerName='Cycle_Hub'];
        
      
        //call auto allocate
        InventoryTriggerFunctions.autoAllocation(inventoryList);
        
        Test.stopTest();
        
    }
    
    
}