/***************************************************
* Author        : Aman Sawhney
* Class Name    : InventoryWork
* Date Created  : 27 - Feb - 2018
* Purpose       : This class handels the inventory 
*				  related execution.
* *************************************************/
public class InventoryWork {
    
    public static Boolean maintainenceCompleted{get;set;}
    
    static{
        maintainenceCompleted = false;
    }
    
    //this method will create maintainence records in inventories
    public static void maintainInventories(List<Inventory__c> inventoryList){
        
        //list of maintainence records to be inserted
        List<MaintenanceVisit__c> maintainenceVisitList = new List<MaintenanceVisit__c>();
        
        //for all inventories
        for(Inventory__c inventory : inventoryList){
            
            //get the contract start date of inventory
            Date contractStartDate = (inventory.Current_Contract__c!=null && inventory.Current_Contract__r.StartDate!=null ?
                                      inventory.Current_Contract__r.StartDate : null);
            
            //check for the contract start date
            if(contractStartDate!=null){
                contractStartDate = (contractStartDate < inventory.ActualInstallationDate__c ? 
                                     inventory.ActualInstallationDate__c : contractStartDate);
                contractStartDate = contractStartDate.addDays(-1);
            }
            else{
                contractStartDate = inventory.ActualInstallationDate__c;
            }
            
            //get the contract end date of inventory
            Date contractEndDate = (inventory.Current_Contract__c!=null && inventory.Current_Contract__r.EndDate!=null ?
                                    inventory.Current_Contract__r.EndDate : null);
            
            //if contract is still not expired and exist on inventory.
            if(contractStartDate!=null && contractEndDate!=null && contractEndDate>=system.today()){
                
                //for all record types other than Cycle_Hub
                if(inventory.RecordType.DeveloperName!='Cycle_Hub'){
                    
                    Integer monthsBetweenStartAndEndDate = contractStartDate.monthsBetween(contractEndDate);
                    
                    //if months between is less than or equals 6 months
                    //add the single maintainence record
                    if(monthsBetweenStartAndEndDate<=6){
                        
                        //initialize planned date
                        Date plannedDate = contractStartDate.addMonths(6);
                        
                        //check for requirement of maintainence
                        Boolean maintainenceRequired = 
                            InventoryWork.checkForAlreadyExistingMaintainence(inventory.Maintenance_Visits__r,plannedDate,
                                                                              inventory.RecordType.DeveloperName);
                        
                        //if maintaainence is required
                        if(maintainenceRequired){
                            maintainenceVisitList.add(
                                new MaintenanceVisit__c(
                                    Inventory__c = inventory.Id,
                                    RecordTypeId = InventoryWork.getMaintainenceRecordTypeIdByInventory(inventory),
                                    Planned_Date__c = plannedDate
                                )
                            );
                        }
                        
                        
                        
                    }
                    
                    //if months between is more than 6 months
                    //more than one maintainence records will be created
                    else{
                        
                        //add the target number of maintainence records till the period of contract end date
                        Date plannedDate = contractStartDate.addMonths(6);
                        
                        //calculate max no. of maintainence records to be created
                        Integer targetNumberOfMaintainenceRecords = 
                            (Integer)Math.ceil(Double.valueOf(monthsBetweenStartAndEndDate) / Double.valueOf(6));
                        
                        //for targetNumberOfMaintainenceRecords times if the planned date is less than
                        //contract end date
                        for(Integer i=1;i<=targetNumberOfMaintainenceRecords && plannedDate<=contractEndDate;i++){
                            
                            //check for requirement of maintainence
                            Boolean maintainenceRequired = 
                                InventoryWork.checkForAlreadyExistingMaintainence(inventory.Maintenance_Visits__r,plannedDate,
                                                                                  inventory.RecordType.DeveloperName);
                            
                            //if maintainence is required
                            if(maintainenceRequired){
                                //add the maintainence record in list with planned date
                                maintainenceVisitList.add(
                                    new MaintenanceVisit__c(
                                        Inventory__c = inventory.Id,
                                        RecordTypeId = InventoryWork.getMaintainenceRecordTypeIdByInventory(inventory),
                                        Planned_Date__c = plannedDate
                                    )
                                );
                            }
                            
                            //set the next maintainence planned date
                            plannedDate = plannedDate.addMonths(6);
                        }
                        
                        
                        
                    }
                    
                }
                
                //for Cycle_Hub record type
                else{
                    
                    Integer daysBetweenStartAndEndDate = contractStartDate.daysBetween(contractEndDate);
                    
                    //if days between is less than or equals 7
                    //add the single maintainence record
                    if(daysBetweenStartAndEndDate<=7){
                        
                        //add planned date
                        Date plannedDate = contractStartDate.addDays(7);
                        
                        //check if the maintainence is required
                        Boolean maintainenceRequired = 
                            InventoryWork.checkForAlreadyExistingMaintainence(inventory.Maintenance_Visits__r,plannedDate,
                                                                              inventory.RecordType.DeveloperName);
                        
                        //maintainence required
                        if(maintainenceRequired){
                            maintainenceVisitList.add(
                                new MaintenanceVisit__c(
                                    Inventory__c = inventory.Id,
                                    RecordTypeId = InventoryWork.getMaintainenceRecordTypeIdByInventory(inventory),
                                    Planned_Date__c = plannedDate
                                )
                            );
                        }
                        
                        
                    }
                    
                    //if months between is more than 6 months
                    //more than one maintainence records will be created
                    else{
                        
                        //add the target number of maintainence records till the period of contract end date
                        Date plannedDate = contractStartDate.addDays(7);
                        
                        //calculate max target maintainence records
                        Integer targetNumberOfMaintainenceRecords = 
                            (Integer)Math.ceil(Double.valueOf(daysBetweenStartAndEndDate) / Double.valueOf(7));
                        
                        //for targetNumberOfMaintainenceRecords times if the planned date is less than
                        //contract end date
                        for(Integer i=1;i<=targetNumberOfMaintainenceRecords && plannedDate<=contractEndDate;i++){
                            
                            //check if the maintainence is required
                            Boolean maintainenceRequired = 
                                InventoryWork.checkForAlreadyExistingMaintainence(inventory.Maintenance_Visits__r,plannedDate,
                                                                                  inventory.RecordType.DeveloperName);
                            
                            //if maintainence required
                            if(maintainenceRequired){
                                
                                //add the maintainence record in list with planned date
                                maintainenceVisitList.add(
                                    new MaintenanceVisit__c(
                                        Inventory__c = inventory.Id,
                                        RecordTypeId = InventoryWork.getMaintainenceRecordTypeIdByInventory(inventory),
                                        Planned_Date__c = plannedDate
                                    )
                                );
                                
                                
                            }
                            
                            //set the next maintainence planned date
                            plannedDate = plannedDate.addDays(7);
                        }
                        
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        try{
            System.debug('maintainenceVisitList -->'+maintainenceVisitList);
            if(!maintainenceVisitList.isEmpty()){
                insert maintainenceVisitList;
            }
        }
        catch(Exception e){
            system.debug('Exception occurred while creating maintainence records->'+e.getMessage());
        }
    }
    
    //this checks if the maintainence is required
    public static Boolean checkForAlreadyExistingMaintainence(List<MaintenanceVisit__c> maintainenceVisitList , Date plannedDate, String recordTypeDevName ){
        
        Boolean maintainenceRequired = true;
        if(maintainenceVisitList!=null && !maintainenceVisitList.isEmpty()){
            
            //for all maintainences, check if any plannned date of these maintainences
            // consides with the given planned date
            for(MaintenanceVisit__c maintainence : maintainenceVisitList){
                
                //check for same date
                if(maintainence.Planned_Date__c == plannedDate){
                    maintainenceRequired = false;
                    break;
                }
                
                if(recordTypeDevName=='Cycle_Hub'){
                    
                    //check for same week
                    if(plannedDate.daysBetween(maintainence.Planned_Date__c)<7){
                        maintainenceRequired = false;
                        break;
                    }
                    
                }
                else{
                    
                    //check for same 6 month duration
                    if(plannedDate.monthsBetween(maintainence.Planned_Date__c)<6){
                        maintainenceRequired = false;
                        break;
                    }
                    
                }
                
            }
            
        }
        
        return maintainenceRequired;
        
    }
    
    //this method return maintainence record type id
    //wrt inventory record
    public static Id getMaintainenceRecordTypeIdByInventory(Inventory__c inventory){
        
        Id maintainenceRecordTypeId = null;
        Schema.DescribeSObjectResult maintainenceType = Schema.SObjectType.MaintenanceVisit__c;
        
        try{
            
            String invRecordTypeDevName = inventory.RecordType.DeveloperName;
            
            if(invRecordTypeDevName.contains('Bikehangar')){
                maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Bikehangar Maintenance').getRecordTypeId();
            }
            
            else if(invRecordTypeDevName.contains('Bikelocker')){
                maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Bikelocker Maintenance').getRecordTypeId();
            }
            
            else if(invRecordTypeDevName.contains('Cycle_Hub')){
                maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Cycle Hub Maintenance').getRecordTypeId();
            }
            
            else if(invRecordTypeDevName.contains('Cycle_Room')){
                maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Cycle Room Maintenance').getRecordTypeId();
            }
            
            else{
                maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Cycle Shelter Maintenance').getRecordTypeId();
            }
            
        }
        catch(Exception e){
            maintainenceRecordTypeId = maintainenceType.getRecordTypeInfosByName().get('Bikehangar Maintenance').getRecordTypeId();
        }
        
        return maintainenceRecordTypeId;
    }
    
    //strip spaces in postal code
    public static List<Inventory__c> stripSpaces(List<Inventory__c> inventoryList){
        
        if(inventoryList!=null && !inventoryList.isEmpty()){
            
            for(Inventory__c i:inventoryList){
                if(i.Post_Code__c!=null){
                    i.Post_Code__c = i.Post_Code__c.replaceAll( '\\s+', '');
                }
            }
            
        }
        return inventoryList;
        
    }
    
    //this method allocate spaces in system
    public static void allocate(Map<Id,InventoryInfo> inventoryInfoMap,Map<Id,List<Waiting_List__c>> reflectiveWaitingListMap){
        
        //initialize the collections for insertion of allocations, historic waiting lists and deletion of waiting list
        List<Allocation__c> insertableAllocationList = new List<Allocation__c>();
        List<Historic_Waiting_List__c> insertableHistWaitList = new List<Historic_Waiting_List__c>();
        Set<Id> deletableWaitingLstIdSet = new Set<Id>();
        Set<Id> contactProcessed = new Set<Id>();
        
        //if we have some inventories infomation
        if(inventoryInfoMap!=null && !inventoryInfoMap.keySet().isEmpty()){
            
            //for each inventory in map
            for(Id inventoryId : inventoryInfoMap.keySet()){
                
                //get all the variables in invInfo in simple variables
                InventoryInfo invInfo = inventoryInfoMap.get(inventoryId);
                List<Space__c> spaceList = invInfo.inventory.Spaces__r;
                
                //check for all waiting lists of inventory
                if(invInfo.waitingLstWrpList!=null && !invInfo.waitingLstWrpList.isEmpty()){
                    
                    //calculate the size and store it in variable for better performance
                    Integer waitingListSize = invInfo.waitingLstWrpList.size();
                    Integer spaceListSize = (spaceList==null || spaceList.isEmpty()?0:spaceList.size());
                    Integer leastSize = (spaceListSize < waitingListSize ? spaceListSize : waitingListSize);
                    
                    //if inventory is of type cycle hub
                    if(invInfo.inventory.RecordType.DeveloperName=='Cycle_Hub'){
                        
                        //if space exist
                        if(spaceListSize>0){
                            
                            Integer noOfAciveTenants = 0;
                            if(invInfo.inventory.axOversubscribeAmount__c!=null){
                                noOfAciveTenants = (Integer)(invInfo.inventory.AllowedNumberofTenants__c + invInfo.inventory.axOversubscribeAmount__c);
                            }
                            else{
                                noOfAciveTenants = (Integer)(invInfo.inventory.AllowedNumberofTenants__c);
                            }
                            
                            Integer spaceIndex = 0;
                            Integer spaceCounter = noOfAciveTenants;
                            
                            //for each waiting list record
                            for(Integer i=0;i<waitingListSize;i++){
                                
                                //get the waitingf list sObject record wrapped in wrapper
                                Waiting_List__c waitingListRecord = invInfo.waitingLstWrpList[i].waitingListRecord;
                                
                                //code to reset the space counter when maximum allocations aare made in this space.
                                if(spaceCounter==0){
                                    spaceCounter = noOfAciveTenants;
                                    spaceIndex = spaceIndex+1;
                                }
                                
                                //check if the index made is valid space list index
                                if(spaceListSize > spaceIndex){
                                    
                                    //add allocation of this waiting list
                                    insertableAllocationList.add(new Allocation__c(
                                        LeadTenant__c = waitingListRecord.Contact__c,
                                        RentPaymentFrequency__c  = invInfo.inventory.Borough_Payment_Frequency__c,
                                        Space__c = spaceList[spaceIndex].Id
                                    ));    
                                    
                                    //add historic waiting list record for this inventory
                                    insertableHistWaitList.add(new Historic_Waiting_List__c(
                                        Contact__c  = waitingListRecord.Contact__c,
                                        Original_Date_Requested__c = waitingListRecord.DateRequested__c,
                                        Inventory__c = inventoryId,
                                        Status__c = 'Offered',
                                        Priority__c = waitingListRecord.Priority__c,
                                        Suggested_Location__c = waitingListRecord.Suggested_Location__c,
                                        SuggestedLocationComments__c = waitingListRecord.Suggested_Location_Comments__c,
                                        TenantAdditionalComments__c = waitingListRecord.TenantAdditionalComments__c
                                    ));
                                    
                                    //add the waiting list id to be deleted
                                    deletableWaitingLstIdSet.add(waitingListRecord.Id);
                                    
                                    //decrease space counter
                                    spaceCounter=spaceCounter - 1;
                                
                                    //if waiting list has unprocessed contact, search the reflective waiting list
                                    //to create historic waiting list
                                    if(waitingListRecord.Contact__c!=null && !contactProcessed.contains(waitingListRecord.Contact__c)){
                                        
                                        //get the reflective waiting lists in other inventories with contact same as this waiting list
                                        List<Waiting_List__c> reflectiveWaitingList = reflectiveWaitingListMap.get(waitingListRecord.Contact__c);    
                                        
                                        //add historical waiting list for reflective waiting list
                                        if(reflectiveWaitingList!=null && !reflectiveWaitingList.isEmpty()){
                                            
                                            for(Waiting_List__c refWaitListRec : reflectiveWaitingList){
                                                
                                                //add historic waiting list record for this inventory
                                                if(refWaitListRec.Inventory__c!=inventoryId){
                                                    insertableHistWaitList.add(new Historic_Waiting_List__c(
                                                        Contact__c  = refWaitListRec.Contact__c,
                                                        Original_Date_Requested__c = refWaitListRec.DateRequested__c,
                                                        Inventory__c = refWaitListRec.Inventory__c,
                                                        Status__c = 'Offered',
                                                        Priority__c = refWaitListRec.Priority__c,
                                                        Suggested_Location__c = refWaitListRec.Suggested_Location__c,
                                                        SuggestedLocationComments__c = refWaitListRec.Suggested_Location_Comments__c,
                                                        TenantAdditionalComments__c = refWaitListRec.TenantAdditionalComments__c
                                                    ));    
                                                    
                                                    //add waiting list id in deletableWaitingLstIdSet to delete
                                                    deletableWaitingLstIdSet.add(refWaitListRec.Id);
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                        
                                        //add the contact to processed set
                                        contactProcessed.add(waitingListRecord.Contact__c);
                                        
                                    }
                                    
                            	}
                                
                                //go out of this loop since spaces are over
                                else{
                                    break;
                                }
                            }
                            
                        }
                        
                    }
                    
                    //for other types of inventories
                    else{
                        
                        //loop on the waiting list to match it with space
                        for(Integer i=0;i<leastSize;i++){
                            
                            //get the waitingf list sObject record wrapped in wrapper
                            Waiting_List__c waitingListRecord = invInfo.waitingLstWrpList[i].waitingListRecord;
                            
                            //add allocations, historical waiting list for this waiting list
                            if(spaceList!=null && !spaceList.isEmpty() && spaceList[i]!=null){
                                
                                //add allocation of this waiting list
                                insertableAllocationList.add(new Allocation__c(
                                    LeadTenant__c = waitingListRecord.Contact__c,
                                    RentPaymentFrequency__c  = invInfo.inventory.Borough_Payment_Frequency__c,
                                    Space__c = spaceList[i].Id
                                ));
                                
                                //add historic waiting list record for this inventory
                                insertableHistWaitList.add(new Historic_Waiting_List__c(
                                    Contact__c  = waitingListRecord.Contact__c,
                                    Original_Date_Requested__c = waitingListRecord.DateRequested__c,
                                    Inventory__c = inventoryId,
                                    Status__c = 'Offered',
                                    Priority__c = waitingListRecord.Priority__c,
                                    Suggested_Location__c = waitingListRecord.Suggested_Location__c,
                                    SuggestedLocationComments__c = waitingListRecord.Suggested_Location_Comments__c,
                                    TenantAdditionalComments__c = waitingListRecord.TenantAdditionalComments__c
                                ));
                                
                                //add the waiting list id to be deleted
                                deletableWaitingLstIdSet.add(waitingListRecord.Id);
                                
                            }
                            
                            //if waiting list has unprocessed contact, search the reflective waiting list
                            //to create historic waiting list
                            if(waitingListRecord.Contact__c!=null && !contactProcessed.contains(waitingListRecord.Contact__c)){
                                
                                //get the reflective waiting lists in other inventories with contact same as this waiting list
                                List<Waiting_List__c> reflectiveWaitingList = reflectiveWaitingListMap.get(waitingListRecord.Contact__c);    
                                
                                //add historical waiting list for reflective waiting list
                                if(reflectiveWaitingList!=null && !reflectiveWaitingList.isEmpty()){
                                    
                                    for(Waiting_List__c refWaitListRec : reflectiveWaitingList){
                                        
                                        //add historic waiting list record for this inventory
                                        if(refWaitListRec.Inventory__c!=inventoryId){
                                            insertableHistWaitList.add(new Historic_Waiting_List__c(
                                                Contact__c  = refWaitListRec.Contact__c,
                                                Original_Date_Requested__c = refWaitListRec.DateRequested__c,
                                                Inventory__c = refWaitListRec.Inventory__c,
                                                Status__c = 'Offered',
                                                Priority__c = refWaitListRec.Priority__c,
                                                Suggested_Location__c = refWaitListRec.Suggested_Location__c,
                                                SuggestedLocationComments__c = refWaitListRec.Suggested_Location_Comments__c,
                                                TenantAdditionalComments__c = refWaitListRec.TenantAdditionalComments__c
                                            ));    
                                            
                                            //add waiting list id in deletableWaitingLstIdSet to delete
                                            deletableWaitingLstIdSet.add(refWaitListRec.Id);
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                                //add the contact to processed set
                                contactProcessed.add(waitingListRecord.Contact__c);
                                
                            }
                            
                        }
                        
                        
                    }
                    
                }
                
            }
            
        }
        
        //insert the allocations, historic waiting list and delete waiting list
        try{
            
            if(insertableAllocationList!=null && !insertableAllocationList.isEmpty()){
                
                insert insertableAllocationList;
                
                //insert the historical waiting list
                if(insertableHistWaitList!=null && !insertableHistWaitList.isEmpty()){
                    
                    insert insertableHistWaitList;
                    
                    //delete the waiting list
                    if(deletableWaitingLstIdSet!=null && !deletableWaitingLstIdSet.isEmpty()){
                        List<Id> deletableWaitingList = new List<Id>();
                        deletableWaitingList.addAll(deletableWaitingLstIdSet);
                        Database.delete(deletableWaitingList);    
                    }
                }
                
            }
            
            
        }
        catch(Exception e){
            
            System.debug('EXCEPTION HAPPENED ON LINE--> '+e.getLineNumber()+'--Message : '+e.getMessage());
            
        }
        
    }
    
    //wrapper class to consolidate the inventory information
    public class InventoryInfo{
        public Inventory__c inventory{get;set;}
        
        //this wrapper is created to override the sort method
        //for the waiting list sobject
        public List<WaitingListCmpWrapper> waitingLstWrpList{
            get{
                if(waitingLstWrpList!=null){
                    waitingLstWrpList.sort();
                    return waitingLstWrpList;
                }
                else{
                    return null;
                }
            }
            set;
        }
    }
    
}