/***************************************************
* Author        : Aman Sawhney
* Class Name    : InventoryMaintainenceBatch
* Date Created  : 27 - Feb - 2018
* Purpose       : This class aggregates the batch operation
*				  inventory for maintainence records.
* *************************************************/
public class InventoryMaintainenceBatch implements Database.Batchable<sObject>{
    
    public String query;
    
    public InventoryMaintainenceBatch(String query){
        this.query = query;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Inventory__c> inventoryList){
        
        //this method creates the maintainence records on inventories
        InventoryWork.maintainInventories(inventoryList);
        
    }
    
    public void finish(Database.BatchableContext BC){}
    
    
}