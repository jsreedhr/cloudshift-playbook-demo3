public without sharing class OpportunityTriggerFunctions{
    
    public static void createinventory(List <Opportunity> lst_newopps, List <Opportunity> lst_oldopps){
        
        //Declare the variables
        Set <Id> set_WonoppIds = new Set <Id>();
        List <Contract> lst_cons = new List <Contract>();
        Map <Id, Id> map_OppIdConId = new Map <Id, Id>();
        List <Inventory__c> lst_invs = new List <Inventory__c>();
        Map<String, Id> map_RTName_rtID = new Map <String, ID>();
        Map<Id, String> map_rtID_RTName = new Map <ID, String>();
        List <Space__c> lst_spacestocreate = new List <Space__c>();
        Map<Id, Double> map_InvId_SubAmount = new Map <ID, Double>();
        List <Subsidy__c> lst_subs = new List <Subsidy__c>();
        
        List <RecordType> lst_rts = [SELECT Id,Name, DeveloperName FROM RecordType WHERE SobjectType='Inventory__c'];
        
        IF(!lst_rts.isEmpty()){
            for(RecordType rt: lst_rts){
                map_RTName_rtID.put(rt.Name, rt.id);
                map_rtID_RTName.put(rt.id, rt.Name);
            }            
        }
        
        //Check the opp status
        
        for(Opportunity nopp: lst_newopps){
            for(Opportunity oopp: lst_oldopps){
                If(nopp.id == oopp.id && nopp.StageName != oopp.StageName && nopp.StageName == 'Closed Won'){
                    set_WonoppIds.add(nopp.id);
                }
            }
        }
        
        //Query for products where inventory is ticked
        List <OpportunityLineItem> lst_olis = new List <OpportunityLineItem>([SELECT ID, Opportunityid, Opportunity.AccountId, Quantity, PONumber__c, Auto_Create__c, Name, Number_of_Spaces__c, Price_Per_User__c, Subsidy__c, Subsidy_Term__c FROM OpportunityLineItem WHERE Auto_Create__c !=Null AND Opportunityid in: set_WonoppIds]);
        
        IF(!lst_olis.isempty()){
            
            System.debug(lst_olis.size());
            for(Opportunity nopp: lst_newopps){
                Contract c = new Contract();
                c.AccountId = nopp.AccountId;
                c.Name = nopp.Account_Name__c + ' - ' + '(' + nopp.PONumber__c + ')';
                c.StartDate = nopp.Contract_Start_Date__c;
                c.ContractTerm = Integer.valueOf(nopp.Contract_Term__c);
                c.Status = 'Draft';
                c.Originating_Opportunity__c = nopp.id;
                lst_cons.add(c);
            }
            insert lst_cons;
            
            for(Contract c: lst_cons){
                map_OppIdConId.put(c.Originating_Opportunity__c, c.id);
                c.Status = 'Activated';
            }
            update lst_cons;
            
            //Create inventories
            for(OpportunityLineItem oli: lst_olis){
                for(integer i = 0; i < oli.quantity; i++){
                    Inventory__c inv = new Inventory__c();
                    inv.Stage__c = 'New';
                    inv.Project_PO_Number__c = oli.PONumber__c;
                    inv.Originating_Opportunity__c = oli.Opportunityid;
                    inv.Originating_Contract__c = map_OppIdConId.get(oli.Opportunityid);
                    inv.Current_Contract__c = map_OppIdConId.get(oli.Opportunityid);
                    inv.RecordTypeId = map_RTName_rtID.get(oli.Auto_Create__c);
                    inv.Number_of_Spaces_Created__c = oli.Number_of_Spaces__c;
                    inv.Price_Per_User__c = oli.Price_Per_User__c;
                    inv.Originating_Subsidy_Amount__c = oli.Subsidy__c;
                    inv.Originating_Subsidy_Term__c = oli.Subsidy_Term__c;
                    inv.ClientBorough__c = oli.Opportunity.AccountId;
                    lst_invs.add(inv);
                }
            }
            insert lst_invs;
                                            
            //Create the spaces 
            for(Inventory__c inv: lst_invs){
                    for(integer i=0; i<inv.Number_of_Spaces_Created__c; i++){
                        IF(inv.Number_of_Spaces_Created__c >=1){
                        Space__c s = new Space__c();
                        s.Inventory__c = inv.id;
                        s.name = String.valueOf(i+1);
                        lst_spacestocreate.add(s);
                        }
                    }
            	}
            IF(!lst_spacestocreate.isEmpty()){
            insert lst_spacestocreate;
            }                        
        }  
    }
}