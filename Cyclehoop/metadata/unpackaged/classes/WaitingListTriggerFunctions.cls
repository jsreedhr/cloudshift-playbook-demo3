public without sharing class WaitingListTriggerFunctions {
    
    public static void checkdupecontact(List <Waiting_List__c> lst_waitlist){
        
        Set <String> set_WLEmails = new Set <String>();
        List <Contact> lst_unmatchingconstoinsert = new List <Contact>();
        List <Contact> lst_matchingconstoinsert = new List <Contact>();
		List <Account> lst_unmatchingpersonaccountstoinsert = new List<Account>();
		List <Account> lst_matchingpersonaccountstoinsert = new List<Account>();
        Map <String, Id> MapConEmailId = new Map <String, ID>();
        List<Account> personAccountWithContactList = null;
		
        //If the contact record is populated we won't need to process further
        for (Waiting_List__c w: lst_waitlist){
            IF(w.email__c != null && w.Contact__c == null){
                set_WLEmails.add(w.email__c);
            }
        }

        //if set_WLEmails is not empty, this means not that all the waiting lists are mapped with contact
        //respective contacts
        if(!set_WLEmails.isEmpty()){

            //List <Contact> lst_cons = new List <Contact>([SELECT ID, Email FROM Contact Where Email in: set_WLEmails]);
			
			List<Account> lst_personaccounts = [select FirstName,LastName,BillingStreet,BillingState,BillingCity,
                                  BillingCountry,BillingPostalCode,PersonEmail,
                                  PersonContactId,RecordTypeId,Phone
                                  from Account where 
                                  IsPersonAccount=true AND PersonEmail in: set_WLEmails];
			
			Id personAccountrecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().
                get('Person Account').getRecordTypeId();
            
            //If there are no matching contacts we will need to create some
            If(lst_personaccounts.isEmpty()){
                for (Waiting_List__c w: lst_waitlist){
                    Account personAccount  = new Account();
                    personAccount.FirstName   = w.First_Name__c;
                    personAccount.LastName   = w.Last_Name__c;
                    personAccount.PersonEmail  = w.Email__c;
                    //personAccount.Age__c = w.Age__c;
                    personAccount.Phone = w.Phone__c;
                    IF(w.Building_Name_Number__c != Null){
                        personAccount.BillingStreet  = w.Building_Name_Number__c + ' ' + w.Street__c;
                    }
                    IF(w.Building_Name_Number__c == Null){
                        personAccount .BillingStreet  = w.Street__c;
                    }
                    personAccount.BillingCity  = w.City__c;
                    personAccount.BillingCountry  = w.Country__c;
                    personAccount.BillingState  = w.State__c;
                    personAccount.BillingPostalCode  = w.Post_Code__c;
					personAccount.RecordTypeId = personAccountrecordTypeId;
                    lst_unmatchingpersonaccountstoinsert.add(personAccount);
                }
                insert lst_unmatchingpersonaccountstoinsert;
                
				personAccountWithContactList = [select Id,PersonEmail,PersonContactId from Account where Id IN:lst_unmatchingpersonaccountstoinsert];
				
                //After creating them we need to add in the references to the new contacts
                for (Waiting_List__c w: lst_waitlist){
                    for (Account c: personAccountWithContactList){
                        IF(w.email__c == c.PersonEmail){
                            w.Contact__c = c.PersonContactId;
                        }
                    }
                }
            }
            
            If(!lst_personaccounts.isEmpty()){
                
                for(Account c: lst_personaccounts){
                    MapConEmailId.put(c.PersonEmail, c.PersonContactId);
                }

                //If there are matching contacts we will need to link the contacts to the waiting list records
                for (Waiting_List__c w: lst_waitlist){
                    for (Account c: lst_personaccounts){
                        //There may be some in the batch that match
                        IF(w.Email__c == c.PersonEmail){
                            w.Contact__c = c.PersonContactId;
                        }
                        
                        //There may be some in the batch that do not match
                        IF(MapConEmailId.get(w.email__c) == null ){
                            Account personAccount  = new Account();
							personAccount.FirstName   = w.First_Name__c;
							personAccount.LastName   = w.Last_Name__c;
							personAccount.PersonEmail  = w.Email__c;
							//con.Age__c = w.Age__c;
							personAccount.Phone = w.Phone__c;
							IF(w.Building_Name_Number__c != Null){
								personAccount.BillingStreet  = w.Building_Name_Number__c + ' ' + w.Street__c;
							}
							IF(w.Building_Name_Number__c == Null){
								personAccount .BillingStreet  = w.Street__c;
							}
							personAccount.BillingCity  = w.City__c;
							personAccount.BillingCountry  = w.Country__c;
							personAccount.BillingState  = w.State__c;
							personAccount.BillingPostalCode  = w.Post_Code__c;
							personAccount.RecordTypeId = personAccountrecordTypeId;
							lst_matchingpersonaccountstoinsert.add(personAccount);
                        }
                    } 
                }

                IF(!lst_matchingpersonaccountstoinsert.isempty()){
                    insert lst_matchingpersonaccountstoinsert;
                }
                personAccountWithContactList = [select Id,PersonEmail,PersonContactId from Account where Id IN:lst_matchingpersonaccountstoinsert];
                
                //After creating them we need to add in the references to the new contacts
                for (Waiting_List__c w: lst_waitlist){
                    for (Account c: personAccountWithContactList){
                        IF(w.email__c == c.PersonEmail){
                            w.Contact__c = c.PersonContactId;
                        }
                    }
                }
            }
        }
    }
    
    public static void fillInventory(List <Waiting_List__c> waitinglistList) {
        
    }
    
}