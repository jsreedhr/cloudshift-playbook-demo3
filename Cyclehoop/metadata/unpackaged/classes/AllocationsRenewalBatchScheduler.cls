global class AllocationsRenewalBatchScheduler implements Schedulable {

   global void execute(SchedulableContext ctx) {
      AllocationsRenewalBatch p = new AllocationsRenewalBatch();
        database.executeBatch(p);
   }   
}