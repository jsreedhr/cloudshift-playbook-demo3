@isTest
public class OpportunityTriggerFunctionsTest {

    private static testMethod void testMethod1(){

        Test.startTest();

        Product2 prod = new Product2(Name = 'La200');
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        Pricebook2 customPB = new Pricebook2(Name = 'Custom Pricebook', isActive = true);
        insert customPB;

        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;


        List < RecordType > lst_rts = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Inventory__c'];
        //Inserting personal account record
        List < Account > accountList = CommonTestDataFactory.createAccounts(1, true);
        system.assertNotEquals(accountList, null);

        Id stExam = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Company Account Record Type').getRecordTypeId();
        List < Account > accountList2 = new List < Account > ();

        Account account = new Account(
            name = 'Account_1',
            Sector__c = 'Public Sector - NHS',
            Phone = '999-999-(1000+1)',
            Billingstreet = 'test street',
            BillingCity = 'test city',
            BillingPostalcode = 'LS1 3DD',
            BillingCountry = 'test country',
            BillingState = 'test state',
            Industry = 'Apparel',
            Website = 'test@org.com',
            RecordtypeId = stExam
        );
        accountList2.add(account);
        insert accountList2;
        //Inserting contact record
        //List<Contact> contactList = CommonTestDataFactory.createContacts(1,true);

        Contact contact = new Contact(
            LastName = 'Contact_1',
            Phone = '999-555-1000',
            Email = 'contact_1@example.com',
            AccountId = accountList2[0].id
        );
        insert contact;
        System.debug('aaa--' + contact.id);


        Opportunity opp = new Opportunity(
            name = 'opp1',
            AccountId = accountList2[0].id,
            Contact_Name__c = contact.id,
            CloseDate = System.today().addDays(2),
            Sales_Type__c = 'Direct Sale',
            StageName = 'Quotation',
            ForecastCategoryName = 'Pipeline'

        );

        insert opp;
        OpportunityLineItem lineItem = new OpportunityLineItem(
            Opportunityid = opp.id,
            Number_of_Spaces__c = 2,
            TotalPrice = 123,
            Quantity = 2,
            Auto_Create__c = 'Bikehangar',
            PricebookEntryId = customPrice.id
            // UnitPrice=23

        );
        insert lineItem;

        opp.StageName = 'Closed Won';
        opp.ForecastCategoryName = 'Closed';
        opp.Contract_Start_Date__c = System.today().addDays(-6);
        opp.Contract_Term__c = 4;
        opp.Payment_Type__c = '100% up front';
        opp.Expected_Project_Completion_Date__c = System.today().addDays(2);
        opp.PONumber__c = '1234';
        opp.Project_Type__c = 'Supply, Install & Managed';
        update opp;

        Id recordTypeId = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Bikehangar').getRecordTypeId();
        Id recordTypeId2 = Schema.SObjectType.Inventory__c.getRecordTypeInfosByName().get('Cycle Hub').getRecordTypeId();
        Id recordTypeIdWL = Schema.SObjectType.Waiting_List__c.getRecordTypeInfosByName().get('Waiting List Record Type').getRecordTypeId();

        List < Inventory__c > lstInven = new List < Inventory__c > ();

        Inventory__c inventory = new Inventory__c(
            Stage__c = 'New',
            RecordTypeId = recordTypeId,
            Deposit__c = 20.00,
            ReadyforAutoAllocation__c = true
            
             
        );
        lstInven.add(inventory);

        Contract contract = new Contract(
            Status = 'Draft',
            StartDate = system.today(),
            ContractTerm = 6,
            AccountId = account.Id
        );
        insert contract;

        Inventory__c inventory2 = new Inventory__c(
            RecordTypeId = recordTypeId2,
            ReadyforAutoAllocation__c = true,
            Deposit__c = 20.00,
            AllowedNumberofTenants__c = 2,
            Stage__c = 'New',
            Originating_Subsidy_Amount__c = 1,
            Originating_Subsidy_Term__c = 2,
            Current_Contract__c = contract.Id,
            axOversubscribeAmount__c = 23

        );
        lstInven.add(inventory2);
        
        
        insert lstInven;


        List < Waiting_List__c > waitingList = new List < Waiting_List__c > ();
        Waiting_List__c waitInfo = new Waiting_List__c(
            RecordTypeId = recordTypeIdWL,
            DateRequested__c = System.today(),
            Status__c = 'New',
            Priority__c = false,
            Suggested_Location__c = false,
            Email__c = 'wl_2@example.com',
            Inventory__c = inventory.id,
            Last_Name__c = 'testLast1'
        );

        waitingList.add(waitInfo);
        Waiting_List__c waitInfo2 = new Waiting_List__c(
            RecordTypeId = recordTypeIdWL,
            DateRequested__c = System.today(),
            Status__c = 'New',
            Priority__c = true,
            Suggested_Location__c = true,
            Email__c = 'wl_2@example.com',
            Inventory__c = inventory.id,
            Last_Name__c = 'testLast'
        );
        waitingList.add(waitInfo2);

        Waiting_List__c waitInfo3 = new Waiting_List__c(
            RecordTypeId = recordTypeIdWL,
            DateRequested__c = System.today(),
            Status__c = 'New',
            Priority__c = true,
            Suggested_Location__c = false,
            Email__c = 'wl_2@example.com',
            Inventory__c = inventory2.id,
            Last_Name__c = 'testLast',
            WaitingListException__c = false,
            Contact__c = contact.Id
        );
        waitingList.add(waitInfo3);

        insert waitingList;

        List < Space__c > spaceList = new List < Space__c > ();
        Space__c space = new Space__c(
            Name = 'Space_1',
            Status__c = 'Available',
            Inventory__c = inventory.id
        );
        spaceList.add(space);

        Space__c space2 = new Space__c(
            Name = 'Space_2',
            Status__c = 'Available',
            Inventory__c = inventory.id
        );
        spaceList.add(space2);

        Space__c space3 = new Space__c(
            Name = 'Space_3',
            Status__c = 'Available',
            Inventory__c = inventory2.id
        );
        spaceList.add(space3);
        insert spaceList;

        lstInven[0].Stage__c = 'Installed';
        lstInven[0].ActualInstallationDate__c = System.today().addDays(2);
        // update inventory;

        lstInven[1].Stage__c = 'Installed';
        lstInven[1].ActualInstallationDate__c = System.today().addDays(2);
        // update inventory2;
        
        update lstInven;
        Test.stopTest();
    }

}