global class AllocationsRenewalBatch implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Update for termination date and active let status
        String strActiveLet = 'Active Let';
        //String query = 'SELECT ID, Name, Lead_Tenant_ID__c, Space_ID__c, RentPaymentFrequency__c, OfficeNotes__c, TenantAdditionalComments__c, TenantBikeDetails__c, Date_Requested__c, Renewal__c, Previous_Allocation__c FROM ALlocation__c WHERE EndDate__c = yesterday';
        String query = 'SELECT ID, Name, Lead_Tenant_ID__c, Space_ID__c,Space__c,LeadTenant__c, RentPaymentFrequency__c, OfficeNotes__c, TenantAdditionalComments__c, TenantBikeDetails__c, Date_Requested__c, Renewal__c, Previous_Allocation__c, ChargentToken__c, Transferring_to_Monthly_on_Renewal__c FROM ALlocation__c WHERE EndDate__c = YESTERDAY AND  Termination_Requested_Date__c = NULL AND Status__c =:strActiveLet';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Allocation__c> scope)
    {
        if (scope.size() <= 0)
            return;
        List <Allocation__c> lst_allstoexpire = New List <Allocation__c>();
        List <Allocation__c> lst_allstocreate = New List <Allocation__c>();
        //System.debug('--testAll--'+scope);
        for(Allocation__c all: scope){
            
            all.Status__c = 'Renewed';
            lst_allstoexpire.add(all);
            
            //Create new allocations
            Allocation__c newall = new Allocation__c();
            // newall.Space__c = all.Space_ID__c;
            newall.Space__c = all.Space__c;
            // newall.LeadTenant__c = all.Lead_Tenant_ID__c;
            newall.LeadTenant__c = all.LeadTenant__c;
            newall.StartDate__c = System.today();
            newall.EndDate__c = System.today()+365;
            newall.Status__c = 'Pending Payment';
            newall.RentPaymentFrequency__c = all.RentPaymentFrequency__c;
            newall.OfficeNotes__c = all.OfficeNotes__c;
            newall.TenantAdditionalComments__c = all.TenantAdditionalComments__c;
            newall.TenantBikeDetails__c = all.TenantBikeDetails__c;
            newall.Date_Requested__c = all.Date_Requested__c;
            newall.Renewal__c = TRUE;
			newall.ChargentToken__c = all.ChargentToken__c;
            newall.Previous_Allocation__c = all.id;
            newall.Transferring_to_Monthly_on_Renewal__c = all.Transferring_to_Monthly_on_Renewal__c;
            lst_allstocreate.add(newall);
        }
        
        IF(lst_allstoexpire.size()>0){
            update lst_allstoexpire;
            insert lst_allstocreate;
        }
        
    }
    global void finish(Database.BatchableContext BC) {
        
    }
    
}