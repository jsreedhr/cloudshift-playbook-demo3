public without sharing class OrdersPaymentReqTriggerFunction {
    
    public static void updatePaymentUrl(List <ChargentOrders__Payment_Request__c> lstPaymentReq){
        List<ChargentOrders__Payment_Request__c> lstPayReq = new List<ChargentOrders__Payment_Request__c>();
        Map<id,id> orderPayRegIds = new Map<Id , Id>();
        for(ChargentOrders__Payment_Request__c objPayReq : lstPaymentReq){
            System.debug('--URLinsert--'+objPayReq.ChargentOrders__Pay_Link__c);
        }
        
    }
    
    public static void updatePaymentUrl2(List <ChargentOrders__Payment_Request__c> lstPaymentReq) {
        List<ChargentOrders__Payment_Request__c> lstPayReq = new List<ChargentOrders__Payment_Request__c>();
        Map<Id,Id> orderPayRegIds = new Map<Id , Id>();  
        for(ChargentOrders__Payment_Request__c objPayReq : lstPaymentReq){
            objPayReq.Payment_URL_Modified__c=objPayReq.ChargentOrders__Pay_Link__c;
            
            if(!objPayReq.AllocationRenewal__c){
                objPayReq.ChargentOrders__Pay_Link__c=Label.Chargent_Order_Link+'?InventoryClientBorough='+objPayReq.InventoryClientBorough__c+'&payReqId='+objPayReq.id;
            }
        }
    }  
}